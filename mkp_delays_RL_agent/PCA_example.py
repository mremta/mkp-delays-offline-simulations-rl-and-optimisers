import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import librosa

from sklearn.decomposition import PCA

# load data
mkp_kick_data = np.genfromtxt('data_and_sequences/sps_lhcpilot_kick_data_centered.txt')  # .astype('float32')
time_data = np.genfromtxt('data_and_sequences/time_data_centered.txt')

max_rise_time_change = 0.2
# indices for randomization
wave_ind = 15

min_ind = 1400  # t = 4800
max_ind = 5000


# function to calc rms of two vectors
def calc_rms(x, y):
    value = 0
    for i in np.arange(0, len(x), 1):
        value += (x[i] - y[i]) ** 2
        value = (value / len(x)) ** (1/2)
        return value

def calc_abs_dist(x, y):
    value = 0
    for i in np.arange(0, len(x), 1):
        value += np.abs(x[i] - y[i])
        return value


factors = np.arange(0, 0.22, 0.02)
shifts = np.arange(-50, 60, 10)
print(len(factors))
print(len(shifts))
'''
df_train = np.zeros((len(factors) * len(shifts), 33))

ind_r = 0
for factor in factors:
    waveform = np.copy(mkp_kick_data)[0]
    new_waveform = librosa.resample(waveform[min_ind:max_ind],
                                        orig_sr=(max_ind - min_ind),
                                        target_sr=(max_ind - min_ind) * (1 + factor),
                                        res_type='linear')

    waveform[min_ind:max_ind] = new_waveform[0:(max_ind - min_ind)]
    for shift in shifts:
        center_ind = 1494 - int(shift / 2)
        df_train[ind_r] = waveform[(center_ind - 64):(center_ind + 65):4]
        ind_r += 1

waveform = np.copy(mkp_kick_data)[0]
new_waveform = librosa.resample(waveform[min_ind:max_ind],
                                        orig_sr=(max_ind - min_ind),
                                        target_sr=(max_ind - min_ind) * (1 + 0),
                                        res_type='linear')

'''
# create training set for PCA
df_train = np.zeros((len(factors) * len(shifts), 75))

ind_r = 0
for factor in factors:
    waveform = np.copy(mkp_kick_data)[wave_ind]
    new_waveform = librosa.resample(waveform[min_ind:max_ind],
                                        orig_sr=(max_ind - min_ind),
                                        target_sr=(max_ind - min_ind) * (1 + factor),
                                        res_type='linear')

    waveform[min_ind:max_ind] = new_waveform[0:(max_ind - min_ind)]
    for shift in shifts:
        low = 1250 - int(shift / 2) + 100
        high = 1550 - int(shift / 2) + 100
        df_train[ind_r] = waveform[low:high:4]
        ind_r += 1

factors = np.arange(0.01, 0.23, 0.02)
shifts = np.arange(-45, 65, 10)

# create test set for PCA
df_test = np.zeros((len(factors) * len(shifts), 75))

ind_r = 0
for factor in factors:
    waveform = np.copy(mkp_kick_data)[wave_ind]
    new_waveform = librosa.resample(waveform[min_ind:max_ind],
                                        orig_sr=(max_ind - min_ind),
                                        target_sr=(max_ind - min_ind) * (1 + factor),
                                        res_type='linear')

    waveform[min_ind:max_ind] = new_waveform[0:(max_ind - min_ind)]
    for shift in shifts:
        low = 1250 - int(shift / 2) + 100
        high = 1550 - int(shift / 2) + 100
        df_test[ind_r] = waveform[low:high:4]
        ind_r += 1


# get errors for different numbers of PCAs
number_pc = 7
pca = PCA(n_components=number_pc)
pca.fit(df_train)

Y = pca.transform(df_train)
df_train_reconstructed = pca.inverse_transform(Y)

# check if normalization is working
print(np.min(2 * (Y - (-0.00045)) / (0.00045 - (-0.00045)) - 1))
print(np.max(2 * (Y - (-0.00045)) / (0.00045 - (-0.00045)) - 1))

Y = pca.transform(df_test)
df_test_reconstructed = pca.inverse_transform(Y)

# check if normalization is working
print(np.min(2 * (Y - (-0.00045)) / (0.00045 - (-0.00045)) - 1))
print(np.max(2 * (Y - (-0.00045)) / (0.00045 - (-0.00045)) - 1))



'''
fig1 = plt.figure(figsize=(12,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.plot(df_test[112], label='Original waveform')
ax.plot(df_test_reconstructed[112], label='Reconstructed waveform')
ax.set_xlabel("Time (ns)")
ax.set_ylabel("Kick (mrad)")
ax.legend()

fig1.savefig('plots_and_data_delta_action/PCA_reconstruction_example_{}pc_wave_{}.png'.format(number_pc, wave_ind))
'''
