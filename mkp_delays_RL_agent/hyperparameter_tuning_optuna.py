from typing import Any, Dict, Optional

import numpy as np

import optuna
from optuna.pruners import MedianPruner
from optuna.samplers import GridSampler, TPESampler

from stable_baselines3 import PPO
from stable_baselines3.common.noise import NormalActionNoise, OrnsteinUhlenbeckActionNoise
from stable_baselines3.common.callbacks import BaseCallback, EvalCallback, StopTrainingOnNoModelImprovement
from torch import nn as nn

from mkpdelays_env_RL_delta_action_v17 import MKPOptEnv  # import environment using relative action

# global parameters for the study
STEPSIZE = 0.005
N_TRIALS = None  # Maximum number of trials
N_JOBS = 1  # Number of jobs to run in parallel (set to one, we parallelize differently)
N_STARTUP_TRIALS = 0  # Stop random sampling after N_STARTUP_TRIALS
N_EVALUATIONS = 4  # Number of evaluations during the training
N_TIMESTEPS = 1000000  # Training budget
EVAL_FREQ = int(N_TIMESTEPS / N_EVALUATIONS)
N_EVAL_EPISODES = 10  # Number of episodes to run at each evaluation
TIMEOUT = None  # stop trial after this many seconds


def sample_ppo_params(trial: optuna.Trial) -> Dict[str, Any]:
    """
    :param trial: Trial to draw hyperparameters for.
    :return: Dictionary containing hyperparameters for PPO.
    """
    batch_size = trial.suggest_categorical("batch_size", [8, 16, 32, 64, 128, 256, 512])
    n_steps = trial.suggest_categorical("n_steps", [8, 16, 32, 64, 128, 256, 512, 1024, 2048])
    gamma = trial.suggest_float("gamma", 0.5, 1)
    learning_rate = trial.suggest_float("learning_rate", 1e-5, 1, log=True)
    lr_schedule = "constant"
    ent_coef = trial.suggest_categorical("ent_coef", [0.0])
    clip_range = trial.suggest_categorical("clip_range", [0.2])
    n_epochs = trial.suggest_categorical("n_epochs", [10])
    gae_lambda = trial.suggest_categorical("gae_lambda", [1.0])
    max_grad_norm = trial.suggest_categorical("max_grad_norm", [0.5])
    vf_coef = trial.suggest_categorical("vf_coef", [0.5])
    width = trial.suggest_categorical("width", [16, 32, 64, 128])
    depth = trial.suggest_categorical("depth", [1, 2])
    # Uncomment for gSDE (continuous actions)
    # log_std_init = trial.suggest_float("log_std_init", -4, 1)
    # Uncomment for gSDE (continuous action)
    # sde_sample_freq = trial.suggest_categorical("sde_sample_freq", [-1, 8, 16, 32, 64, 128, 256])
    # Orthogonal initialization
    ortho_init = False
    # ortho_init = trial.suggest_categorical('ortho_init', [False, True])
    # activation_fn = trial.suggest_categorical('activation_fn', ['tanh', 'relu', 'elu', 'leaky_relu'])
    activation_fn = trial.suggest_categorical("activation_fn", ["relu"])

    if batch_size > n_steps:
        batch_size = n_steps

    # Independent networks usually work best
    # when not working with images
    architecture = [width for i in np.arange(0, depth)]
    net_arch = dict(pi=architecture, vf=architecture)

    activation_fn = {"tanh": nn.Tanh, "relu": nn.ReLU, "elu": nn.ELU, "leaky_relu": nn.LeakyReLU}[activation_fn]

    return {
        "n_steps": n_steps,
        "batch_size": batch_size,
        "gamma": gamma,
        "learning_rate": learning_rate,
        "ent_coef": ent_coef,
        "clip_range": clip_range,
        "n_epochs": n_epochs,
        "gae_lambda": gae_lambda,
        "max_grad_norm": max_grad_norm,
        "vf_coef": vf_coef,
        # "sde_sample_freq": sde_sample_freq,
        "policy_kwargs": dict(
            # log_std_init=log_std_init,
            net_arch=net_arch,
            activation_fn=activation_fn,
            ortho_init=ortho_init,
        ),
    }


class TrialEvalCallback(EvalCallback):
    """
    Callback used for evaluating and reporting a trial.
    """

    def __init__(
        self,
        eval_env,
        trial: optuna.Trial,
        callback_after_eval: Optional[BaseCallback] = None,
        n_eval_episodes: int = 5,
        eval_freq: int = 10000,
        deterministic: bool = True,
        verbose: int = 0,
        best_model_save_path: Optional[str] = None,
        log_path: Optional[str] = None,
    ) -> None:
        """
        :param eval_env: gym Environment for evaluation.
        :param trial: trial to evaluate.
        :param callback_after_eval: can be used to perform and additional callback.
        :param n_eval_episodes: number of evaluation episodes run at each callback.
        :param eval_freq: each eval_freq steps the performance is evaluated.
        :param deterministic: whether the agent should be set into deterministic mode.
        :param verbose: controls console output.
        :param best_model_save_path: path to save the best performing agent.
        :param log_path: path to save the logs.
        """

        super().__init__(
            eval_env=eval_env,
            callback_after_eval=callback_after_eval,
            n_eval_episodes=n_eval_episodes,
            eval_freq=eval_freq,
            deterministic=deterministic,
            verbose=verbose,
            best_model_save_path=best_model_save_path,
            log_path=log_path,
        )
        self.trial = trial
        self.eval_idx = 0
        self.is_pruned = False

    def _on_step(self) -> bool:
        """
        Performs callback and checks whether to continue training.

        :return: Whether to continue training.
        """

        if self.eval_freq > 0 and self.n_calls % self.eval_freq == 0:
            continue_training = super()._on_step()
            self.eval_idx += 1
            # report best or report current ?
            # report num_timesteps or elapsed time ?
            self.trial.report(self.last_mean_reward, self.eval_idx)
            # Early stopping if needed (uses callback_after_eval)
            if not continue_training:
                self.is_pruned = True
                return False
            # Prune trial if needed
            if self.trial.should_prune():
                self.is_pruned = True
                return False
        return True


def objective(trial: optuna.Trial) -> float:
    """
    Objective function used by Optuna to evaluate
    one configuration (i.e., one set of hyperparameters).

    Given a trial object, it will sample hyperparameters,
    evaluate it and report the result (mean episodic reward after training)


    :param trial: Trial to evaluate.
    :return: mean episodic reward after training.
    """

    default_hyperparams = {
        "policy": "MlpPolicy",
    }

    kwargs = default_hyperparams.copy()

    # 1. Sample hyperparameters and update the keyword arguments
    sampled_hyperparams = sample_ppo_params(trial)
    kwargs.update(sampled_hyperparams)

    env = MKPOptEnv(dtc=200, max_rise_time_change=0.2, max_steps=1000, action_scaling=STEPSIZE)
    env.seed(144)
    eval_env = MKPOptEnv(dtc=200, max_rise_time_change=0.2, max_steps=1000, action_scaling=STEPSIZE)
    eval_env.seed(1526683)

    # for Action Noise
    n_actions = env.action_space.shape[0]

    # Create the RL model
    model = PPO(env=env, **kwargs)

    # Callbacks for early stopping and pruning
    stop_train_callback = StopTrainingOnNoModelImprovement(max_no_improvement_evals=1,
                                                           min_evals=1,
                                                           verbose=1)

    eval_callback = TrialEvalCallback(eval_env=eval_env,
                                      trial=trial,
                                      callback_after_eval=None,
                                      n_eval_episodes=N_EVAL_EPISODES,
                                      eval_freq=EVAL_FREQ,
                                      deterministic=True,
                                      verbose=0)

    nan_encountered = False
    try:
        # Train the model
        model.learn(N_TIMESTEPS, callback=eval_callback)
    except ValueError as e:
        # Sometimes, random hyperparams can generate NaN / inf
        print(e)
        nan_encountered = True
    finally:
        # Free memory
        model.env.close()
        eval_env.close()

    # Tell the optimizer that the trial failed
    if nan_encountered:
        return float('nan')

    if eval_callback.is_pruned:
        # Save the model even when pruned
        model.save('plots_and_data_delta_action/study_PPO_5_ns_2/ppo_{}'.format(trial.number))
        raise optuna.exceptions.TrialPruned()

    # Save the model
    model.save('plots_and_data_delta_action/study_PPO_5_ns_2/ppo_{}'.format(trial.number))

    return eval_callback.last_mean_reward


if __name__ == "__main__":
    # Select the sampler, can be random, TPESampler, CMAES, ...
    # sampler = TPESampler(n_startup_trials=N_STARTUP_TRIALS)

    search_space = {"gamma": [0.8],
                    "n_steps": [2048],
                    "batch_size": [8, 16, 32, 64, 128],
                    "learning_rate": [3e-5, 3e-4, 3e-3, 3e-2],
                    "width": [16, 32, 64, 128],
                    "depth": [1, 2]}

    sampler = GridSampler(search_space)
    pruner = MedianPruner(n_startup_trials=N_STARTUP_TRIALS, n_warmup_steps=0)

    db_url = 'postgresql://optuna_user:1234@localhost:5432/optuna_database'
    storage = optuna.storages.RDBStorage(db_url)

    # Create the study and start the hyperparameter optimization
    study = optuna.create_study(sampler=sampler,
                                pruner=pruner,
                                direction='maximize',
                                storage=storage,
                                study_name='study_PPO_5_ns_2',
                                load_if_exists=True)

    study.optimize(objective, n_trials=N_TRIALS, timeout=TIMEOUT)

    print("Number of finished trials: ", len(study.trials))

    print("Best trial:")
    trial = study.best_trial

    print(f"  Value: {trial.value}")

    print("  Params: ")
    for key, value in trial.params.items():
        print(f"    {key}: {value}")

    print("  User attrs:")
    for key, value in trial.user_attrs.items():
        print(f"    {key}: {value}")

