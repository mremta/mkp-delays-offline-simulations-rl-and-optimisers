import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from cpymad.madx import Madx
from numpy import sqrt, sin, cos, pi


# The input of this function are the actual and expected kicks of the MKPs. Ít returns x and px according to the delta.
def mkp_kicks(kmkpa11931_actual, kmkpa11936_actual, kmkpc11952_actual, kmkp11955_actual,
              kmkpa11931_expected, kmkpa11936_expected, kmkpc11952_expected, kmkp11955_expected):

    kmkpa11931_delta = kmkpa11931_actual - kmkpa11931_expected
    kmkpa11936_delta = kmkpa11936_actual - kmkpa11936_expected
    kmkpc11952_delta = kmkpc11952_actual - kmkpc11952_expected
    kmkp11955_delta = kmkp11955_actual - kmkp11955_expected

    # starting point is the center of the first kicker, initial conditions are zero
    result = np.array([0, kmkpa11931_delta])  # first kick
    result = result + [result[1] * 3.622, kmkpa11936_delta]  # drift to center of second kicker and second kick
    result = result + [result[1] * 2.8005, kmkpc11952_delta]  # drift to center of third kicker and third kick
    result = result + [result[1] * 2.8005, kmkp11955_delta]  # drift to center of fourth kicker and fourth kick
    result = result + [result[1] * 1.72, 0]  # marker after the fourth kicker, from here MADX takes over the transport

    result = np.pad(result, (0, 4), 'constant', constant_values=(0, 0))  # the other four state space variables are 0

    return result


initial_conditions = mkp_kicks(kmkpa11931_actual=0.00093159,
                               kmkpa11936_actual=0.00096096,
                               kmkpc11952_actual=0.00038975,
                               kmkp11955_actual=0.00106243,
                               kmkpa11931_expected=0.00105888,
                               kmkpa11936_expected=0.00105888,
                               kmkpc11952_expected=0.00042331,
                               kmkp11955_expected=0.00111644)

print('Initial conditions after kickers:')
print(initial_conditions)

# start MADX
with open('tempfile', 'w') as f:
    madx = Madx(stdout=f,stderr=f)
    madx.option(verbose=True, debug=False, echo=True, warn=True, twiss_print=False)

# load SPS sequence and magnet strengths
madx.call(file='data_and_sequences/SPS_LS2_2020-05-26.seq')
madx.call(file='data_and_sequences/lhc_q20.str')

# move the start of the sequence to the marker right after the kickers
madx.input('''SEQEDIT, SEQUENCE=sps;
                FLATTEN;
                CYCLE, START=NEWBEG;
                FLATTEN;
              ENDEDIT;''')

# generate TWISS
madx.command.beam(sequence='sps')
madx.use(sequence='sps')
twiss = madx.twiss(sequence='sps', rmatrix=True, sectormap=True, sectoracc=True)

# retrieve R and T matrix
rmatrix = madx.sectortable()
tmatrix = madx.sectortable2()
idx = madx.sequence['sps'].expanded_elements.index('BPMBV.51303')  # retrieve the index of bpmbv.51303

# Transport initial conditions to the bpm manually
print('First order approx:')
first_order = np.matmul(rmatrix[idx][:6, :6], initial_conditions)
print(first_order)

second_order = np.zeros(6)
for i in np.arange(0, 6):
    second_order[i] = np.sum(tmatrix[idx][i] * np.outer(initial_conditions, initial_conditions))

print('Second order approx:')
print(first_order + second_order)

# Run Twiss again with non-zero initial conditions
twiss = madx.twiss(sequence='sps', x=initial_conditions[0], px=initial_conditions[1])

result_madx = np.array([twiss['x'][idx],
                        twiss['px'][idx],
                        twiss['y'][idx],
                        twiss['py'][idx],
                        twiss['t'][idx],
                        twiss['pt'][idx]])
print('MADX:')
print(result_madx)

# Cannot save 3d array to txt, so reshape
tmatrix_reshaped = tmatrix[idx].reshape(tmatrix[idx].shape[0], -1)

# save R and T matrix for later
np.savetxt('data_and_sequences/R_matrix.txt', X=rmatrix[idx][:6, :6])
np.savetxt('data_and_sequences/T_matrix.txt', X=tmatrix_reshaped)

# load the reshaped T matrix
loaded_arr = np.loadtxt('data_and_sequences/T_matrix.txt')

# reshape to original shape
load_original_arr = loaded_arr.reshape(6, 6, 6)

# check whether the original matrix was reconstructed
assert (tmatrix[idx] == load_original_arr).all()
