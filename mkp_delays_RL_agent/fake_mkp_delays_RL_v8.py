"""
Helper class meant to replicate the real MKP delays, but using historical waveform data. Version for RL agent, where
we include the possibility to randomize the shape of the waveforms for the agent to learn
"""

import numpy as np
import matplotlib.pyplot as plt
# import pickle
# from pathlib import Path
# import logging
# import time

# import sys
from cpymad.madx import Madx
import pandas as pd
import librosa

# sys.path.append('/home/ewaagaa/cernbox2/Tech_student/Code')  #for office computer
# import acc_phy_lib_elias


class FakeMKPdelays:
    def __init__(self, optics='q20', randomize_waveforms=True, dtc=500, max_rise_time_change=0.2):
        self.optics = optics
        self.mkp_kick_data = np.genfromtxt('data_and_sequences/sps_lhcpilot_kick_data_centered.txt')  # .astype('float32')
        self.time_data = np.genfromtxt('data_and_sequences/time_data_centered.txt')
        self.ref_timestamp = 4900  # timestamp of the beam at injection
        self.dtc = dtc
        self.randomize_waveforms = randomize_waveforms
        self.randomize_waveform_amplitude = False  # randomize waveform amplitude
        self.max_rise_time_change = max_rise_time_change  # maximum relative increase of rise time
        self.rise_time_change = []   # for logging purposes

        # -------------------- RANDOMIZATION OF WAVEFORM SHAPE OR AMPLITUDE -------------------------------
        # define waveform indices on which to start and stop
        self.min_ind = 1400  # t = 4800
        self.max_ind = 5000

        # Load transfer matrices and initial positions
        self.init_pos = np.genfromtxt('data_and_sequences/initial_positions.txt')
        self.transfer_matrices = []
        for i in np.arange(1, 9):
            self.transfer_matrices.append(np.genfromtxt('data_and_sequences/transfer_matrix_{}.txt'.format(i)))

        # randomize waveforms
        self.mkp_waveforms = np.zeros(self.mkp_kick_data.shape)
        self.augment_waveforms()

        self.start_reset()

    # Method to get fake BPM positions by providing a time shift vector dtau to step method, then get coordinates from MADX
    def get_positions(self, dtau):
        self.valid_cycle = False
        bpm_pos_x, bpm_pos_x_c = self.step(
            dtau)  # perform one step, providing MKP time shifts and get the BPM positions

        return bpm_pos_x, bpm_pos_x_c

    # Method to calculate oscillations of the injected and circulating beam (c) for a given timeshift vector for the MKP switches

    def step(self, dtau):

        # Check at what time to evaluate the MKP kick for the injected beam, and for the circulating beam
        t = self.ref_timestamp   # timestamp for evaluation for injected beam
        tc = t - self.dtc  #  timestamp for evaluation for injected beam
        self.dt_general = dtau[-1]  # general time shift
        t_inds = []
        tc_inds = []
        for i in range(8):  # 8 switches in total to iterate over
            t_ind = np.argwhere(self.time_data == (2 * t - dtau[i] - (
                        (2 * t - dtau[i]) % 2)))  # find closest even index corresponding to this time
            tc_ind = np.argwhere(self.time_data == (tc + t - dtau[i] - ((tc + t - dtau[i]) % 2)))
            t_inds.append(t_ind)
            tc_inds.append(tc_ind)

        # Iterate over the modules, check the time shift for each switch, and add the kicks for the injected and circulating beam
        self.new_kicks = []
        self.new_kicks_c = []
        switch_count = 0
        for i in range(16):
            if (((i) % 2) == 0) and (i != 0):  # after every two steps, change electrical switch
                switch_count += 1
            kick = self.mkp_waveforms[i][t_inds[switch_count]]
            kick_c = self.mkp_waveforms[i][tc_inds[switch_count]]
            self.new_kicks.append(kick)
            self.new_kicks_c.append(kick_c)

        # Define a vector with new MKP kicks, for the injected and circulating beam
        self.mkp_kicks = np.empty(4)
        self.mkp_kicks[0] = np.sum(self.new_kicks[:5])
        self.mkp_kicks[1] = np.sum(self.new_kicks[5:10])
        self.mkp_kicks[2] = np.sum(self.new_kicks[10:12])
        self.mkp_kicks[3] = np.sum(self.new_kicks[12:16])
        self.mkp_kicks_c = np.empty(4)
        self.mkp_kicks_c[0] = np.sum(self.new_kicks_c[:5])
        self.mkp_kicks_c[1] = np.sum(self.new_kicks_c[5:10])
        self.mkp_kicks_c[2] = np.sum(self.new_kicks_c[10:12])
        self.mkp_kicks_c[3] = np.sum(self.new_kicks_c[12:16])

        # transport x and px for the injected beam
        x_bar = self.transport(self.init_pos[0],
                               self.init_pos[1],
                               self.mkp_kicks[0],
                               self.mkp_kicks[1],
                               self.mkp_kicks[2],
                               self.mkp_kicks[3])[0]

        # transport x and px for the circulating beam
        x_bar_c = self.transport(self.init_pos[2],
                                 self.init_pos[3],
                                 self.mkp_kicks_c[0],
                                 self.mkp_kicks_c[1],
                                 self.mkp_kicks_c[2],
                                 self.mkp_kicks_c[3])[0]

        # return horizontal beam position of circulating and injected beam
        return x_bar, x_bar_c

    def transport(self, x, x_prime, kmkpa11931, kmkpa11936, kmkpc11952, kmkp11955):

        # kick 1
        result = np.matmul(self.transfer_matrices[0], [x, x_prime + kmkpa11931])  # thin kick
        result = result - [kmkpa11931 * 3.423 / 2, 0]  # linear correction for x
        # between kick 1 and 2
        result = np.matmul(self.transfer_matrices[1], result)
        # kick 2
        result = np.matmul(self.transfer_matrices[2], result + [0, kmkpa11936])  # thin kick
        result = result - [kmkpa11936 * 3.423 / 2, 0]  # linear correction for x
        # between kick 2 and 3
        result = np.matmul(self.transfer_matrices[3], result)
        # kick 3
        result = np.matmul(self.transfer_matrices[4], result + [0, kmkpc11952])  # thin kick
        result = result - [kmkpc11952 * 1.78 / 2, 0]  # linear correction for x
        # between kick 3 and 4
        result = np.matmul(self.transfer_matrices[5], result)
        # kick 4
        result = np.matmul(self.transfer_matrices[6], result + [0, kmkp11955])  # thin kick
        result = result - [kmkp11955 * 3.423 / 2, 0]  # linear correction for x
        # transport
        result = np.matmul(self.transfer_matrices[7], result)

        return result

    # call this function after every episode to randomize waveforms
    def augment_waveforms(self):
        # randomize shape of waveform rise time
        self.rise_time_change = []
        if self.randomize_waveforms:
            ind = 0
            sum = 0
            self.mkp_waveforms = np.copy(self.mkp_kick_data)  # first initialize the waveforms as they are today
            print("\nRandomizing waveform rise times...\n")
            for ele in self.mkp_kick_data:
                if (ind % 2) == 0:  # randomize the change in rise time for each switch
                    factor = np.random.rand() * self.max_rise_time_change
                    self.rise_time_change.append(factor)
                new_waveform = librosa.resample(ele[self.min_ind:self.max_ind],
                                                orig_sr=(self.max_ind - self.min_ind),
                                                target_sr=(self.max_ind - self.min_ind) * (1 + factor),
                                                res_type='linear')

                self.mkp_waveforms[ind][self.min_ind:self.max_ind] = new_waveform[0:(self.max_ind - self.min_ind)]
                ind += 1
            # print("\nAverage change of rise time by factor {}.\n".format(sum / 16 + 1))
        else:
            self.mkp_waveforms = np.copy(self.mkp_kick_data)  # otherwise just copy the waveform data
            # print("\nAverage change of rise time by factor {}.\n".format(1))

        # extract previous waveform data with randomized amplitude, or as it is
        if self.randomize_waveform_amplitude:
            ind = 0
            print("\nRandomizing waveform amplitudes...\n")
            for ele in self.mkp_waveforms:
                self.mkp_waveforms[ind] = ele * (
                        1 + 0.2 * np.random.rand())  # randomize waveform amplitude of present values +/- X percent
                ind += 1


    # Method to plot the oscillations
    def plot_oscillations(self, fig):
        if fig is None:
            fig = plt.figure(figsize=(10, 7))
        fig.suptitle('SPS oscillations - injected and circulating beam', fontsize=20)
        ax = fig.add_subplot(1, 1, 1)  # create an axes object in the figure
        ax.yaxis.label.set_size(24)
        ax.xaxis.label.set_size(24)
        plt.xticks(fontsize=18)
        plt.yticks(fontsize=18)
        # Define some parameters for the circulating beam, and plot it on the same axis as the injected
        if self.optics == 'sftpro':
            nx = 6
        else:
            nx = 5
        # Plot the injected beam:
        acc_phy_lib_elias.plot_envelope(self.twiss, self.sige, self.ex, self.ey, ax, nx=nx)
        # Align the injected and circulating beam longitudinally, then plot it on the same axis
        ds = self.twiss_c.loc['qf.12010'].s - self.twiss.loc['qf.12010'].s
        self.twiss_c['s'] = self.twiss_c['s'].add(-ds)
        acc_phy_lib_elias.plot_envelope(self.twiss_c, self.sige, self.ex, self.ey, ax, nx=nx, hcolor='r')
        # GET THE APERTURE AND PLOT IT
        new_pos_x, aper_neat_x = acc_phy_lib_elias.get_apertures_real(self.twiss_c.iloc[1:-2, :])
        acc_phy_lib_elias.plot_aper_real(new_pos_x, aper_neat_x, ax)
        # ----------------------------------------------------------------

        ax.legend(['Injected beam', 'Circulating beam'], loc='upper right', facecolor='white', framealpha=0.8,
                  prop={'size': 18})
        plt.ylim(-0.1, 0.1)
        plt.xlim(40, 450)
        t1 = plt.text(300, -0.095,
                      '$d\\tau_{{general}} = {:.1f}$ ns\n$dt_{{c}} = {}$ ns'.format(self.dt_general, self.dtc),
                      fontsize=18, horizontalalignment='left')
        # t2 = plt.text(55, -0.08, '$\\bar{{x}} =$ {:.1e} m,  $\\bar{{p}}_{{x}} =$ {:.1e} GeV/c'.format(self.x_bar, self.px_bar), fontsize=12, horizontalalignment='left')
        # t3= plt.text(55, -0.095, '$\\bar{{x}}_{{c}} =$ {:.1e} m,  $\\bar{{p}}_{{x, c}} =$ {:.1e} GeV/c'.format(self.x_bar_c, self.px_bar_c), fontsize=12, horizontalalignment='left')
        t1.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
        # t2.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
        # t3.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
        # print("MKP kicks injected beam: {}".format(self.mkp_kicks))
        # print("MKP kicks circulating beam: {}".format(self.mkp_kicks_c))