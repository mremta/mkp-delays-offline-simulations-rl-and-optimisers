import io
import numpy as np
import matplotlib.pyplot as plt

# plot sin wave
fig = plt.figure(figsize=(5,5), dpi=180)

ax = fig.add_subplot(111)
plt.axis('off')
x = np.linspace(-np.pi, np.pi)

ax.set_xlim(-np.pi, np.pi)
ax.set_xlabel("x")
ax.set_ylabel("y")
ax.plot(x, np.sin(x), label="sin", color='black')

plt.show()

# define a function which returns an image as numpy array from figure
def get_img_from_fig(fig, dpi=180):
    buf = io.BytesIO()
    fig.savefig(buf, format="raw", dpi=dpi)
    buf.seek(0)
    img_arr = np.reshape(np.frombuffer(buf.getvalue(), dtype=np.uint8),
                         newshape=(int(fig.bbox.bounds[3]), int(fig.bbox.bounds[2]), -1))
    buf.close()

    return img_arr

# you can get a high-resolution image as numpy array!!
plot_img_np = get_img_from_fig(fig)

print(np.shape(plot_img_np))

plt.imshow(plot_img_np, interpolation='nearest')
plt.show()

print(np.shape(np.array(plot_img_np)))

a = np.array([[1,1], [1,1]])
print(np.shape(a))

b = np.array([a, ])
print(np.shape(b))