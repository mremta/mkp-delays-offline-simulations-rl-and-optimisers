import numpy as np
import matplotlib.pyplot as plt
import pickle
import time

import gym
import os

from stable_baselines3 import PPO, A2C
from stable_baselines3 import TD3, SAC
from stable_baselines3.common.noise import NormalActionNoise
from stable_baselines3.common.env_checker import check_env
from stable_baselines3.common.callbacks import EvalCallback
from mkpdelays_env_RL_delta_action_v17 import MKPOptEnv  # import environment using relative action

n_steps = 1000000
dtc = 200
algorithm = 'PPO'
max_rise_time_change = 0.2
run_nr = np.arange(119, 123)
seed = 10815


def performance_evaluation(model, action_scaling=0.005, num_episodes=10, seed=10815):
    env = MKPOptEnv(dtc=dtc, action_scaling=action_scaling, max_rise_time_change=max_rise_time_change, max_steps=1000,
                    verbose=True)
    env.seed(seed)

    # Evaluate the agent for the specified number of episodes
    for episode in range(num_episodes):
        # Reset the environment for a new episode
        obs = env.reset()

        # Run the episode until termination
        while True:
            # Use the pretrained RL agent to select actions
            action, _ = model.predict(obs, deterministic=True)

            # Take the selected action in the environment
            obs, reward, done, _ = env.step(action)

            # Break the loop if the episode is done
            if done:
                break

    return env.episode_data


performance_metrics = {'steps_until_stable': [],
                       'final_reward': [],
                       'episode_data': []
                       }

step_sizes = [0.005, 0.01, 0.015, 0.02]
step_sizes = [0.015]

count = 0
for nr in run_nr:
    save_str = "{}_run_{}".format(algorithm, nr)

    path = "plots_and_data_delta_action/experiment_{}/model_{}_steps_{}_ns_dtc_{}.zip".format(
            nr, n_steps, dtc, save_str)

    model = PPO.load(path)

    episode_data = performance_evaluation(model,
                                          action_scaling=step_sizes[count],
                                          num_episodes=100,
                                          seed=seed)
    episode_rewards = episode_data['all_rewards']

    steps_until_stable = []

    for episode in episode_rewards:
        # Calculate the threshold value based on 2% of the maximum value
        threshold = 0.02 * np.max(episode)

        is_stable = False

        # Iterate through the array to find the first index satisfying both conditions
        for i, value in enumerate(episode):
            if all(np.abs((x - np.max(episode))) <= threshold for x in episode[i:]):
                steps_until_stable.append(i)
                is_stable = True
                break

        if not is_stable:
            steps_until_stable.append(len(episode))

    performance_metrics['steps_until_stable'].append(steps_until_stable)
    performance_metrics['final_reward'].append(episode_data['final_reward'])
    performance_metrics['episode_data'].append(episode_data)

    count += 1

# Serialize and save data

with open("plots_and_data_delta_action/performance_eval_default_ppo.pickle", "wb",
          ) as handle:
    pickle.dump(
            performance_metrics, handle, protocol=pickle.HIGHEST_PROTOCOL
                )
'''
with open("plots_and_data_delta_action/ppo_15ns.pickle", "wb",
          ) as handle:
    pickle.dump(
            performance_metrics, handle, protocol=pickle.HIGHEST_PROTOCOL
                )
'''
