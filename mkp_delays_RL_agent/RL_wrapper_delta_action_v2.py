"""
RL test wrapper to create fake MKP delays environment, testing out an RL agent with Stable Baselines 3
--> this versions uses environment that implements the relative action

v2 is tailored to the setting without a reward target but rather a fixed amount of steps
"""

import numpy as np
import matplotlib.pyplot as plt
import pickle
import time

import gym
import os

from stable_baselines3 import PPO, A2C
from stable_baselines3 import TD3, SAC
#from sb3_contrib import TQC
from stable_baselines3.common.noise import NormalActionNoise
from stable_baselines3.common.env_checker import check_env
# from stable_baselines3.common.vec_env import VecNormalize
# from stable_baselines3.common.env_util import make_vec_env
from stable_baselines3.common.callbacks import EvalCallback
from mkpdelays_env_RL_delta_action_v17 import MKPOptEnv  # import environment using relative action
from custom_feature_extractor import CustomCombinedExtractor
from stable_baselines3 import HerReplayBuffer
from stable_baselines3.her.goal_selection_strategy import GoalSelectionStrategy

# Initiate some values and flags
save_plots = True
n_steps = 1000000
dtc = 200
max_rise_time_change = 0.2
check_env_flag = False
algorithm = 'PPO'
run_nr = 127
seed = 144
save_str = "{}_run_{}".format(algorithm, run_nr)

os.mkdir('plots_and_data_delta_action/experiment_{}'.format(run_nr))

# 'dict(net_arch=[400, 300])'

# Initiate environment
env = MKPOptEnv(dtc=dtc, max_rise_time_change=max_rise_time_change, max_steps=1000, verbose=True, action_scaling=0.015)   # also include batch spacing time separation
env.seed(144)

if check_env_flag:
    check_env(env, warn=True)  # If the environment don't follow the interface, an error will be thrown

# Separate evaluation env
eval_env = MKPOptEnv(dtc=dtc, max_rise_time_change=max_rise_time_change, max_steps=1000, verbose=True, action_scaling=0.015)
# Use deterministic actions for evaluation
eval_callback = EvalCallback(eval_env,
                             best_model_save_path='plots_and_data_delta_action/experiment_{}/'.format(run_nr),
                             log_path='plots_and_data_delta_action/experiment_{}/'.format(run_nr),
                             eval_freq=5000,  # each eval_freq steps the agent is tested on the evaluation env
                             deterministic=True,
                             render=False)

# Perform a reset command
obs = env.reset()
#env.render()

#"""
# ------------------- DEFINE TRAINING ALGORITHM --------------------------------------
# Train the agent - TD3, here we use action noise
if algorithm == 'TD3':
    start = time.time()
    n_actions = env.action_space.shape[-1]
    action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.05 * np.ones(n_actions))

    replay_buffer_kwargs = dict(handle_timeout_termination=False)

    '''
    # Use custom feature extractor
    policy_kwargs = dict(
        features_extractor_class=CustomCombinedExtractor
    )
    
    model = TD3("MultiInputPolicy",
                env,
                verbose=1,
                seed=seed,
                action_noise=action_noise,
                buffer_size=500000,
                learning_starts=1000,
                policy_kwargs=policy_kwargs)
    '''
    '''
    # Model call with HER (Hindsight Experience Replay)
    model = TD3("MultiInputPolicy",
                env,
                verbose=1,
                seed=seed,
                action_noise=action_noise,
                buffer_size=500000,
                learning_starts=1000,
                replay_buffer_class=HerReplayBuffer,
                replay_buffer_kwargs=dict(
                    n_sampled_goal=4,
                    goal_selection_strategy='future')
                )
    '''
    model = TD3("MlpPolicy",
                env,
                verbose=1,
                seed=seed,
                action_noise=action_noise,
                buffer_size=500000,
                learning_starts=1000,
                replay_buffer_kwargs=replay_buffer_kwargs,
                optimize_memory_usage=True  # slower, but requires less RAM
                )

    model.learn(total_timesteps=n_steps, callback=eval_callback, log_interval=50)
    end = time.time()

# Train the agent - A2C, and clock the training
if algorithm == 'A2C':
    start = time.time()
    model = A2C('MlpPolicy', env, seed=seed, verbose=1).learn(n_steps)
    end = time.time()

# Train agent PPO
if algorithm == 'PPO':
    kwargs = {
        "n_steps": 2048,
        "batch_size": 64,
        "gamma": 0.9,
        "learning_rate": 0.0003,
        "ent_coef": 0.0,
        "clip_range": 0.2,
        "n_epochs": 10,
        "gae_lambda": 1.0,
        "max_grad_norm": 0.5,
        "vf_coef": 0.5,
        # "sde_sample_freq": sde_sample_freq,
        "policy_kwargs": dict(
            # log_std_init=log_std_init,
            net_arch=dict(pi=[128], vf=[128]),
        ),
    }


    start = time.time()
    # model = PPO(policy='MlpPolicy', env=env, seed=seed, verbose=1, **kwargs).learn(total_timesteps=n_steps)
    model = PPO(policy='MlpPolicy', env=env, seed=seed, verbose=1).learn(total_timesteps=n_steps)
    end = time.time()
# ------------------------------------------------------------------------------------

# Train agent SAC
if algorithm == 'SAC':
    policy_kwargs = dict(net_arch=[256, 256])

    start = time.time()
    model = SAC('MlpPolicy',
                env,
                gamma=0.99,
                seed=seed,
                verbose=1,
                buffer_size=1000000,
                learning_starts=1000,
                target_entropy=-8,
                use_sde=True,
                policy_kwargs=policy_kwargs)

    model.learn(total_timesteps=n_steps, callback=eval_callback, log_interval=50)
    end = time.time()
# ------------------------------------------------------------------------------------

# Train agent TQC
if algorithm == 'TQC':
    policy_kwargs = dict(net_arch=dict(pi=[256, 256], qf=[512, 512, 512]),
                         n_critics=5)

    start = time.time()
    model = TQC('MlpPolicy',
                env,
                gamma=0.99,
                seed=seed,
                verbose=1,
                buffer_size=1000000,
                learning_starts=1000,
                target_entropy=-8,
                use_sde=True,
                policy_kwargs=policy_kwargs)

    model.learn(total_timesteps=n_steps, callback=eval_callback, log_interval=50)
    end = time.time()
# ------------------------------------------------------------------------------------


print("\nTraining with {} steps lasted for {:.2f} seconds!\n".format(n_steps, end - start))


#Also plot reward over time
fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.plot(env.episode_data["reward_sum"], 'o', markersize=1)
ax.set_xlabel("Training episode")
ax.set_ylabel("Reward")
if save_plots:
    fig1.savefig('plots_and_data_delta_action/experiment_{}/agent_reward_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)


# calculate running average, starting at index 1 (not a zero)
def running_mean(x, N):
    cumsum = np.cumsum(np.insert(x, 0, 0))
    return (cumsum[N:] - cumsum[:-N]) / float(N)


# calculate the best achieved reward for each episode
max_reward = []
for ele in env.episode_data["all_rewards"]:
    max_reward.append(np.max(ele))


# Plot final reward of each episode
fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.plot(max_reward, label="maximum reward")
ax.plot(env.episode_data["initial_reward"][2:], label="initial reward")
ax.plot(env.episode_data["final_reward"], label="final reward")
ax.set_xlabel("Training episode")
ax.set_ylabel("Reward")
ax.legend()

fig1.savefig('plots_and_data_delta_action/experiment_{}/agent_final_reward_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)


'''
# plot resulting beam oscillations
fig2 = plt.figure(figsize=(10,7))
env.mkp_delays.plot_oscillations(fig2)
if save_plots:
    fig2.savefig('plots_and_data_delta_action/experiment_{}/injection_oscillations_{}_steps_{}_ns_dtc_{}.png'.format(
    run_nr, n_steps, dtc, save_str), dpi=250)
'''


# Save model
if save_plots:
    model.save("plots_and_data_delta_action/experiment_{}/model_{}_steps_{}_ns_dtc_{}".format(
        run_nr, n_steps, dtc, save_str))

# Serialize and save data
if save_plots:
    with open(
        "plots_and_data_delta_action/experiment_{}/episode_data_{}_steps_{}_ns_dtc_{}.pickle".format(
            run_nr, n_steps, dtc, save_str
        ),
        "wb",
    ) as handle:
        pickle.dump(
            env.episode_data, handle, protocol=pickle.HIGHEST_PROTOCOL
        )
#"""