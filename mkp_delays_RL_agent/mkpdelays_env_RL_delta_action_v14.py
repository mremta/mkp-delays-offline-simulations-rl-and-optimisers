from abc import abstractmethod
from typing import Optional

import gym
from gym import spaces
from gym import error
import numpy as np

# import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
# import sys
# from cpymad.madx import Madx
import typing as t
# import datetime
# from math import exp

# to access the parameter control
# import logging  # to release log messages
# from pyjapc import PyJapc
# from cernml.coi import OptEnv, register, Machine
# from cernml.coi import cancellation
from fake_mkp_delays_RL_v3 import FakeMKPdelays


class MKPOptEnv(gym.Env):
    r"""A goal-based environment. It functions just as any regular Gym environment but it
    imposes a required structure on the observation_space. More concretely, the observation
    space is required to contain at least three elements, namely `observation`, `desired_goal`, and
    `achieved_goal`. Here, `desired_goal` specifies the goal that the agent should attempt to achieve.
    `achieved_goal` is the goal that it currently achieved instead. `observation` contains the
    actual observations of the environment as per usual.

    - :meth:`compute_reward` - Externalizes the reward function by taking the achieved and desired goal, as well as extra information. Returns reward.
    - :meth:`compute_terminated` - Returns boolean termination depending on the achieved and desired goal, as well as extra information.
    - :meth:`compute_truncated` - Returns boolean truncation depending on the achieved and desired goal, as well as extra information.
    """

    def __init__(
            self,
            japc=None,
            use_surrogate=True,
            cancellation_token=None,
            optics="q20",
            dtc=500,
            objective=-5e-4,  # starting objective to make learning easier / harder
            reward_dangling=False,
            sigmoid_start=-5.,
            max_steps=20,
            parameters=8,
            run_nr=100
    ):
        self.objective = objective

        self.is_surrogate = use_surrogate
        self.mkp_delays = None
        self.time_data = np.genfromtxt('data_and_sequences/sps_lhcpilot_time_data.txt')
        self.sigmoid_start = sigmoid_start
        self.dtc = dtc

        self.current_episode = 0
        self.max_steps = max_steps
        self.current_step = 0
        self.reward_sum = 0
        self.step_size = 20

        self.dof = parameters
        self.off_set = np.zeros(parameters)

        self.figure = None
        self.render_mode = 'matplotlib_figures'
        self.run_nr = run_nr

        self.episode_data = {
            'reward_sum': []
        }

        high = np.ones(parameters)
        low = -1 * np.ones(parameters)

        self.action_space = spaces.Box(low=low, high=high, dtype=np.float32)
        self.observation_space = spaces.Dict({
            'observation': spaces.Box(low=-1, high=1, shape=(515,), dtype=np.float64),
            'achieved_goal': spaces.Box(low=-1, high=1, shape=(1,), dtype=np.float64),
            'desired_goal': spaces.Box(low=-1, high=1, shape=(1,), dtype=np.float64)
        })

    def reset(self, seed: Optional[int] = None, options = None):
        print("\n\nResetting environment...\n\n")
        self.current_step = 0
        self.reward_sum = 0
        self.reset_MKP_delays()   # reset randomization of waveforms

        # Reset off_set
        self.off_set = np.zeros(self.dof)

        # Randomization of the initial delays
        if self.is_surrogate:
            dtau0 = np.zeros(self.dof)
            for i in range(0, self.dof):
                dtau0[i] = np.random.uniform(-30, 30) - 200

            self.off_set = dtau0

        print("reset action: {}".format(self.off_set))

        state = self._observe(self.off_set)

        # plot waveforms before optimization, every 50 episodes
        if (self.current_episode + 1) % 50 == 0:
            fig = self.render("matplotlib_figures")
            fig[0].savefig(
                f'plots_and_data_delta_action/experiment_{self.run_nr}/waveforms_episode_{(self.current_episode + 1)}_start.png')

        return state, None

    @abstractmethod
    def compute_reward(self, achieved_goal, desired_goal, info = None):
        """Compute the step reward. This externalizes the reward function and makes
        it dependent on a desired goal and the one that was achieved. If you wish to include
        additional rewards that are independent of the goal, you can include the necessary values
        to derive it in 'info' and compute it accordingly.

        Args:
            achieved_goal (np.array): the goal that was achieved during execution
            desired_goal (np.array): the desired goal that we asked the agent to attempt to achieve
            info (dict): an info dictionary with additional information

        Returns:
            float: The reward that corresponds to the provided achieved goal w.r.t. to the desired
            goal. Note that the following should always hold true:

                ob, reward, terminated, truncated, info = env.step()
                assert reward == env.compute_reward(ob['achieved_goal'], ob['desired_goal'], info)
        """
        return achieved_goal - desired_goal

    @abstractmethod
    def compute_terminated(self, achieved_goal, desired_goal, info = None):
        """Compute the step termination. Allows to customize the termination states depending on the
        desired and the achieved goal. If you wish to determine termination states independent of the goal,
        you can include necessary values to derive it in 'info' and compute it accordingly. The envirtonment reaches
        a termination state when this state leads to an episode ending in an episodic task thus breaking .
        More information can be found in: https://farama.org/New-Step-API#theory

        Termination states are

        Args:
            achieved_goal (np.array): the goal that was achieved during execution
            desired_goal (np.array): the desired goal that we asked the agent to attempt to achieve
            info (dict): an info dictionary with additional information

        Returns:
            bool: The termination state that corresponds to the provided achieved goal w.r.t. to the desired
            goal. Note that the following should always hold true:

                ob, reward, terminated, truncated, info = env.step()
                assert terminated == env.compute_terminated(ob['achieved_goal'], ob['desired_goal'], info)
        """
        return achieved_goal >= desired_goal

    @abstractmethod
    def compute_truncated(self, achieved_goal, desired_goal, info = None):
        """Compute the step truncation. Allows to customize the truncated states depending on the
        desired and the achieved goal. If you wish to determine truncated states independent of the goal,
        you can include necessary values to derive it in 'info' and compute it accordingly. Truncated states
        are those that are out of the scope of the Markov Decision Process (MDP) such as time constraints in a
        continuing task. More information can be found in: https://farama.org/New-Step-API#theory

        Args:
            achieved_goal (np.array): the goal that was achieved during execution
            desired_goal (np.array): the desired goal that we asked the agent to attempt to achieve
            info (dict): an info dictionary with additional information

        Returns:
            bool: The truncated state that corresponds to the provided achieved goal w.r.t. to the desired
            goal. Note that the following should always hold true:

                ob, reward, terminated, truncated, info = env.step()
                assert truncated == env.compute_truncated(ob['achieved_goal'], ob['desired_goal'], info)
        """
        return achieved_goal < desired_goal

    # Initiate MKP delays object
    def reset_MKP_delays(self):
        if self.is_surrogate:
            self.mkp_delays = FakeMKPdelays(
                dtc=self.dtc,
                sigmoid_start=self.sigmoid_start,
                randomize_waveforms=False
            )
            print("Resetting to fake MKP delays (simulated data)")

    def _observe(self, off_set):
        raw_state = []
        dtau = off_set
        # Check at what index to evaluate the MKP kick data
        t = self.mkp_delays.ref_timestamp  # reference time for kick data evaluation
        for i in range(self.dof):  # 8 switches in total to iterate over
            ts = t - (self.dtc / 2) - dtau[i]
            t_ind = np.argwhere(self.time_data == (ts - (ts % 2)))  # find closest even index corresponding to this time
            min_index = t_ind.item() - 128
            max_index = t_ind.item() + 128
            kick = self.mkp_delays.mkp_waveforms[(i * 2)][
                   min_index:max_index:4].flatten()  # only append every second waveform (one per switch)
            raw_state.append(np.array(kick))

        # include 10 last changes in the objective function as state
        # self.history_objective = np.roll(self.history_objective, 1, axis=0)

        # self.history_objective[0] = loss - self.history_objective[1]
        beampos = self.mkp_delays.get_positions((off_set + self.mkp_delays.ref_timestamp))

        raw_state.append(np.array(beampos))  # add the two oscillation amplitude positions
        raw_state.append(np.array([self.current_step / self.max_steps]))
        # raw_state.append(np.array(self.off_set))  # add sum of actions (+ initial delays = absolute action)
        # raw_state.append(self.history_objective)  # add the changes in the objective function
        raw_state = np.array(raw_state, dtype=object).flatten()
        state = np.concatenate(raw_state, axis=0)  # concatenate into 1D array

        x1bar = (
                        abs(np.max(beampos[0])) + abs(np.min(beampos[0]))
                ) / 2
        x2bar = (
                        abs(np.max(beampos[1])) + abs(np.min(beampos[1]))
                ) / 2
        achieved_goal = -1 * (x1bar ** 2 + x2bar ** 2 + (x1bar - x2bar) ** 2)

        obs = {'observation': np.concatenate(raw_state, axis=0),
               'achieved_goal': np.array([achieved_goal]),
               'desired_goal': np.array([self.objective])}

        return obs

    def step(self, delta_action):
        self.current_step += 1

        self.off_set += delta_action * self.step_size

        obs = self._observe(self.off_set)
        reward = self.compute_reward(obs['achieved_goal'], obs['desired_goal'])
        self.reward_sum += reward

        # Evaluate whether episode has finished or not
        terminated = reward >= 0
        truncated = False

        if not terminated and self.current_step >= self.max_steps:
            truncated = True

        if terminated or truncated:
            self.current_episode += 1

            # plot waveforms after optimization every 50 episodes
            if self.current_episode % 50 == 0:
                fig = self.render("matplotlib_figures")
                fig[0].savefig(
                    f'plots_and_data_delta_action/experiment_{self.run_nr}/waveforms_episode_{self.current_episode}_end.png')

            # also append the various data from the episode
            self.episode_data["reward_sum"].append(self.reward_sum)  # summed reward per episode

            print("\n\n------ EPISODE {} ended after {} steps with final loss: {:.3e} -------- \n\n".format(
                self.current_episode, self.current_step, obs['achieved_goal'].item()))

        return obs, reward, terminated, truncated, {}

    def render(self, mode: str = "human") -> t.Any:
        if mode == "human":
            _, axes = plt.subplots()
            self.update_axes(axes)
            plt.show()
            return None
        if mode == "matplotlib_figures":
            if self.figure is None:
                self.figure = plt.figure()
                axes = self.figure.subplots()
            else:
                [axes] = self.figure.axes
            self.update_axes(axes)
            return [self.figure]
        return super().render(mode)

    # Method to clear japc subscriptions
    def close(self):
        return None

    # Update plot axes for bpm positions and rewards
    def update_axes(self, axes: Axes) -> None:
        """Render this problem into the given axes."""
        _ylim = axes.get_ylim()
        axes.clear()

        dtau = self.off_set
        # Check at what index to evaluate the MKP kick data
        t = self.mkp_delays.ref_timestamp  # reference time for kick data evaluation

        t_inds = []
        for i in range(8):  # 8 switches in total to iterate over
            ts = t - (self.dtc / 2) - dtau[i]
            t_ind = np.argwhere(self.time_data == (
                       ts - (ts % 2)))  # find closest even index corresponding to this time
            t_inds.append(t_ind)

        switch_count = 0
        for i in range(16):
            if (((i) % 2) == 0) and (i != 0):  # after every two steps, change electrical switch
                 switch_count += 1
            min_index = t_inds[switch_count].item() - 128
            max_index = t_inds[switch_count].item() + 128
            t_center = np.argwhere(self.time_data == (t - (self.dtc / 2) - ((t - (self.dtc / 2)) % 2))).item()

            axes.plot(
                self.mkp_delays.time_data[(t_center - 128):(t_center + 128)],
                 1000 * self.mkp_delays.mkp_waveforms[i][min_index:max_index],
                 label="_nolegend_",
                )
        # axes.set_xlim(4e3, 6e3)
        axes.axvline(x=self.mkp_delays.ref_timestamp, color='b', ls='--',
                     label='Injected beam')  # why does the GUI crash when we include these=?
        axes.axvline(x=(self.mkp_delays.ref_timestamp - self.dtc), color='c', ls='--', label='Circulating beam')
        axes.set_ylabel("Kick [mrad]")
        axes.set_xlabel("Time [ns]")
        axes.legend(loc=4)
        axes.grid()
