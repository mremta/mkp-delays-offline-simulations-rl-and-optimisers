"""
Optimisation environment to find the ideal time shift vector dtau for the electrical switches
of the MKPs in the SPS injection in order to minimise the horizontal beam oscillations in the SPS

This versions uses the fake_mkp_delays_RL module to simulate the waveforms and the oscillation amplitude arising from it
--> _take_action() method is based on relative action, not on absolute action

Self-contained environment, with all sequence and IC files loaded from the same repository
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
import sys
from cpymad.madx import Madx
from gym import spaces
import typing as t
import datetime
from math import exp

# to access the parameter control
import logging  # to release log messages
from pyjapc import PyJapc
from cernml.coi import OptEnv, register, Machine
from cernml.coi import cancellation
from fake_mkp_delays_RL_v2 import FakeMKPdelays  # import version for the RL agent.

MODULE_NAME = "mkpdelays"


class MKPOptEnv(OptEnv):

    # Load optics, voltage-to-kick converted data, and corresponding time data
    def __init__(
            self,
            japc=None,
            use_surrogate=True,
            mkp_index=None,
            # numpy array containing the MKP indices to optimize specifically, otherwise optimize for all
            ts_lower=None,
            ts_upper=None,
            bunch_index_width=1,  # default value 1, if no bunch train
            acqStamp=3415,  # for LHCINDIV
            cancellation_token=None,
            optics="q20",
            dtc=500,
            objective=-5e-4,  # starting objective to make learning easier / harder, final objective is self.final_objective
            reward_dangling=False,
            sigmoid_start=-5.,
            run_nr=100
    ):
        self.run_nr = run_nr
        self.is_surrogate: bool = use_surrogate  # use simulated waveform data (True) or real data (False)
        self.optics = optics
        self.delta_t0 = (
            4900  # general starting time shift of injected beam in ns, for retrieved historical data
        )
        self.dtc = dtc  # time shift in ns between injected batch and circulating batch in the SPS
        self.t = self.delta_t0  # to keep track of general time stamp
        self.tc = self.t - self.dtc
        self.mkp_index = mkp_index
        self.bunch_index_width = (
            bunch_index_width  # how many bunches per train
        )
        self.acqStamp = acqStamp
        # Set cancellation token, to reset after optimisation is done:
        if cancellation_token is None:
            cancellation_token = cancellation.Token()
        self.token = cancellation_token
        self.objective = objective  # standard objective to fulfil, reward is larger than objective, then episode is successful
        self.final_objective = -5e-4
        self.max_steps = 100  # maximum episode length
        self.sigmoid_start = sigmoid_start  # for randomization of rise time, where to start the sigmoid function interval
        self.reward_dangling = reward_dangling
        self.time_data = np.genfromtxt('data_and_sequences/sps_lhcpilot_time_data.txt')
        self.state = None
        self.history_injected = np.zeros(10)
        self.history_circulating = np.zeros(10)

        # Initiate JAPC if not given
        if not self.is_surrogate:
            # Check data type and select data source
            if japc is None:
                japc = PyJapc(
                    "SPS.USER.LHCINDIV", noSet=True
                )
            self.japc = japc
            logging.info("REAL MKP delays")

        # -------------------- PART TO TEST WHEN REAL_MKP_DELAY CLASS IS TESTED ------------------------------
        # Define the process variables for all MKP switches, and the general time shift
        self.mkpAcquisition = "MKP.BA1.F3.PFN."
        self.all_vars = []
        for i in range(8):
            self.all_vars.append(
                "{}{}/ExpertSettingDevice#arrG2MainSwitchFineTimingDelay".format(
                    self.mkpAcquisition, i + 1
                )
            )
        self.all_vars.append("MKP.BA1.F3.CONTROLLER/Setting#arrKickDelay")
        # -----------------------------------------------------------------------------------------------------

        # IF MKP INDEX IS USED, DROP VARIABLES OF MKPS NOT INVOLVED
        if self.mkp_index is not None:
            self.all_vars = [self.all_vars[j] for j in self.mkp_index]
            print(
                "\nChanging delays of MKPs with index {}: \n".format(
                    self.mkp_index
                )
            )
            if 8 in self.mkp_index:
                print(
                    "--> index 8 included: also optimising for GENERAL delay...\n"
                )

        self.dof = len(self.all_vars)
        self.action_scaling = 1  # if needed to scale the action

        # For first action, use example data if surrogate, otherwise present state from Japc
        if use_surrogate:
            dtau0 = np.zeros(self.dof)
            if (
                    self.mkp_index is None or 8 in self.mkp_index
            ):  # check if all indices are given, or if the general time shift is given in the index
                dtau0[-1] = self.delta_t0
            self.x0_action = dtau0
        else:
            raw_x0action = self.japc.getParam(
                self.all_vars
            )  # raw action in template format
            print("\nRaw x0 action is {}\n".format(raw_x0action))
            self.x0_action = []
            # Convert field template to numpy array of relevant values
            for i in range(self.dof - 1):
                self.x0_action.append(
                    raw_x0action[i][0]
                )  # select first value for individual MKP switches

            # Check the last element, if it is general time shift or just another given MKP index
            if self.mkp_index is None or 8 in self.mkp_index:
                self.x0_action.append(
                    raw_x0action[-1][1]
                )  # select relevant value (2nd) for the general time shift
            else:
                self.x0_action.append(
                    raw_x0action[-1][0]
                )  # in this case, no general time shift but just another single MKP index

        # We work with relative terms, so define initial normalized x0 action (off_set) as zeros
        self.off_set = np.zeros(self.dof)

        # Define limits for the individual MKP delays, and then for the general time shift
        # If no time individual time shifts are given, go for the standard individual timeshift of max 50 ns
        if ts_lower is None or ts_upper is None:
            self.limits = np.array(
                [
                    np.array(self.x0_action) - 100,  # individual lower
                    np.array(self.x0_action) + 100,  # individual upper
                ]
            )
            self.limits[0, -1] = self.x0_action[-1] - 750  # General lower
            self.limits[1, -1] = self.x0_action[-1] + 750  # General upper
        else:
            self.limits = np.array(
                [
                    np.array(ts_lower),
                    np.array(ts_upper),
                ]
            )

        print("\nLimits are: {}".format(self.limits))

        # Calculate normalised action
        self.x0_action_norm = self.norm_data(self.x0_action)
        print("\nx0 action is: {}\n".format(self.x0_action))

        # Create mkp delay device object
        self.reset_MKP_delays()

        # Observe initial beam position
        self.beampos = self.mkp_delays.get_positions(self.x0_action)

        # Attributes for reinforcement learning agent
        self.max_episode_length = 100
        self.curr_episode = 0
        self.action_episode_memory = []
        self.current_step = 0
        self.is_finalized = False
        self.total_step_counter = 0  # to count number of total steps agent is acting upon
        self.success_count = 0  # how many times in a row an episode is successful

        # create dictionary for the episode data
        self.episode_data = {
            "length": [],
            "success": [],
            "actions": [],
            "boundary_violation": [],
            "real_actions": [],
            "all_rewards": [],
            "final_reward": [],
            "reward_sum": [],
            "timestamps": [],
            "states": [],
            "mkp_waveform_kick_data": []
        }

        # initiate action space for delta action --> test with 10% of absolute action space
        high = 0.1 * np.ones(self.dof)
        low = -0.1 * np.ones(self.dof)
        self.action_space = spaces.Box(low=low, high=high, dtype=np.float32)
        self.optimization_space = self.action_space

        # state space => also called observation space
        high = self.limits[1, :]
        low = self.limits[0, :]
        print("\nLower limits: {},\nHigher limits: {}\n".format(low, high))
        init_state = self._observe()
        self.observation_space = spaces.Box(-1., 1., shape=(len(init_state),), dtype=np.float64)

        self.figure = None  # enable space for figure if needed


    # Initiate MKP delays object
    def reset_MKP_delays(self):
        if self.is_surrogate:
            self.mkp_delays = FakeMKPdelays(
                self.limits,
                dtc=self.dtc,
                sigmoid_start=self.sigmoid_start,
                randomize_waveforms=False
            )
            print("Resetting to fake MKP delays (simulated data)")
        else:
            print("Real MKP delays")
            self.mkp_delays = RealMKPdelays(
                self.japc,
                self.limits,
                self.mkp_index,
                self.bunch_index_width,
                self.acqStamp,
                self.token,
            )

    # If pseudorandom numbers are needed
    def seed(self, seed):
        np.random.seed(seed)

    # Step method for optimiser
    def step(self, delta_action):
        self.current_step += 1
        self.state, self.reward = self._take_action(delta_action)  # take actions

        # Add penalty if action is not okay
        #if not self.is_action_ok:
        #    self.reward = -10.

        # Log the data
        self.log_data["actions"].append(self.off_set)
        self.log_data["real_actions"].append(self.inv_norm_data(self.off_set))
        # self.log_data["mkp_waveform_kick_data"].append(self.mkp_delays.mkp_kick_data)  # not needed after each step
        self.log_data["timestamp"].append(str(datetime.datetime.now()).replace(" ", "_").replace(":", "-"))
        self.log_data["out"].append(self.reward)
        self.log_data["state"].append(self.state)

        # Evaluate whether episode has finished or not
        success = self.reward > self.objective

        # Finalize episode if action is not okay // disabled, just give penalty and prevent action
        # possibility to iteratively decrease the objective for episode to finish, to achieve better objectives
        if success:
            self.success_count += 1
            if self.reward_dangling:
                self._reward_dangling()  # increase objective to find reasonable goal
        #if self.success_count >= 10:
        #    self.objective = min(0.5 * self.objective, self.final_objective)  # decrease objective by 50%
        #    print("New objective is: {:.3e}".format(self.objective))
        #    self.success_count = 0  # reset

        if success or self.current_step >= self.max_steps:
            self.curr_episode += 1

            # plot waveforms after optimization every 50 episodes
            if self.curr_episode % 50 == 0:
                fig = self.render("matplotlib_figures")
                fig[0].savefig(
                    f'plots_and_data_delta_action/experiment_{self.run_nr}/waveforms_episode_{self.curr_episode}_end.png')

            done = True
            self.is_finalized = True
            self.reset_MKP_delays()  # reset the MKP delays if episode is done, to reset randomization of waveforms
            # also append the various data from the episode
            self.episode_data["length"].append(self.current_step)
            self.episode_data["success"].append(success)
            self.episode_data["actions"].append(self.log_data["actions"])
            self.episode_data["boundary_violation"].append(self.log_data["boundary_violation"])
            self.episode_data["real_actions"].append(self.log_data["real_actions"])
            self.episode_data["all_rewards"].append(self.log_data["out"])
            self.episode_data["final_reward"].append(self.log_data["out"][-1])  # final reward obtained
            self.episode_data["reward_sum"].append(np.sum(self.log_data["out"]))  # summed reward per episode
            #self.episode_data["mkp_waveform_kick_data"].append(
            #    self.mkp_delays.mkp_kick_data)  # we randomize per episode, so log this waveform data

            print("\n Final real action: {}\n".format(self.log_data["real_actions"][-1]))

            print("\n\n------ EPISODE {} with reward sum: {:.3e} -------- \n\n".format(
                self.curr_episode, np.sum(self.log_data["out"])
            ))

        return self.state, self.reward, self.is_finalized, {}

    # Method to optimize single objective function
    def compute_single_objective(self, action):
        _, reward, _, _ = self.step(action)

        return -reward

    # Method to get initial parameters
    def get_initial_params(self) -> t.Any:
        return self.norm_data(self.x0_action)

    # Reset the state of the environment and returns an initial observation.
    # Returns observation (object): the initial observation of the space.
    def reset(self):
        print("\n\nResetting environment...\n\n")
        self.is_finalized = False
        self.current_step = 0
        self.done = False

        # self.reset_MKP_delays()   # reset randomization of waveforms

        # Keep logged history in memory to pickle, for each episode
        self.log_data = {
            "actions": [],
            "real_actions": [],
            "boundary_violation": 0,
            "bpm_pos": [],
            "out": [],
            "mkp_waveform_kick_data": [],
            "mkp_waveform_time_data": [],
            "state": [],
            "timestamp": [],
            "opt_variables": []
        }
        self.log_data["mkp_waveform_time_data"].append(
            self.mkp_delays.time_data)  # store waveform time data (same for all)
        self.log_data["opt_variables"].append(self.all_vars)  # store what variables were optimised for

        # Also initialize the absolute action
        self.off_set = np.zeros(self.dof)

        # Randomization of the initial delays (x0 action)
        if self.is_surrogate:
            self.delta_t0 = np.random.normal(4700, 100)

            self.t = self.delta_t0  # to keep track of general time stamp
            self.tc = self.t - self.dtc

            dtau0 = np.zeros(self.dof)
            for i in range(0, 8):
                dtau0[i] = np.random.normal(0, 10)
            if (
                    self.mkp_index is None or 8 in self.mkp_index
            ):  # check if all indices are given, or if the general time shift is given in the index
                dtau0[-1] = self.delta_t0

            self.x0_action = dtau0

            self.limits = np.array(
                [
                    np.array(self.x0_action) - 100,  # individual lower
                    np.array(self.x0_action) + 100,  # individual upper
                ]
            )
            self.limits[0, -1] = self.x0_action[-1] - 750  # General lower
            self.limits[1, -1] = self.x0_action[-1] + 750  # General upper

        print("reset action: {}".format(self.x0_action))

        state, _, _, _ = self.step(np.zeros(self.dof))

        # plot waveforms before optimization, every 50 episodes
        if (self.curr_episode + 1) % 50 == 0:
            fig = self.render("matplotlib_figures")
            fig[0].savefig(
                f'plots_and_data_delta_action/experiment_{self.run_nr}/waveforms_episode_{(self.curr_episode + 1)}_start.png')

        return state

    # Method to ensure action stays within normalised limits [-1, 1]
    def _check_action(self, action: np.ndarray):

        # print("Action is: {}".format(action))
        checked = np.all(action >= -1) and np.all(action <= 1)
        if not checked:
            logging.warning(
                "Action out of limits being clipped {}".format(
                    action
                )
            )
        clipped_action = np.clip(action, -1, 1)
        return checked, clipped_action

    # Method to take relative (delta) action with respect to absolute action
    def _take_action(self, delta_action):

        self.delta_action = delta_action

        # print("Delta_action {}\n Off_set: {}".format(self.delta_action, self.off_set))

        # add relative action to absolute action --> working in "deltas"!
        proposed_action = self.off_set + delta_action * self.action_scaling
        self.is_action_ok, action = self._check_action(proposed_action)
        self.off_set = action

        # Assign action to state and un-normalise the action
        # the action is normalised between 0 and 1 and state is the CTRL settings
        self.real_action = self.inv_norm_data(action)

        logging.info(
            "Action is OK: {}, {}".format(self.is_action_ok, self.real_action)
        )

        # Add total number of steps to total step counter
        self.total_step_counter += 1

        # Get reward from model
        reward, self.full_actions, loss = self._get_reward(self.real_action, proposed_action)

        # Add penalty to reward if action is not okay
        if not self.is_action_ok:
            self.log_data["boundary_violation"] += 1

        # Observe state from MKP waveform rise times
        state = self._observe()

        # print("State shape of waveform is: {}, dim {}".format(type(state), len(state)))
        print("Current step: {}, Reward: {:.3e}, Loss: {:.3e}".format(self.current_step, reward, loss))

        return state, reward

    # Method for observing current state
    def _observe(self):
        raw_state = []
        dtau = self.inv_norm_data(self.off_set)
        # Check at what index to evaluate the MKP kick data
        t = 2 * self.mkp_delays.ref_timestamp - dtau[-1]  # new time to evaluate the kick data
        t_inds = []
        for i in range(8):  # 8 switches in total to iterate over
            t_ind = np.argwhere(self.time_data == (t - dtau[i] - ((t - dtau[i]) % 2)))  # find closest even index corresponding to this time
            t_inds.append(t_ind)

        # Iterate over the modules, check the time shift for each switch, and add the kicks for the injected and circulating beam
        switch_count = 0
        for i in range(16):
            if (((i) % 2) == 0) and (i != 0):  # after every two steps, change electrical switch
                switch_count += 1
            min_index = t_inds[switch_count].item() - 140
            max_index = t_inds[switch_count].item() + 140
            kick = self.mkp_delays.mkp_waveforms[i][min_index:max_index:4].flatten()
            raw_state.append(np.array(kick))

        # include 10 last oscillation amplitude positions (injected and circulating) as state
        self.history_injected = np.roll(self.history_injected, 1, axis=0)
        self.history_circulating = np.roll(self.history_injected, 1, axis=0)

        self.history_injected[0] = np.array(self.beampos)[0]
        self.history_circulating[0] = np.array(self.beampos)[1]

        #raw_state.append(np.array(self.beampos))  # add the two oscillation amplitude positions
        raw_state.append(np.array(self.off_set))  # add sum of actions (+ initial delays = absolute action)
        raw_state.append(self.history_injected)  # add the injected oscillation amplitudes
        raw_state.append(self.history_circulating)  # add the circulating oscillation amplitudes
        raw_state = np.array(raw_state, dtype=object).flatten()
        state = np.concatenate(raw_state, axis=0)  # concatenate into 1D array

        return state

    # Method to extract losses depending on the normalised actions takens
    def _get_reward(self, real_action, proposed_action):

        logging.info("About to get BPM positions")
        self.beampos = self.mkp_delays.get_positions(real_action)
        self.log_data["bpm_pos"].append(self.beampos)
        x1bar = (
                        abs(np.max(self.beampos[0])) + abs(np.min(self.beampos[0]))
                ) / 2
        x2bar = (
                        abs(np.max(self.beampos[1])) + abs(np.min(self.beampos[1]))
                ) / 2
        sq_sum_bpm = x1bar ** 2 + x2bar ** 2 + (x1bar - x2bar) ** 2
        self.t = self.mkp_delays.t  # general mkp shift for injected beam
        self.tc = self.t - self.dtc  # general time shift for circulating beam

        action_penalty = 0
        for a in proposed_action:
            action_penalty += 1 / (1 + exp(-30 * (a - 1))) + \
                             1 / (1 + exp(30 * (a + 1)))

        reward = -1 * sq_sum_bpm - action_penalty

        #if reward > self.objective:
        #    reward += 10.

        # logging.info("Reward = {}".format(reward))

        return reward, real_action, sq_sum_bpm

    def _reward_dangling(self, gamma=0.99):
        self.objective = min(self.objective * gamma, self.final_objective)
        print('New objective: {}'.format(self.objective))

    # Method to render plots
    def render(self, mode: str = "human") -> t.Any:
        if mode == "human":
            _, axes = plt.subplots()
            self.update_axes(axes)
            plt.show()
            return None
        if mode == "matplotlib_figures":
            if self.figure is None:
                self.figure = plt.figure()
                axes = self.figure.subplots()
            else:
                [axes] = self.figure.axes
            self.update_axes(axes)
            return [self.figure]
        return super().render(mode)

    # Method to clear japc subscriptions
    def close(self):
        self.japc.clearSubscriptions()
        self.japc.stopSubscriptions()

    # Update plot axes for bpm positions and rewards
    def update_axes(self, axes: Axes) -> None:
        """Render this problem into the given axes."""
        _ylim = axes.get_ylim()
        axes.clear()

        if self.log_data["bpm_pos"] != []:
            dtau = self.inv_norm_data(self.off_set)
            # Check at what index to evaluate the MKP kick data
            t = 2 * self.mkp_delays.ref_timestamp - dtau[-1]  # new time to evaluate the kick data
            t_inds = []
            for i in range(8):  # 8 switches in total to iterate over
                t_ind = np.argwhere(self.time_data == (
                            t - dtau[i] - ((t - dtau[i]) % 2)))  # find closest even index corresponding to this time
                t_inds.append(t_ind)

            switch_count = 0
            for i in range(16):
                if (((i) % 2) == 0) and (i != 0):  # after every two steps, change electrical switch
                    switch_count += 1
                min_index = t_inds[switch_count].item() - 140
                max_index = t_inds[switch_count].item() + 140
                axes.plot(
                    self.mkp_delays.time_data[2310:2590],
                    1000 * self.mkp_delays.mkp_waveforms[i][min_index:max_index],
                    label="_nolegend_",
                )
        else:
            axes.plot([])
        # axes.set_xlim(4e3, 6e3)
        axes.axvline(x=self.mkp_delays.ref_timestamp, color='b', ls='--',
                     label='Injected beam')  # why does the GUI crash when we include these=?
        axes.axvline(x=(self.mkp_delays.ref_timestamp - self.dtc), color='c', ls='--', label='Circulating beam')
        axes.set_ylabel("Kick [mrad]")
        axes.set_xlabel("Time [ns]")
        axes.legend(loc=4)
        axes.grid()

    # Normalize data between -1 and 1, by performing this operation:
    """
    X is in the range [a, b]
    X-a is in the range [0, b-a], and delta = b-a
    (x-a)/delta is in the range [0, 1]
    2*(x-a)/delta is in the range [0, 2]
    2*(x-a)/delta -1 is in the range [-1, 1]
    """

    def norm_data(self, x_data):
        x_data_norm = np.zeros(len(x_data))
        delta = self.limits[1, :] - self.limits[0, :]
        x_data_norm = 2 * ((x_data - self.limits[0, :]) / delta) - 1

        return x_data_norm

    # Un-normalize normalized data
    def inv_norm_data(self, x_norm):
        x_data = np.zeros(len(x_norm))
        delta = self.limits[1, :] - self.limits[0, :]
        x_data = (x_norm + 1) / 2 * delta + self.limits[0, :]

        return x_data


class mkpOpt(MKPOptEnv):
    metadata = {
        "render.modes": ["human", "matplotlib_figures"],
        "cern.machine": Machine.SPS,
        "cern.japc": False,
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, use_surrogate=True, **kwargs)


class mkpOptReal(MKPOptEnv):
    metadata = {
        "render.modes": ["human", "matplotlib_figures"],
        "cern.machine": Machine.SPS,
        "cern.japc": True,
        "cern.cancellable": True,
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


register(
    "mkpOpt-v0",
    entry_point=mkpOpt,
)

register(
    "mkpOptReal-v0",
    entry_point=mkpOptReal,
    # kwargs=
)
