import pickle
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import copy
import librosa
import time


min_ind = 2400
max_ind = 2625
sigmoid_start = 3.
mkp_kick_data = np.genfromtxt('data_and_sequences/sps_lhcpilot_kick_data.txt')
time_data = np.genfromtxt('data_and_sequences/sps_lhcpilot_time_data.txt')

with open("plots_and_data_delta_action/experiment_{}/episode_data_{}_steps_{}_ns_dtc_{}.pickle"
          .format(79, 50000, 250, 'PPO_run_79'), "rb") as input_file:
    episode_data = pickle.load(input_file)

print(episode_data['rise_time_change'][280])
print(episode_data['real_actions'][280])


new_waveform = librosa.resample(mkp_kick_data[4][2400:3600],
                                                orig_sr=(3600 - 2400),
                                                target_sr=(3600 - 2400) * (1 + 0.075),
                                                res_type='linear')

mkp_kick_data[4][2400:3600] = new_waveform[0:1200]

new_waveform = librosa.resample(mkp_kick_data[5][2400:3600],
                                                orig_sr=(3600 - 2400),
                                                target_sr=(3600 - 2400) * (1 + 0.075),
                                                res_type='linear')

mkp_kick_data[5][2400:3600] = new_waveform[0:1200]


plt.plot(time_data[2350:2650], mkp_kick_data[4][2350-25:2650-25])
plt.plot(time_data[2350:2650], mkp_kick_data[5][2350-25:2650-25])
plt.axvline(x=5100, color='b', ls='--', label='Injected beam')
plt.axvline(x=4850, color='c', ls='--', label='Circulating beam')
plt.show()

print(time_data[min_ind])
print(time_data[max_ind])
print(len(time_data))
print(time_data[2450 - 64])
print(np.argwhere(time_data == (4900 - 250)))

# randomize shape of waveform rise time
def sigmoid(x):
    z = np.exp(-x)
    sig = 1 / (1 + z)

    return sig



#new_waveform = librosa.resample(mkp_kick_data[0][2400:3000], orig_sr=600, target_sr=900, res_type='linear')

#plt.plot(time_data[2400:3300], new_waveform, time_data[2400:3000], mkp_kick_data[0][2400:3000])

#plt.show()

normalization_parameter = np.zeros((16, 2))

ind = 1
for ele in mkp_kick_data:
    print('Kicker {}'.format(ind))
    print(np.min(ele))
    print(np.max(ele))
    normalization_parameter[ind - 1][0] = np.min(ele)
    normalization_parameter[ind - 1][1] = np.max(ele)
    ind += 1

#np.savetxt('data_and_sequences/normalization_parameter.txt', X = normalization_parameter)


#print(np.genfromtxt('data_and_sequences/normalization_parameter.txt'))

sps_lhcpilot_kick_data_centered = np.empty([16, 10000])

ind = 0
ind_fig = 1
fig1 = plt.figure(figsize=(10, 7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.set_xlabel('Time (ns)')
ax.set_ylabel('Kick (mrad)')
for ele in mkp_kick_data:
    middle = np.argmax(ele >= 0.5 * np.max(ele))

    print('Kicker {}'.format(ind-1))
    print(0.5 * np.max(ele))
    print(time_data[middle])
    shift = int((4900 - time_data[middle]) / 2)  # each index is 2 ns
    ax.plot(time_data[2300:3000], ele[(2300-shift):(3000-shift)])

    if ind % 2 == 1:
        fig1.savefig('plots_and_data_delta_action/waveforms_switch_{}.png'.format(ind_fig))
        ind_fig += 1
        fig1 = plt.figure(figsize=(10, 7))
        ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
        ax.yaxis.label.set_size(20)
        ax.xaxis.label.set_size(20)
        plt.xticks(fontsize=14)
        plt.yticks(fontsize=14)
        ax.set_xlabel('Time (ns)')
        ax.set_ylabel('Kick (mrad)')


    sps_lhcpilot_kick_data_centered[ind] = ele[(1000-shift):(11000-shift)]
    ind += 1

plt.show()

#np.savetxt('data_and_sequences/sps_lhcpilot_kick_data_centered.txt', X = sps_lhcpilot_kick_data_centered)
#np.savetxt('data_and_sequences/time_data_centered.txt', X = time_data[1000:11000])


mkp_waveforms = np.genfromtxt('data_and_sequences/sps_lhcpilot_kick_data_centered.txt')

start = time.time()
fft = np.fft.rfft((mkp_waveforms[0][1000:4000] - np.mean(mkp_waveforms[0][1000:4000])))
end = time.time()

print(end - start)

plt.plot(np.real(fft[0:20]))
plt.show()
plt.plot(np.imag(fft[0:20]))
plt.show()
'''
new_waveform = librosa.resample(mkp_waveforms[0][1000:4000],
                                        orig_sr=3000,
                                        target_sr=3300,
                                        res_type='linear')

fft = np.fft.fft(new_waveform)

plt.plot(fft)
'''

'''

new_waveform = librosa.resample(mkp_waveforms[0][1000:4000],
                                        orig_sr=3000,
                                        target_sr=3600,
                                        res_type='linear')

fft = np.fft.fft(new_waveform[0:3000])

plt.plot(fft)

fft = np.fft.fft(new_waveform[100:3100])
plt.plot(fft)
plt.show()

'''

'''

mkp_waveforms = np.load('data_and_sequences/sps_lhcpilot_kick_data_centered.npy')
time_data = np.load('data_and_sequences/time_data_centered.npy')


# Test, if the waveform randomization works better when waveforms are aligned
min_ind = 1400
max_ind = 1625

rise_time_original = np.zeros(16)
ind = 1
for ele in mkp_waveforms:
    start = np.argmax(ele >= 0.1 * np.max(ele))
    stop = np.argmax(ele >= 0.9 * np.max(ele))

    print('Kicker {}'.format(ind))
    print(np.max(ele))

    print(time_data[start])
    print(time_data[stop])

    rise_time_original[ind - 1] = time_data[stop] - time_data[start]
    ind += 1

rise_time = np.zeros((16, len(np.arange(1, 2, 0.1))))
counter = 0
for i in np.arange(1, 2, 0.1):
    ind = 0
    mkp_waveforms = np.load('data_and_sequences/sps_lhcpilot_kick_data_centered.npy')

    for ele in mkp_waveforms:
        #sigmoid_interval = np.linspace(i, 5, num=(max_ind - min_ind))
        #mkp_waveforms[ind][min_ind:max_ind] *= sigmoid(sigmoid_interval)
        new_waveform = librosa.resample(ele[min_ind:6000],
                                        orig_sr=(6000-min_ind), 
                                        target_sr=(6000-min_ind)*i,
                                        res_type='linear')

        start = np.argmax(new_waveform >= 0.1 * np.max(new_waveform))
        stop = np.argmax(new_waveform >= 0.9 * np.max(new_waveform))
        print(start)
        print(stop)
        rise_time[ind][counter] = (stop - start) * 2

        ind += 1
    counter += 1

for i in np.arange(0, 16, 1):
    fig1 = plt.figure(figsize=(10, 7))
    ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
    ax.yaxis.label.set_size(20)
    ax.xaxis.label.set_size(20)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    ax.plot(np.arange(1, 2, 0.1), np.array(rise_time[i]))
    ax.hlines(rise_time_original[i], 1, 2)
    ax.set_xlabel('Randomization parameter')
    ax.set_ylabel('Rise time (ns)')
    fig1.suptitle('Change in rise time for kicker {}'.format(i + 1), fontsize=20)
    fig1.savefig('plots_and_data_delta_action/rise_time_randomization_stretching_kicker_{}.png'.format(i + 1))


'''

'''
sigmoid_interval = np.linspace(sigmoid_start, 5, num=(max_ind - min_ind))  # space over which the randomizing sigmoid function is defined: random start value between 0 and sigmoid_start, up to 10

sigmoid_interval_2 = np.linspace(np.random.rand() * sigmoid_start, 5, num=(max_ind - min_ind))  # space over which the randomizing sigmoid function is defined: random start value between 0 and sigmoid_start, up to 10

interval = np.linspace(min_ind, max_ind, num=(max_ind - min_ind))


plt.plot(interval, sigmoid(sigmoid_interval), interval, sigmoid(sigmoid_interval_2))
plt.show()

mkp_waveforms = np.genfromtxt('data_and_sequences/sps_lhcpilot_kick_data.txt')


ind = 0
# Average case
for ele in mkp_kick_data:
    sigmoid_interval = np.linspace(sigmoid_start, 5, num=(max_ind - min_ind))
    ele[min_ind:max_ind] = ele[min_ind:max_ind] * sigmoid(sigmoid_interval)
    mkp_waveforms[ind] = ele
    ind += 1

mkp_kick_data_old = np.genfromtxt('data_and_sequences/sps_lhcpilot_kick_data.txt')

plt.plot(time_data[min_ind:max_ind], mkp_waveforms[0][min_ind:max_ind], time_data[min_ind:max_ind], mkp_kick_data_old[0][min_ind:max_ind])

plt.show()


mkp_kick_data_old = np.genfromtxt('data_and_sequences/sps_lhcpilot_kick_data.txt')
kick_sum_old = np.sum(mkp_kick_data_old, axis=0)
kick_sum = np.sum(mkp_waveforms, axis=0)

start = np.argmax(kick_sum_old >= 0.02 * np.max(kick_sum_old))
stop = np.argmax(kick_sum_old >= 0.98 * np.max(kick_sum_old))
print(time_data[stop] - time_data[start])

start = np.argmax(kick_sum >= 0.1 * np.max(kick_sum))
stop = np.argmax(kick_sum >= 0.9 * np.max(kick_sum))
print(time_data[stop] - time_data[start])

mkp_waveforms = mkp_kick_data_old

'''
'''
mkp_waveforms = np.genfromtxt('data_and_sequences/sps_lhcpilot_kick_data.txt')

t = 4900
t_ind = np.argwhere(time_data == (t - (t % 2)))

min_ind = t_ind.item() - 140
max_ind = t_ind.item() + 140

print(min_ind)

rise_time_original = np.zeros(16)
ind = 1
for ele in mkp_waveforms:
    start = np.argmax(ele >= 0.1 * np.max(ele))
    stop = np.argmax(ele >= 0.9 * np.max(ele))

    print('Kicker {}'.format(ind))
    print(np.max(ele))

    print(time_data[start])
    print(time_data[stop])

    rise_time_original[ind-1] = time_data[stop] - time_data[start]

    plt.plot(time_data[2500:5000], ele[2500:5000])
    ind += 1


rise_time = np.zeros((16, len(np.arange(-5, 5, 0.1))))
counter = 0
for i in np.arange(-5, 5, 0.1):
    ind = 0
    mkp_waveforms = np.genfromtxt('data_and_sequences/sps_lhcpilot_kick_data.txt')

    for ele in mkp_kick_data:
        sigmoid_interval = np.linspace(i, 5, num=(max_ind - min_ind))
        mkp_waveforms[ind][min_ind:max_ind] *= sigmoid(sigmoid_interval)

        start = np.argmax(mkp_waveforms[ind] >= 0.1 * np.max(mkp_waveforms[ind]))
        stop = np.argmax(mkp_waveforms[ind] >= 0.9 * np.max(mkp_waveforms[ind]))

        rise_time[ind][counter] = time_data[stop] - time_data[start]

        ind += 1

    mkp_kick_data_old = np.genfromtxt('data_and_sequences/sps_lhcpilot_kick_data.txt')
    counter += 1
    



for i in np.arange(0, 16, 1):
    fig1 = plt.figure(figsize=(10,7))
    ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
    ax.yaxis.label.set_size(20)
    ax.xaxis.label.set_size(20)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    ax.plot(np.arange(-5, 5, 0.1), np.array(rise_time[i]))
    ax.hlines(rise_time_original[i], -5, 5)
    ax.set_xlabel('Randomization parameter')
    ax.set_ylabel('Rise time (ns)')
    fig1.suptitle('Change in rise time for kicker {}'.format(i+1), fontsize=20)
    fig1.savefig('plots_and_data_delta_action/rise_time_randomization_kicker_{}.png'.format(i+1))
'''
'''

mkp_waveforms = np.genfromtxt('data_and_sequences/sps_lhcpilot_kick_data.txt')

def inv_norm_data(x_norm, limits):
    x_data = np.zeros(len(x_norm))
    delta = limits[1, :] - limits[0, :]
    x_data = (x_norm + 1) / 2 * delta + limits[0, :]

    return x_data


delta_t0 = np.random.normal(5050, 100)

t = 2 * 4900 - delta_t0  # to keep track of general time stamp
tc = t - 250

dtau0 = np.zeros(9)
#for i in range(0, 8):
#    dtau0[i] = np.random.normal(0, 10)
dtau0[-1] = delta_t0

#dtau0[6] = +100
dtau0[-1] = 4900

x0_action = dtau0

limits = np.array(
    [
        np.array(x0_action) - 100,  # individual lower
        np.array(x0_action) + 100,  # individual upper
    ]
)
limits[0, -1] = x0_action[-1] - 750  # General lower
limits[1, -1] = x0_action[-1] + 750  # General upper

print("reset action: {}".format(x0_action))

off_set = np.zeros(9)

raw_state = []
dtau = inv_norm_data(off_set, limits)
t = 2 * 4900 - dtau[-1]

t_inds = []
for i in range(8):  # 8 switches in total to iterate over
    t_ind = np.argwhere(
        time_data == (t - dtau[i] - ((t - dtau[i]) % 2)))  # find closest even index corresponding to this time
    t_inds.append(t_ind)

# Iterate over the modules, check the time shift for each switch, and add the kicks for the injected and circulating beam
kicks = []
kicks_c = []
switch_count = 0
for i in range(16):
    if (((i) % 2) == 0) and (i != 0):  # after every two steps, change electrical switch
        switch_count += 1
    min_index = t_inds[switch_count].item() - 140
    max_index = t_inds[switch_count].item() + 140
    kick = mkp_waveforms[i][min_index:max_index:4].flatten()
    raw_state.append(np.array(kick))

for ele in raw_state:
    plt.plot(time_data[2310:2590:4], ele)

plt.show()
'''


'''


mkp_waveforms = np.genfromtxt('data_and_sequences/sps_lhcpilot_kick_data.txt')

t = 4900
t_ind = np.argwhere(time_data == (t - (t % 2)))

min_ind = t_ind.item() - 140
max_ind = t_ind.item() + 140

print(min_ind)


fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.set_xlabel('Time (ns)')
ax.set_ylabel('Kick (mrad)')
fig1.suptitle('Waveforms of the kicker magnets', fontsize=20)



rise_time_original = np.zeros(16)
ind = 1
for ele in mkp_waveforms:
    start = np.argmax(ele >= 0.02 * np.max(ele))
    stop = np.argmax(ele >= 0.98 * np.max(ele))

    print('Kicker {}'.format(ind))
    print(np.max(ele))

    print(time_data[start])
    print(time_data[stop])

    rise_time_original[ind-1] = time_data[stop] - time_data[start]

    ax.plot(time_data[2300:2700], ele[2300:2700] / np.max(ele))
    ind += 1
fig1.savefig('plots_and_data_delta_action/waveforms_original_normalized.png')
'''

'''
plt.plot(time_data[2500:5000], mkp_waveforms[0][2500:5000])
plt.show()

#plt.bar(np.arange(1, 17, 1), rise_time_original)
#plt.show()


fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.bar(np.arange(1, 17, 1), rise_time_original)
ax.set_xlabel('Kicker')
ax.set_ylabel('Rise time (ns)')
fig1.suptitle('0.02 to 0.98 rise time', fontsize=20)
fig1.savefig('plots_and_data_delta_action/rise_time_original_kicker_002_098.png')
'''
