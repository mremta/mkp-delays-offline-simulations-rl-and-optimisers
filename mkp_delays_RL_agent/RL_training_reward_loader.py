"""
Plotter to compare performance of trained RL agent for different runs 
"""
import numpy as np
import matplotlib.pyplot as plt
import pickle
import pandas as pd


# Initiate and load model 
save_plot = True
algorithm = 'A2C'
n_steps = 50000
dtc = 250

# Prepare data strings for the different training runs  
# load_str = "plots_and_data/episode_data_{}_steps_{}_ns_dtc_{}.pickle".format(n_steps, dtc, algorithm)
save_str = "plots_and_data/agent_training_rewards_{}_ns_dtc_{}.png".format(dtc, algorithm)
data_str = [
    "episode_data_50000_steps_250_ns_dtc_A2C.pickle",
    "episode_data_50000_steps_250_ns_dtc_A2C_run_2.pickle",
    "episode_data_50000_steps_250_ns_dtc_A2C_run_3.pickle",
    "episode_data_65000_steps_250_ns_dtc_A2C_run_4.pickle",
    "episode_data_50000_steps_250_ns_dtc_A2C_run_5_ind_lim_200.pickle"
   ]

episode_rewards = []
for ele in data_str:
    load_str = "plots_and_data/{}".format(ele)
    
    #Unpickle the data
    with open(load_str, "rb") as handle:
        episode_data = pickle.load(handle)
    
    episode_rewards.append(episode_data["reward_sum"])
    

# Plot reward over time for different training phases 
fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14) 
plt.yticks(fontsize=14)
for count, item in enumerate(episode_rewards):
    ax.plot(item, label="Training session {}".format(count+1))
ax.set_xlabel("Training episode")
ax.set_ylabel("Reward")
ax.legend(prop={'size': 16}) 
fig1.tight_layout()

if save_plot:
    fig1.savefig(save_str, dpi=250)
    
# Also plot the mean and the standard deviations 
df = pd.DataFrame(episode_rewards)
reward_mean = df.mean()  
reward_std = df.std()

fig2 = plt.figure(figsize=(10,7))
ax2 = fig2.add_subplot(1, 1, 1)  # create an axes object in the figure
ax2.yaxis.label.set_size(20)
ax2.xaxis.label.set_size(20)
plt.xticks(fontsize=16) 
plt.yticks(fontsize=16)
ax2.set_xlabel("Training episode")
ax2.set_ylabel("Mean reward")
fig2.tight_layout()
ax2.plot(reward_mean)
ax2.fill_between(np.arange(len(reward_mean)), reward_mean+reward_std, reward_mean-reward_std, alpha = 0.4, color = "c")    
ax2.set_ylim(-12, 0.4)

if save_plot:
    fig2.savefig("plots_and_data/mean_agent_training_rewards_{}_ns_dtc_{}.png".format(dtc, algorithm), dpi=250)