"""
Helper class meant to replicate the real MKP delays, but using historical waveform data. Version for RL agent, where
we include the possibility to randomize the shape of the waveforms for the agent to learn
"""

import numpy as np
import matplotlib.pyplot as plt
# import pickle
# from pathlib import Path
# import logging
# import time

# import sys
from cpymad.madx import Madx
import pandas as pd
import librosa

# sys.path.append('/home/ewaagaa/cernbox2/Tech_student/Code')  #for office computer
# import acc_phy_lib_elias

class FakeMKPdelays:
    def __init__(self, optics='q20', randomize_waveforms=True, dtc=500, max_rise_time_change=0.2):
        self.optics = optics
        self.mkp_kick_data = np.genfromtxt('data_and_sequences/sps_lhcpilot_kick_data_centered.txt')  # .astype('float32')
        self.time_data = np.genfromtxt('data_and_sequences/time_data_centered.txt')
        self.ref_timestamp = 4900  # timestamp of the beam at injection
        self.dtc = dtc
        self.randomize_waveform_amplitude = False  # randomize waveform amplitude
        self.max_rise_time_change = max_rise_time_change  # maximum relative increase of rise time
        self.avg_rise_time_change = 1   # for logging purposes

        # -------------------- RANDOMIZATION OF WAVEFORM SHAPE OR AMPLITUDE -------------------------------
        # define waveform indices on which to start and stop
        self.min_ind = 1400  # t = 4800
        self.max_ind = 5000

        # randomize shape of waveform rise time
        if randomize_waveforms:
            ind = 0
            sum = 0
            self.mkp_waveforms = self.mkp_kick_data  # first initialize the waveforms as they are today
            print("\nRandomizing waveform rise times...\n")
            for ele in self.mkp_kick_data:
                if (ind % 2) == 0:  # randomize the change in rise time for each switch
                    factor = np.random.rand() * self.max_rise_time_change
                    sum += factor * 2
                new_waveform = librosa.resample(ele[self.min_ind:self.max_ind],
                                        orig_sr=(self.max_ind - self.min_ind),
                                        target_sr=(self.max_ind - self.min_ind) * (1 + factor),
                                        res_type='linear')

                self.mkp_waveforms[ind][self.min_ind:self.max_ind] = new_waveform[0:(self.max_ind - self.min_ind)]
                ind += 1
            self.avg_rise_time_change = 1 + sum / 16
            #print("\nAverage change of rise time by factor {}.\n".format(sum / 16 + 1))
        else:
            self.mkp_waveforms = self.mkp_kick_data  # otherwise just copy the waveform data
            print("\nAverage change of rise time by factor {}.\n".format(1))

        # extract previous waveform data with randomized amplitude, or as it is
        if self.randomize_waveform_amplitude:
            ind = 0
            self.mkp_waveforms = self.mkp_kick_data  # first initialize the waveforms as they are today
            print("\nRandomizing waveform amplitudes...\n")
            for ele in self.mkp_kick_data:
                self.mkp_waveforms[ind] = ele * (
                            1 + 0.2 * np.random.rand())  # randomize waveform amplitude of present values +/- X percent
                ind += 1
        # --------------------------------------------------------------------------

        self.start_reset()

    # Method to get fake BPM positions by providing a time shift vector dtau to step method, then get coordinates from MADX
    def get_positions(self, dtau):
        self.valid_cycle = False
        bpm_pos_x, bpm_pos_x_c = self.step(
            dtau)  # perform one step, providing MKP time shifts and get the BPM positions

        return bpm_pos_x, bpm_pos_x_c

        # Method to calculate oscillations of the injected and circulating beam (c) for a given timeshift vector for the MKP switches

    def step(self, dtau):

        # Check at what time to evaluate the MKP kick for the injected beam, and for the circulating beam
        t = self.ref_timestamp   # timestamp for evaluation for injected beam
        tc = t - self.dtc  #  timestamp for evaluation for injected beam
        self.dt_general = dtau[-1]  # general time shift
        t_inds = []
        tc_inds = []
        for i in range(8):  # 8 switches in total to iterate over
            t_ind = np.argwhere(self.time_data == (2 * t - dtau[i] - (
                        (2 * t - dtau[i]) % 2)))  # find closest even index corresponding to this time
            tc_ind = np.argwhere(self.time_data == (tc + t - dtau[i] - ((tc + t - dtau[i]) % 2)))
            t_inds.append(t_ind)
            tc_inds.append(tc_ind)

        # Iterate over the modules, check the time shift for each switch, and add the kicks for the injected and circulating beam
        self.new_kicks = []
        self.new_kicks_c = []
        switch_count = 0
        for i in range(16):
            if (((i) % 2) == 0) and (i != 0):  # after every two steps, change electrical switch
                switch_count += 1
            kick = self.mkp_waveforms[i][t_inds[switch_count]]
            kick_c = self.mkp_waveforms[i][tc_inds[switch_count]]
            self.new_kicks.append(kick)
            self.new_kicks_c.append(kick_c)

        # Define a vector with new MKP kicks, for the injected and circulating beam
        self.mkp_kicks = np.empty(4)
        self.mkp_kicks[0] = np.sum(self.new_kicks[:5])
        self.mkp_kicks[1] = np.sum(self.new_kicks[5:10])
        self.mkp_kicks[2] = np.sum(self.new_kicks[10:12])
        self.mkp_kicks[3] = np.sum(self.new_kicks[12:16])
        self.mkp_kicks_c = np.empty(4)
        self.mkp_kicks_c[0] = np.sum(self.new_kicks_c[:5])
        self.mkp_kicks_c[1] = np.sum(self.new_kicks_c[5:10])
        self.mkp_kicks_c[2] = np.sum(self.new_kicks_c[10:12])
        self.mkp_kicks_c[3] = np.sum(self.new_kicks_c[12:16])

        # Update the MKP kicks through the MADX globals
        self.madx.globals['kmkpa11931'] = self.mkp_kicks[0]
        self.madx.globals['kmkpa11936'] = self.mkp_kicks[1]
        self.madx.globals['kmkpc11952'] = self.mkp_kicks[2]
        self.madx.globals['kmkp11955'] = self.mkp_kicks[3]
        self.madx2.globals['kmkpa11931'] = self.mkp_kicks_c[0]
        self.madx2.globals['kmkpa11936'] = self.mkp_kicks_c[1]
        self.madx2.globals['kmkpc11952'] = self.mkp_kicks_c[2]
        self.madx2.globals['kmkp11955'] = self.mkp_kicks_c[3]

        # POSSIBILITY TO PRINT MKP KICKS
        # print("MKP kicks injected beam: {}".format(self.mkp_kicks))
        # print("MKP kicks circulating beam: {}".format(self.mkp_kicks_c))
        # print("MKP kicks circulating beam: {}".format(self.mkp_kicks_c))

        # Get oscillations from injected beam
        try:
            # Attempt Twiss commands for the injected and circulating beam , restart MADX if it crashes
            # Injected beam
            self.madx.use(sequence='short_injection_and_some_sps')
            self.twiss = self.madx.twiss(betx=self.twiss_beta1['betx'], alfx=-self.twiss_beta1['alfx'],
                                         bety=self.twiss_beta1['bety'], alfy=-self.twiss_beta1['alfy'],
                                         dx=self.twiss_beta1['dx'], dpx=-self.twiss_beta1['dpx'],
                                         dy=self.twiss_beta1['dy'], dpy=-self.twiss_beta1['dpy'],
                                         x=self.twiss_beta1['x'], y=self.twiss_beta1['y'],
                                         px=-self.twiss_beta1['px'], py=-self.twiss_beta1['py']).dframe()

            # Find the average amplitude of the oscillations for the injected beam
            self.x_bar = np.max(abs(self.twiss.loc['qf.12010':'qf.13010'].x))
            self.px_bar = np.max(abs(self.twiss.loc['qf.12010':'qf.13010'].px))


        # Restart MADX if it crashes
        except RuntimeError:
            print("Resetting MADX for injected beam...")
            with open('tempfile', 'r') as f:
                lines = f.readlines()
                for ele in lines:
                    if '+=+=+= fatal' in ele:
                        print('{}'.format(ele))
            self.reset()

        # """
        # Get oscillations from circulating beam
        try:
            # Attempt Twiss commands for the injected and circulating beam , restart MADX if it crashes
            self.madx2.use(sequence="reduced_circulating_sps")
            self.twiss_c = self.madx2.twiss(betx=self.initbeta0[0], alfx=self.initbeta0[1],
                                            bety=self.initbeta0[2], alfy=self.initbeta0[3],
                                            dx=self.initbeta0[4], dpx=self.initbeta0[5],
                                            dy=self.initbeta0[6], dpy=self.initbeta0[7],
                                            x=self.initbeta0[8], y=self.initbeta0[9],
                                            px=self.initbeta0[10], py=self.initbeta0[11]).dframe()

            # Find the average amplitude of the oscillations for the circulating beam
            self.x_bar_c = np.max(abs(self.twiss_c.loc['qf.12010':'qf.13010'].x))
            self.px_bar_c = np.max(abs(self.twiss_c.loc['qf.12010':'qf.13010'].px))


        # Restart MADX if it crashes
        except RuntimeError:
            print("Resetting MADX for circulating beam...")
            with open('tempfile2', 'r') as f:
                lines = f.readlines()
                for ele in lines:
                    if '+=+=+= fatal' in ele:
                        print('{}'.format(ele))
            self.reset()

        # return horizontal beam position of circulating and injected beam
        return self.x_bar, self.x_bar_c

        # """
        # Reset the MKP kicks to their original values, by acting on the global variables
        self.madx.globals['kmkpa11931'] = self.mkp_kicks_org1
        self.madx.globals['kmkpa11936'] = self.mkp_kicks_org2
        self.madx.globals['kmkpc11952'] = self.mkp_kicks_org3
        self.madx.globals['kmkp11955'] = self.mkp_kicks_org4
        self.madx2.globals['kmkpa11931'] = self.mkp_kicks_org1_c
        self.madx2.globals['kmkpa11936'] = self.mkp_kicks_org2_c
        self.madx2.globals['kmkpc11952'] = self.mkp_kicks_org3_c
        self.madx2.globals['kmkp11955'] = self.mkp_kicks_org4_c
        # """

    # Method to start a new madx process
    def start_reset(self):
        with open('tempfile', 'w') as f:
            self.madx = Madx(stdout=f, stderr=f)
        self.madx.option(echo=False, warn=True, info=False, debug=False, verbose=False)

        # --------------- CIRCULATING BEAM -----------------
        with open('tempfile2', 'w') as f:
            self.madx2 = Madx(stdout=f, stderr=f)
        self.madx2.option(echo=False, warn=True, info=False, debug=False, verbose=False)

        # Load sequence from class method
        self.load_seq()

        # Method to reset madx if it crashes, and kills it

    def reset(self):
        print("Reset!")
        # ------------- INJECTED BEAM ---------------------
        # If MAD-X fails, re-spawn process
        # if 'self.madx' in globals():
        self.madx.quit()
        del self.madx
        print("Deleted!")
        with open('tempfile', 'w') as f:
            self.madx = Madx(stdout=f, stderr=f)
        self.madx.option(echo=False, warn=True, info=False, debug=False, verbose=False)
        # --------------- CIRCULATING BEAM -----------------
        # If MAD-X fails, re-spawn process
        # if 'self.madx2' in globals():
        self.madx2.quit()
        del self.madx2
        with open('tempfile2', 'w') as f:
            self.madx2 = Madx(stdout=f, stderr=f)
        self.madx2.option(echo=False, warn=True, info=False, debug=False, verbose=False)

        # Load sequence from class method
        self.load_seq()

    # Method to load injection sequence and parts of SPS
    def load_seq(self):
        # -------------------------- THEN LOAD INJECTED BEAM, WITH SOME SPS ------------------------------
        # Activate the aperture for the Twiss flag to include it in Twiss command!
        self.madx.input('select,flag=twiss,clear;')
        self.madx.select(flag='twiss',
                         column=['N1', 'apertype', 'aper_1', 'aper_2', 'aper_3', 'aper_4', 'apoff_1', 'apoff_2',
                                 'aptol_1', 'aptol_2', 'aptol_3'])
        self.madx.options['update_from_parent'] = True

        # Load sequence and initial conditions for short_injection_and_some_sps, to get the injected beam and about 412 m into the SPS
        self.madx.call('data_and_sequences/short_injection_and_some_sps_{}.seq'.format(self.optics))
        # Load Twiss and survey parameters
        self.twiss_reversed = pd.read_csv("data_and_sequences/sps_injection_twiss_reversed_{}.csv".format(self.optics))
        self.twiss_beta1 = self.twiss_reversed.iloc[
            -1]  # locate Twiss parameters at the start of the loaded reversed sequence, initial conditions for later

        # Call the old aperture files for SPS, for the injection sequence
        self.madx.use(sequence="short_injection_and_some_sps")
        self.madx.call("data_and_sequences/aperturedb_1.dbx")
        self.madx.call("data_and_sequences/aperturedb_2.dbx")
        self.madx.call("data_and_sequences/aperturedb_3.dbx")

        # Extract beam info - same for injected and circulating
        self.madx.use(sequence='short_injection_and_some_sps')
        self.sige = self.madx.sequence['short_injection_and_some_sps'].beam['sige']  # relative energy spread
        self.ex = self.madx.sequence['short_injection_and_some_sps'].beam['ex']
        self.ey = self.madx.sequence['short_injection_and_some_sps'].beam['ey']

        # -------------------------- LOAD CIRCULATING BEAM IN SEPARATE MADX PROCESS ---------------------------------------
        # Activate the aperture for the Twiss flag to include it in Twiss command!
        self.madx2.input('select,flag=twiss,clear;')
        self.madx2.select(flag='twiss',
                          column=['N1', 'apertype', 'aper_1', 'aper_2', 'aper_3', 'aper_4', 'apoff_1', 'apoff_2',
                                  'aptol_1', 'aptol_2', 'aptol_3'])
        self.madx2.options['update_from_parent'] = True

        # Load sequence and initial conditions for tt2tt10SPS
        self.madx2.call('data_and_sequences/reduced_circulating_sps_{}.seq'.format(self.optics))
        self.initbeta0 = np.genfromtxt(
            'data_and_sequences/reduced_circulating_sps_twiss_init_{}.csv'.format(self.optics))
        # self.initbeta0 = pd.read_csv('data_and_sequences/reduced_circulating_sps_twiss_init_{}.csv'.format(self.optics), index_col=0, header=False)

        # ----------------- FIX THE APERTURES for the plot -------------------------------
        # Call the old aperture files for SPS
        self.madx2.use(sequence="reduced_circulating_sps")
        self.madx2.call("data_and_sequences/aperturedb_1.dbx")
        self.madx2.call("data_and_sequences/aperturedb_2.dbx")
        self.madx2.call("data_and_sequences/aperturedb_3.dbx")

        # Save original MKP kicks for later
        self.mkp_kicks_org1 = self.madx.globals['kmkpa11931']
        self.mkp_kicks_org2 = self.madx.globals['kmkpa11936']
        self.mkp_kicks_org3 = self.madx.globals['kmkpc11952']
        self.mkp_kicks_org4 = self.madx.globals['kmkp11955']

        self.mkp_kicks_org1_c = self.madx2.globals['kmkpa11931']
        self.mkp_kicks_org2_c = self.madx2.globals['kmkpa11936']
        self.mkp_kicks_org3_c = self.madx2.globals['kmkpc11952']
        self.mkp_kicks_org4_c = self.madx2.globals['kmkp11955']

        # Method to generate Twiss parameters specifically for the environment

    def generate_twiss(self):

        # Update the MKP kicks through the MADX globals
        self.madx.globals['kmkpa11931'] = self.mkp_kicks[0]
        self.madx.globals['kmkpa11936'] = self.mkp_kicks[1]
        self.madx.globals['kmkpc11952'] = self.mkp_kicks[2]
        self.madx.globals['kmkp11955'] = self.mkp_kicks[3]
        self.madx2.globals['kmkpa11931'] = self.mkp_kicks_c[0]
        self.madx2.globals['kmkpa11936'] = self.mkp_kicks_c[1]
        self.madx2.globals['kmkpc11952'] = self.mkp_kicks_c[2]
        self.madx2.globals['kmkp11955'] = self.mkp_kicks_c[3]

        # Injected beam
        self.madx.use(sequence='short_injection_and_some_sps')
        self.twiss = self.madx.twiss(betx=self.twiss_beta1['betx'], alfx=-self.twiss_beta1['alfx'],
                                     bety=self.twiss_beta1['bety'], alfy=-self.twiss_beta1['alfy'],
                                     dx=self.twiss_beta1['dx'], dpx=-self.twiss_beta1['dpx'],
                                     dy=self.twiss_beta1['dy'], dpy=-self.twiss_beta1['dpy'],
                                     x=self.twiss_beta1['x'], y=self.twiss_beta1['y'],
                                     px=-self.twiss_beta1['px'], py=-self.twiss_beta1['py']).dframe()

        # Circulating beam
        self.madx2.use(sequence="reduced_circulating_sps")
        self.twiss_c = self.madx2.twiss(betx=self.initbeta0[0], alfx=self.initbeta0[1],
                                        bety=self.initbeta0[2], alfy=self.initbeta0[3],
                                        dx=self.initbeta0[4], dpx=self.initbeta0[5],
                                        dy=self.initbeta0[6], dpy=self.initbeta0[7],
                                        x=self.initbeta0[8], y=self.initbeta0[9],
                                        px=self.initbeta0[10], py=self.initbeta0[11]).dframe()

        # Find maximum oscillation amplitude
        self.x_bar = np.max(abs(self.twiss.loc['qf.12010':'qf.13010'].x))
        self.px_bar = np.max(abs(self.twiss.loc['qf.12010':'qf.13010'].px))
        self.x_bar_c = np.max(abs(self.twiss_c.loc['qf.12010':'qf.13010'].x))
        self.px_bar_c = np.max(abs(self.twiss_c.loc['qf.12010':'qf.13010'].px))

        # Find Courant-Snyder invariant, for injected and circulating beam
        alpha = self.twiss.loc['qf.12010'].alfx
        beta = self.twiss.loc['qf.12010'].betx
        gamma = (1 + alpha ** 2) / beta
        x = self.twiss.loc['qf.12010'].x
        px = self.twiss.loc['qf.12010'].px
        self.J = 0.5 * (gamma * x ** 2 + 2 * alpha * x * px + beta * px ** 2)

        alpha_c = self.twiss_c.loc['qf.12010'].alfx
        beta_c = self.twiss_c.loc['qf.12010'].betx
        gamma_c = (1 + alpha_c ** 2) / beta_c
        x_c = self.twiss_c.loc['qf.12010'].x
        px_c = self.twiss_c.loc['qf.12010'].px
        self.J_c = 0.5 * (gamma_c * x_c ** 2 + 2 * alpha_c * x_c * px_c + beta_c * px_c ** 2)

    # Method to plot the oscillations
    def plot_oscillations(self, fig):
        if fig is None:
            fig = plt.figure(figsize=(10, 7))
        fig.suptitle('SPS oscillations - injected and circulating beam', fontsize=20)
        ax = fig.add_subplot(1, 1, 1)  # create an axes object in the figure
        ax.yaxis.label.set_size(24)
        ax.xaxis.label.set_size(24)
        plt.xticks(fontsize=18)
        plt.yticks(fontsize=18)
        # Define some parameters for the circulating beam, and plot it on the same axis as the injected
        if self.optics == 'sftpro':
            nx = 6
        else:
            nx = 5
        # Plot the injected beam:
        acc_phy_lib_elias.plot_envelope(self.twiss, self.sige, self.ex, self.ey, ax, nx=nx)
        # Align the injected and circulating beam longitudinally, then plot it on the same axis
        ds = self.twiss_c.loc['qf.12010'].s - self.twiss.loc['qf.12010'].s
        self.twiss_c['s'] = self.twiss_c['s'].add(-ds)
        acc_phy_lib_elias.plot_envelope(self.twiss_c, self.sige, self.ex, self.ey, ax, nx=nx, hcolor='r')
        # GET THE APERTURE AND PLOT IT
        new_pos_x, aper_neat_x = acc_phy_lib_elias.get_apertures_real(self.twiss_c.iloc[1:-2, :])
        acc_phy_lib_elias.plot_aper_real(new_pos_x, aper_neat_x, ax)
        # ----------------------------------------------------------------

        ax.legend(['Injected beam', 'Circulating beam'], loc='upper right', facecolor='white', framealpha=0.8,
                  prop={'size': 18})
        plt.ylim(-0.1, 0.1)
        plt.xlim(40, 450)
        t1 = plt.text(300, -0.095,
                      '$d\\tau_{{general}} = {:.1f}$ ns\n$dt_{{c}} = {}$ ns'.format(self.dt_general, self.dtc),
                      fontsize=18, horizontalalignment='left')
        # t2 = plt.text(55, -0.08, '$\\bar{{x}} =$ {:.1e} m,  $\\bar{{p}}_{{x}} =$ {:.1e} GeV/c'.format(self.x_bar, self.px_bar), fontsize=12, horizontalalignment='left')
        # t3= plt.text(55, -0.095, '$\\bar{{x}}_{{c}} =$ {:.1e} m,  $\\bar{{p}}_{{x, c}} =$ {:.1e} GeV/c'.format(self.x_bar_c, self.px_bar_c), fontsize=12, horizontalalignment='left')
        t1.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
        # t2.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
        # t3.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
        # print("MKP kicks injected beam: {}".format(self.mkp_kicks))
        # print("MKP kicks circulating beam: {}".format(self.mkp_kicks_c))