"""
Optimisation environment to find the ideal time shift vector dtau for the electrical switches
of the MKPs in the SPS injection in order to minimise the horizontal beam oscillations in the SPS

This versions uses the fake_mkp_delays_RL module to simulate the waveforms and the oscillation amplitude arising from it
--> _take_action() method is based on relative action, not on absolute action

Self-contained environment, with all sequence and IC files loaded from the same repository
"""

import numpy as np
# import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.axes import Axes
# import sys
# from cpymad.madx import Madx
from gym import spaces
import typing as t
import datetime
from math import exp
import time

# to access the parameter control
import logging  # to release log messages
from pyjapc import PyJapc
from cernml.coi import OptEnv, register, Machine
from cernml.coi import cancellation
from fake_mkp_delays_RL_v5 import FakeMKPdelays  # import version for the RL agent.

MODULE_NAME = "mkpdelays"


class MKPOptEnv(OptEnv):

    # Load optics, voltage-to-kick converted data, and corresponding time data
    def __init__(
            self,
            japc=None,
            use_surrogate=True,
            bunch_index_width=1,  # default value 1, if no bunch train
            acqStamp=3415,  # for LHCINDIV
            cancellation_token=None,
            optics="q20",
            dtc=225,
            objective=-5e-4,  # only for backwards compatibility
            reward_dangling=False,
            max_steps=50,  # maximal episode length
            max_rise_time_change=0.2,
            run_nr=100   # for saving plots and data in the correct folder
    ):
        self.run_nr = run_nr
        self.is_surrogate: bool = use_surrogate  # use simulated waveform data (True) or real data (False)
        self.optics = optics
        self.dtc = dtc  # time shift in ns between injected batch and circulating batch in the SPS
        self.bunch_index_width = (
            bunch_index_width  # how many bunches per train
        )
        self.acqStamp = acqStamp
        # Set cancellation token, to reset after optimisation is done:
        if cancellation_token is None:
            cancellation_token = cancellation.Token()
        self.token = cancellation_token
        self.objective = objective  # only for backwards compatibility
        self.final_objective = -5e-4  # only for backwards compatibility
        self.max_steps = max_steps  # maximum episode length
        self.reward_dangling = reward_dangling
        self.max_rise_time_change = max_rise_time_change

        self.time_data = np.genfromtxt('data_and_sequences/time_data_centered.txt')
        self.normalization_parameter = np.genfromtxt('data_and_sequences/normalization_parameter.txt')
        # self.history_objective = np.zeros(10)

        # Initiate JAPC if not given
        if not self.is_surrogate:
            # Check data type and select data source
            if japc is None:
                japc = PyJapc(
                    "SPS.USER.LHCINDIV", noSet=True
                )
            self.japc = japc
            logging.info("REAL MKP delays")

        # -------------------- PART TO TEST WHEN REAL_MKP_DELAY CLASS IS TESTED ------------------------------
        # Define the process variables for all MKP switches, and the general time shift
        self.mkpAcquisition = "MKP.BA1.F3.PFN."
        self.all_vars = []
        for i in range(8):
            self.all_vars.append(
                "{}{}/ExpertSettingDevice#arrG2MainSwitchFineTimingDelay".format(
                    self.mkpAcquisition, i + 1
                )
            )
        self.all_vars.append("MKP.BA1.F3.CONTROLLER/Setting#arrKickDelay")
        # -----------------------------------------------------------------------------------------------------

        self.dof = 8
        self.off_set = np.zeros(self.dof)
        self.action_scaling = 0.1  # scale the action, defining the maximum change in each step

        # Initialize limits arbitrarily
        self.limits = np.array(
                [
                    np.array(self.off_set) - 50,  # individual lower
                    np.array(self.off_set) + 50,  # individual upper
                ]
            )

        # Attributes for reinforcement learning agent
        self.max_episode_length = 50
        self.curr_episode = 0
        self.current_step = 0
        self.total_step_counter = 0  # to count number of total steps agent is acting upon
        self.success_count = 0  # how many times in a row an episode is successful

        # create dictionary for the episode data
        self.episode_data = {
            "length": [],
            "actions": [],
            "boundary_violation": [],
            "real_actions": [],
            "all_rewards": [],
            "initial_reward": [],
            "final_reward": [],
            "reward_sum": [],
            "timestamps": [],
            "states": [],
            "mkp_waveform_kick_data": [],
            "rise_time_change": []
        }

        # initiate action space
        high = 1 * np.ones(self.dof)
        low = -1 * np.ones(self.dof)
        self.action_space = spaces.Box(low=low, high=high, dtype=np.float32)
        self.optimization_space = self.action_space

        # state space => also called observation space
        self.observation_space = spaces.Box(-1., 1., shape=(275,), dtype=np.float64)
        #self.observation_space = spaces.Box(-1., 1., shape=(59,), dtype=np.float64)

        self.figure = None  # enable space for figure if needed

        # initiate simulation script
        self.reset_MKP_delays()

    # Initiate MKP delays object
    def reset_MKP_delays(self):
        if self.is_surrogate:
            self.mkp_delays = FakeMKPdelays(
                dtc=self.dtc,
                randomize_waveforms=True,
                max_rise_time_change=self.max_rise_time_change
            )
            # print("Resetting to fake MKP delays (simulated data)")
        else:
            print("Real MKP delays")
            self.mkp_delays = RealMKPdelays(
                self.japc,
                self.limits,
                self.mkp_index,
                self.bunch_index_width,
                self.acqStamp,
                self.token,
            )

    # If pseudorandom numbers are needed
    def seed(self, seed):
        np.random.seed(seed)

    # Step method for optimiser
    def step(self, delta_action):
        self.current_step += 1
        is_finalized = False
        state, reward, loss = self._take_action(delta_action)  # take actions

        # Log the data
        self.log_data["actions"].append(self.off_set)
        self.log_data["real_actions"].append(self.real_action)
        # self.log_data["mkp_waveform_kick_data"].append(self.mkp_delays.mkp_kick_data)  # not needed after each step
        self.log_data["timestamp"].append(str(datetime.datetime.now()).replace(" ", "_").replace(":", "-"))
        self.log_data["out"].append(reward)
        self.log_data["state"].append(state)

        if self.current_step >= self.max_steps:
            self.curr_episode += 1
            is_finalized = True

            # plot waveforms after optimization every 500 episodes
            if self.curr_episode % 500 == 0:
                fig = self.render("matplotlib_figures")
                fig[0].savefig(
                    f'plots_and_data_delta_action/experiment_{self.run_nr}/waveforms_episode_{self.curr_episode}_end.png')

            # also append the various data from the episode
            self.episode_data["length"].append(self.current_step)
            self.episode_data["actions"].append(self.log_data["actions"])
            self.episode_data["boundary_violation"].append(self.log_data["boundary_violation"])
            self.episode_data["real_actions"].append(self.log_data["real_actions"])
            self.episode_data["all_rewards"].append(self.log_data["out"])
            self.episode_data["final_reward"].append(self.log_data["out"][-1])  # final reward obtained
            self.episode_data["reward_sum"].append(np.sum(self.log_data["out"]))  # summed reward per episode
            self.episode_data["rise_time_change"].append(self.mkp_delays.rise_time_change)
            #self.episode_data["mkp_waveform_kick_data"].append(
            #    self.mkp_delays.mkp_kick_data)  # we randomize per episode, so log this waveform data

            #print("\n Final real action: {}\n".format(self.log_data["real_actions"][-1]))

            print("\n\n------ EPISODE {} with reward sum: {:.3e} -------- \n\n".format(
                self.curr_episode, np.sum(self.log_data["out"])
            ))

        return state, reward, is_finalized, {}

    # Method to optimize single objective function
    def compute_single_objective(self, action):
        _, reward, _, _ = self.step(action)

        return -reward

    # Method to get initial parameters
    def get_initial_params(self) -> t.Any:
        return self.norm_data(self.x0_action)

    # Reset the state of the environment and returns an initial observation.
    # Returns observation (object): the initial observation of the space.
    def reset(self):
        print("\n\nResetting environment...\n\n")
        #start = time.time()
        self.mkp_delays.augment_waveforms()  # reset the MKP delays if episode is done, to reset randomization of waveforms
        #self.reset_MKP_delays()
        #end = time.time()
        #print(end-start)
        self.current_step = 0

        # Keep logged history in memory to pickle, for each episode
        self.log_data = {
            "actions": [],
            "real_actions": [],
            "boundary_violation": 0,
            "bpm_pos": [],
            "out": [],
            "mkp_waveform_kick_data": [],
            "mkp_waveform_time_data": [],
            "state": [],
            "timestamp": [],
            "opt_variables": []
        }
        # self.log_data["opt_variables"].append(self.all_vars)  # store what variables were optimised for

        # Also initialize the absolute action
        self.off_set = np.zeros(self.dof)

        # Randomization of the initial delays (x0 action)
        if self.is_surrogate:
            # randomize general delay
            delta_t0 = np.random.normal(4700, 0.1)

            # randomize individual delays
            dtau0 = np.zeros(self.dof)
            for i in range(0, 8):
                dtau0[i] = np.random.normal(0, 0.1)

            # limits for action space are +- 200 of the starting delays
            self.x0_action = dtau0 + delta_t0 # work with delays relative to zero

            self.limits = np.array(
                [
                    np.array(self.x0_action) - 50,  # individual lower
                    np.array(self.x0_action) + 50,  # individual upper
                ]
            )

        #print("reset action: {}".format(self.x0_action))

        state, reward, _ = self._take_action(np.zeros(self.dof))
        self.episode_data["initial_reward"].append(reward)

        # plot waveforms before optimization, every 50 episodes
        if (self.curr_episode + 1) % 500 == 0:
            fig = self.render("matplotlib_figures")
            fig[0].savefig(
                f'plots_and_data_delta_action/experiment_{self.run_nr}/waveforms_episode_{(self.curr_episode + 1)}_start.png')

        return state

    # Method to ensure action stays within normalised limits [-1, 1]
    def _check_action(self, action: np.ndarray):

        # print("Action is: {}".format(action))
        checked = np.all(action >= -1) and np.all(action <= 1)
        if not checked:
            logging.warning(
                "Action out of limits being clipped {}".format(
                    action
                )
            )
        clipped_action = np.clip(action, -1, 1)
        return checked, clipped_action

    # Method to take relative (delta) action with respect to absolute action
    def _take_action(self, delta_action):

        self.delta_action = delta_action

        # print("Delta_action {}\n Off_set: {}".format(self.delta_action, self.off_set))

        # add relative action to absolute action --> working in "deltas"!
        proposed_action = self.off_set + delta_action * self.action_scaling
        self.is_action_ok, action = self._check_action(proposed_action)
        self.off_set = action

        # Assign action to state and un-normalise the action
        self.real_action = self.inv_norm_data(action)

        # Add total number of steps to total step counter
        self.total_step_counter += 1

        # Get reward from model
        reward, self.full_actions, loss = self._get_reward(self.real_action, proposed_action)

        # Add penalty to reward if action is not okay
        if not self.is_action_ok:
            self.log_data["boundary_violation"] += 1

        # Observe state from MKP waveform rise times
        state = self._observe(loss)

        # print("State shape of waveform is: {}, dim {}".format(type(state), len(state)))
        #print("Current step: {}, Reward: {:.3e}, Loss: {:.3e}".format(self.current_step, reward, loss))

        return state, reward, loss

    # Method for observing current state
    def _observe(self, loss):
        raw_state = []
        dtau = self.inv_norm_data(self.off_set)
        # Check at what index to evaluate the MKP kick data
        t = self.mkp_delays.ref_timestamp  # reference time for kick data evaluation
        for i in range(8):  # 8 switches in total to iterate over
            ts = 2 * t - (self.dtc / 2) - dtau[i]
            t_ind = np.argwhere(self.time_data == (ts - (ts % 2)))  # find closest even index corresponding to this time
            min_index = t_ind.item() - 64
            max_index = t_ind.item() + 65  # ensure that the same amount of elements is taken at either side
            kick = self.mkp_delays.mkp_waveforms[(i * 2)][min_index:max_index:4].flatten()  # only append every second waveform (one per switch)
            kick = np.maximum(kick, np.zeros(len(kick)))  # ensure that waveforms are non-negative
            kick = 2 * kick / (self.normalization_parameter[i * 2][1] * 1.1) - 1  # normalize kick with some tolerance
            raw_state.append(np.array(kick))
            '''
            ts = 2 * t - dtau[i]
            t_ind = np.argwhere(self.time_data == (ts - (ts % 2)))  # find closest even index corresponding to this time
            min_index = t_ind.item() - 4
            max_index = t_ind.item() + 5  # ensure that the same amount of elements is taken at either side
            kick = self.mkp_delays.mkp_waveforms[(i * 2)][min_index:max_index:4].flatten()  # only append every second waveform (one per switch)
            kick = np.maximum(kick, np.zeros(len(kick)))  # ensure that waveforms are non-negative
            kick = 2 * kick / (self.normalization_parameter[i * 2][1] * 1.1) - 1  # normalize kick with some tolerance
            raw_state.append(np.array(kick))

            ts = 2 * t - self.dtc - dtau[i]
            t_ind = np.argwhere(self.time_data == (ts - (ts % 2)))  # find closest even index corresponding to this time
            min_index = t_ind.item() - 4
            max_index = t_ind.item() + 5  # ensure that the same amount of elements is taken at either side
            kick = self.mkp_delays.mkp_waveforms[(i * 2)][min_index:max_index:4].flatten()  # only append every second waveform (one per switch)
            kick = np.maximum(kick, np.zeros(len(kick)))  # ensure that waveforms are non-negative
            kick = 2 * kick / (self.normalization_parameter[i * 2][1] * 1.1) - 1  # normalize kick with some tolerance
            raw_state.append(np.array(kick))
            '''

        # include 10 last changes in the objective function as state
        #self.history_objective = np.roll(self.history_objective, 1, axis=0)
        #self.history_objective[0] = loss - self.history_objective[1]

        raw_state.append((2 * (np.array(self.beampos) - (-0.1)) / (0.1 - (-0.1)) - 1))  # add the two oscillation amplitude positions, approximately normalized
        raw_state.append(np.array(self.off_set))  # add sum of actions (+ initial delays = absolute action)

        raw_state.append(np.array([2 * self.current_step / self.max_steps - 1]))  # append steps spent relative to max_steps (normalized)
        #raw_state.append(self.history_objective)  # add the changes in the objective function
        raw_state = np.array(raw_state, dtype=object).flatten()
        state = np.concatenate(raw_state, axis=0)  # concatenate into 1D array

        return state

    # Method to extract losses depending on the normalised actions takens
    def _get_reward(self, real_action, proposed_action):
        self.beampos = self.mkp_delays.get_positions(real_action)
        self.log_data["bpm_pos"].append(self.beampos)
        x1bar = (
                        abs(np.max(self.beampos[0])) + abs(np.min(self.beampos[0]))
                ) / 2
        x2bar = (
                        abs(np.max(self.beampos[1])) + abs(np.min(self.beampos[1]))
                ) / 2
        sq_sum_bpm = x1bar ** 2 + x2bar ** 2 + (x1bar - x2bar) ** 2

        '''
        action_penalty = 0
        for a in proposed_action:
            action_penalty += 1 / (1 + exp(-30 * (a - 1))) + \
                             1 / (1 + exp(30 * (a + 1)))
        '''
        reward = -1 * sq_sum_bpm # - action_penalty
        reward = 2 * (reward - (-1e-3)) / (-1e-4 - (-1e-3)) - 1
        reward = np.clip(reward, -1, 1)

        return reward, real_action, sq_sum_bpm

    def _reward_dangling(self, gamma=0.99):
        self.objective = min(self.objective * gamma, self.final_objective)
        print('New objective: {}'.format(self.objective))

    # Method to render plots
    def render(self, mode: str = "human") -> t.Any:
        if mode == "human":
            _, axes = plt.subplots()
            self.update_axes(axes)
            plt.show()
            return None
        if mode == "matplotlib_figures":
            if self.figure is None:
                self.figure = plt.figure()
                axes = self.figure.subplots()
            else:
                [axes] = self.figure.axes
            self.update_axes(axes)
            return [self.figure]
        return super().render(mode)

    # Method to clear japc subscriptions
    def close(self):
        self.japc.clearSubscriptions()
        self.japc.stopSubscriptions()

    # Update plot axes for bpm positions and rewards
    def update_axes(self, axes: Axes) -> None:
        """Render this problem into the given axes."""
        _ylim = axes.get_ylim()
        axes.clear()

        if self.log_data["bpm_pos"] != []:
            dtau = self.inv_norm_data(self.off_set)
            # Check at what index to evaluate the MKP kick data
            t = self.mkp_delays.ref_timestamp  # reference time for kick data evaluation

            t_inds = []
            for i in range(8):  # 8 switches in total to iterate over
                ts = 2 * t - (self.dtc / 2) - dtau[i]
                t_ind = np.argwhere(self.time_data == (
                        ts - (ts % 2)))  # find closest even index corresponding to this time
                t_inds.append(t_ind)

            switch_count = 0
            for i in range(16):
                if ((i % 2) == 0) and (i != 0):  # after every two steps, change electrical switch
                    switch_count += 1
                min_index = t_inds[switch_count].item() - 128
                max_index = t_inds[switch_count].item() + 128
                t_center = np.argwhere(self.time_data == (t - (self.dtc / 2) - ((t - (self.dtc / 2)) % 2))).item()

                axes.plot(
                    self.mkp_delays.time_data[(t_center - 128):(t_center + 128)],
                    1000 * self.mkp_delays.mkp_waveforms[i][min_index:max_index],
                    label="_nolegend_",
                )
        else:
            axes.plot([])
        # axes.set_xlim(4e3, 6e3)
        axes.axvline(x=self.mkp_delays.ref_timestamp, color='b', ls='--',
                     label='Injected beam')  # why does the GUI crash when we include these=?
        axes.axvline(x=(self.mkp_delays.ref_timestamp - self.dtc), color='c', ls='--', label='Circulating beam')
        axes.set_ylabel("Kick [mrad]")
        axes.set_xlabel("Time [ns]")
        axes.legend(loc=4)
        axes.grid()

    # Normalize data between -1 and 1, by performing this operation:
    """
    X is in the range [a, b]
    X-a is in the range [0, b-a], and delta = b-a
    (x-a)/delta is in the range [0, 1]
    2*(x-a)/delta is in the range [0, 2]
    2*(x-a)/delta -1 is in the range [-1, 1]
    """

    def norm_data(self, x_data):
        x_data_norm = np.zeros(len(x_data))
        delta = self.limits[1, :] - self.limits[0, :]
        x_data_norm = 2 * ((x_data - self.limits[0, :]) / delta) - 1

        return x_data_norm

    # Un-normalize normalized data
    def inv_norm_data(self, x_norm):
        x_data = np.zeros(len(x_norm))
        delta = self.limits[1, :] - self.limits[0, :]
        x_data = (x_norm + 1) / 2 * delta + self.limits[0, :]

        return x_data


class mkpOpt(MKPOptEnv):
    metadata = {
        "render.modes": ["human", "matplotlib_figures"],
        "cern.machine": Machine.SPS,
        "cern.japc": False,
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, use_surrogate=True, **kwargs)


class mkpOptReal(MKPOptEnv):
    metadata = {
        "render.modes": ["human", "matplotlib_figures"],
        "cern.machine": Machine.SPS,
        "cern.japc": True,
        "cern.cancellable": True,
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


register(
    "mkpOpt-v0",
    entry_point=mkpOpt,
)

register(
    "mkpOptReal-v0",
    entry_point=mkpOptReal,
    # kwargs=
)
