"""
RL test wrapper to create fake MKP delays environment, testing out an RL agent with Stable Baselines 3
--> this versions uses environment that implements the relative action

v2 is tailored to the setting without a reward target but rather a fixed amount of steps
"""

import numpy as np
import matplotlib.pyplot as plt
import pickle
import time

# import gym
import os

from stable_baselines3 import PPO, A2C
from stable_baselines3 import TD3, SAC
#from sb3_contrib import TQC
from stable_baselines3.common.noise import NormalActionNoise
from stable_baselines3.common.env_checker import check_env
# from stable_baselines3.common.vec_env import VecNormalize
# from stable_baselines3.common.env_util import make_vec_env
from stable_baselines3.common.callbacks import EvalCallback
from mkpdelays_env_RL_delta_action_v16 import MKPOptEnv  # import environment using relative action
from custom_feature_extractor import CustomCombinedExtractor
from stable_baselines3 import HerReplayBuffer
from stable_baselines3.her.goal_selection_strategy import GoalSelectionStrategy
from itertools import product

# Initiate some values and flags
n_steps = 1000000
dtc = 250
max_rise_time_change = 0.2
algorithm = 'SAC'
run_nr = 78
seed = 144
hyperparameters = []  # expects a list of lists, each containing
hyperparameter_name = []  # expects a list containing the names of the hyperparameters
save_str = "{}_run_{}".format(algorithm, run_nr)

os.mkdir('plots_and_data_delta_action/experiment_{}'.format(run_nr))

combination = 1

for ele in product(hyperparameters):  # try every hyperparameter combination

    # Initiate environment
    env = MKPOptEnv(dtc=dtc, objective=-5e-4, run_nr=run_nr, max_rise_time_change=max_rise_time_change, max_steps=10)   # also include batch spacing time separation
    env.seed(144)

    # Separate evaluation env
    eval_env = MKPOptEnv(dtc=dtc, objective=-5e-4, run_nr=run_nr, max_rise_time_change=max_rise_time_change, max_steps=10)
    # Use deterministic actions for evaluation
    eval_callback = EvalCallback(eval_env,
                                 best_model_save_path='plots_and_data_delta_action/experiment_{}/'.format(run_nr),
                                 log_path='plots_and_data_delta_action/experiment_{}/'.format(run_nr),
                                 eval_freq=5000,  # each eval_freq steps the agent is tested on the evaluation env
                                 deterministic=True,
                                 render=False)

    # ------------------- DEFINE TRAINING ALGORITHM --------------------------------------
    # Train the agent - TD3, here we use action noise
    if algorithm == 'TD3':
        start = time.time()
        n_actions = env.action_space.shape[-1]
        action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.05 * np.ones(n_actions))

        replay_buffer_kwargs = dict(handle_timeout_termination=False)

        '''
        # Use custom feature extractor
        policy_kwargs = dict(
            features_extractor_class=CustomCombinedExtractor
        )
        
        model = TD3("MultiInputPolicy",
                    env,
                    verbose=1,
                    seed=seed,
                    action_noise=action_noise,
                    buffer_size=500000,
                    learning_starts=1000,
                    policy_kwargs=policy_kwargs)
        '''
        '''
        # Model call with HER (Hindsight Experience Replay)
        model = TD3("MultiInputPolicy",
                    env,
                    verbose=1,
                    seed=seed,
                    action_noise=action_noise,
                    buffer_size=500000,
                    learning_starts=1000,
                    replay_buffer_class=HerReplayBuffer,
                    replay_buffer_kwargs=dict(
                        n_sampled_goal=4,
                        goal_selection_strategy='future')
                    )
        '''
        model = TD3("MlpPolicy",
                    env,
                    verbose=1,
                    seed=seed,
                    action_noise=action_noise,
                    buffer_size=500000,
                    learning_starts=1000,
                    replay_buffer_kwargs=replay_buffer_kwargs,
                    optimize_memory_usage=True  # slower, but requires less RAM
                    )

        model.learn(total_timesteps=n_steps, callback=eval_callback, log_interval=50)
        end = time.time()

    # Train the agent - A2C, and clock the training
    if algorithm == 'A2C':
        start = time.time()
        model = A2C('MlpPolicy', env, seed=seed, verbose=1).learn(n_steps)
        end = time.time()

    # Train agent PPO
    if algorithm == 'PPO':
        start = time.time()
        model = PPO('MlpPolicy', env, seed=seed, verbose=1).learn(total_timesteps=n_steps)
        end = time.time()
    # ------------------------------------------------------------------------------------

    # Train agent SAC
    if algorithm == 'SAC':
        policy_kwargs = dict(net_arch=[256, 256])

        start = time.time()
        model = SAC('MlpPolicy',
                    env,
                    gamma=0.99,
                    seed=seed,
                    verbose=1,
                    buffer_size=1000000,
                    learning_starts=1000,
                    target_entropy=-8,
                    use_sde=True,
                    policy_kwargs=policy_kwargs)

        model.learn(total_timesteps=n_steps, callback=eval_callback, log_interval=50)
        end = time.time()
    # ------------------------------------------------------------------------------------

    # Train agent TQC
    if algorithm == 'TQC':
        policy_kwargs = dict(net_arch=dict(pi=[256, 256], qf=[512, 512, 512]),
                             n_critics=5)

        start = time.time()
        model = TQC('MlpPolicy',
                    env,
                    gamma=0.99,
                    seed=seed,
                    verbose=1,
                    buffer_size=1000000,
                    learning_starts=1000,
                    target_entropy=-8,
                    use_sde=True,
                    policy_kwargs=policy_kwargs)

        model.learn(total_timesteps=n_steps, callback=eval_callback, log_interval=50)
        end = time.time()

    model.save("plots_and_data_delta_action/experiment_{}/model_{}_steps_{}_ns_dtc_{}_hp_{}".format(
            run_nr, n_steps, dtc, save_str, combination))

    with open(
        "plots_and_data_delta_action/experiment_{}/episode_data_{}_steps_{}_ns_dtc_{}_hp_{}.pickle".format(
                run_nr, n_steps, dtc, save_str, combination), "wb") as handle:
        pickle.dump(env.episode_data, handle, protocol=pickle.HIGHEST_PROTOCOL)

    with open(
        "plots_and_data_delta_action/experiment_{}/trained_models.txt".format(
                run_nr, n_steps, dtc, save_str, combination), "a") as handle:
        handle.write('{}: '.format(combination))
        for pair in zip(hyperparameter_name, ele):
            handle.write(pair)

        handle.write('\n')

    combination += 1
