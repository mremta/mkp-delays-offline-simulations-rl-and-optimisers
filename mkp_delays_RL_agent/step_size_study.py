"""
RL test wrapper to create fake MKP delays environment, testing out an RL agent with Stable Baselines 3
--> this versions uses environment that implements the relative action

v2 is tailored to the setting without a reward target but rather a fixed amount of steps
"""

import numpy as np
import matplotlib.pyplot as plt
import pickle
import time

import gym
import os

from stable_baselines3 import PPO, A2C
from stable_baselines3 import TD3, SAC
# from sb3_contrib import TQC
from stable_baselines3.common.noise import NormalActionNoise
from stable_baselines3.common.env_checker import check_env
# from stable_baselines3.common.vec_env import VecNormalize
# from stable_baselines3.common.env_util import make_vec_env
from stable_baselines3.common.callbacks import EvalCallback
from mkpdelays_env_RL_delta_action_v17 import MKPOptEnv  # import environment using relative action
from custom_feature_extractor import CustomCombinedExtractor
from stable_baselines3 import HerReplayBuffer
from stable_baselines3.her.goal_selection_strategy import GoalSelectionStrategy

# Initiate some values and flags
save_plots = True
n_steps = 1000000
dtc = 200
max_rise_time_change = 0.2
check_env_flag = False
algorithm = 'PPO'
run_nr = 124  # starting number
seed = 144


step_sizes = [0.005, 0.01, 0.015, 0.02]  # 104-110

hyperparameters = []

# 5 ns
hyperparameters.append({
    "n_steps": 2048,
    "batch_size": 16,
    "gamma": 0.85,
    "learning_rate": 3e-05,
    "ent_coef": 0.0,
    "clip_range": 0.2,
    "n_epochs": 10,
    "gae_lambda": 1.0,
    "max_grad_norm": 0.5,
    "vf_coef": 0.5,
    # "sde_sample_freq": sde_sample_freq,
    "policy_kwargs": dict(
        # log_std_init=log_std_init,
        net_arch=dict(pi=[64, 64], vf=[64, 64]),
    ),
})

# 10 ns
hyperparameters.append({
    "n_steps": 2048,
    "batch_size": 64,
    "gamma": 0.9,
    "learning_rate": 0.0003,
    "ent_coef": 0.0,
    "clip_range": 0.2,
    "n_epochs": 10,
    "gae_lambda": 1.0,
    "max_grad_norm": 0.5,
    "vf_coef": 0.5,
    # "sde_sample_freq": sde_sample_freq,
    "policy_kwargs": dict(
        # log_std_init=log_std_init,
        net_arch=dict(pi=[128], vf=[128]),
    ),
})

# 15 ns
hyperparameters.append({
    "n_steps": 2048,
    "batch_size": 8,
    "gamma": 0.85,
    "learning_rate": 0.0003,
    "ent_coef": 0.0,
    "clip_range": 0.2,
    "n_epochs": 10,
    "gae_lambda": 1.0,
    "max_grad_norm": 0.5,
    "vf_coef": 0.5,
    # "sde_sample_freq": sde_sample_freq,
    "policy_kwargs": dict(
        # log_std_init=log_std_init,
        net_arch=dict(pi=[32], vf=[32]),
    ),
})

# 15 ns with lower gamma
hyperparameters.append({
    "n_steps": 2048,
    "batch_size": 32,
    "gamma": 0.80,
    "learning_rate": 0.0003,
    "ent_coef": 0.0,
    "clip_range": 0.2,
    "n_epochs": 10,
    "gae_lambda": 1.0,
    "max_grad_norm": 0.5,
    "vf_coef": 0.5,
    # "sde_sample_freq": sde_sample_freq,
    "policy_kwargs": dict(
        # log_std_init=log_std_init,
        net_arch=dict(pi=[128, 128], vf=[128, 128]),
    ),
})

# 20 ns
hyperparameters.append({
    "n_steps": 2048,
    "batch_size": 16,
    "gamma": 0.85,
    "learning_rate": 0.0003,
    "ent_coef": 0.0,
    "clip_range": 0.2,
    "n_epochs": 10,
    "gae_lambda": 1.0,
    "max_grad_norm": 0.5,
    "vf_coef": 0.5,
    # "sde_sample_freq": sde_sample_freq,
    "policy_kwargs": dict(
        # log_std_init=log_std_init,
        net_arch=dict(pi=[16, 16], vf=[16, 16]),
    ),
})

step_sizes = [0.015]

count = 3
for ele in step_sizes:

    save_str = "{}_run_{}".format(algorithm, run_nr)

    os.mkdir('plots_and_data_delta_action/experiment_{}'.format(run_nr))

    # Initiate environment
    env = MKPOptEnv(dtc=dtc, max_rise_time_change=max_rise_time_change, max_steps=1000, action_scaling=ele,
                    verbose=True)  # also include batch spacing time separation
    env.seed(144)

    if check_env_flag:
        check_env(env, warn=True)  # If the environment don't follow the interface, an error will be thrown

    # Separate evaluation env
    eval_env = MKPOptEnv(dtc=dtc, max_rise_time_change=max_rise_time_change, max_steps=1000, action_scaling=ele, verbose=True)
    # Use deterministic actions for evaluation
    eval_callback = EvalCallback(eval_env,
                                 best_model_save_path='plots_and_data_delta_action/experiment_{}/'.format(run_nr),
                                 log_path='plots_and_data_delta_action/experiment_{}/'.format(run_nr),
                                 eval_freq=5000,  # each eval_freq steps the agent is tested on the evaluation env
                                 deterministic=True,
                                 render=False)

    # Perform a reset command
    obs = env.reset()
    # env.render()

    kwargs = hyperparameters[count]

    start = time.time()
    model = PPO(policy='MlpPolicy', env=env, seed=seed, verbose=1, **kwargs).learn(total_timesteps=n_steps)
    end = time.time()


    # Save model
    if save_plots:
        model.save("plots_and_data_delta_action/experiment_{}/model_{}_steps_{}_ns_dtc_{}".format(
            run_nr, n_steps, dtc, save_str))

    # Serialize and save data
    if save_plots:
        with open(
                "plots_and_data_delta_action/experiment_{}/episode_data_{}_steps_{}_ns_dtc_{}.pickle".format(
                    run_nr, n_steps, dtc, save_str
                ),
                "wb",
        ) as handle:
            pickle.dump(
                env.episode_data, handle, protocol=pickle.HIGHEST_PROTOCOL
            )
    run_nr += 1
    count += 1

