"""
File to check phase advance from the kickers to bpm
"""


import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from cpymad.madx import Madx
from pybt.tools.parsers import read_twiss_file
from numpy import sqrt, sin, cos, pi


twiss = read_twiss_file('data_and_sequences/twiss_tt2tt10sps_lhc_q20_nom.tfs')
twiss_sps = twiss[1]

print(twiss_sps.mux['mkpc.11952'] * 360)
print(twiss_sps.mux['bpmbv.51503'] * 360)
print((twiss_sps.mux['bpmbv.51303'] * 360 - twiss_sps.mux['mkpc.11952'] * 360) % 360)
print((twiss_sps.mux['bpmbh.51999'] * 360 - twiss_sps.mux['mkpc.11952'] * 360) % 360)
print(twiss_sps.mux['vvsb.11832'] * 360)

print([twiss_sps.x['mkpa.11931'], twiss_sps.px['mkpa.11931']])


optics = 'q20'
elements = ['mkpa.11931', 'mkpa.11936', 'mkpc.11952', 'mkp.11955']

# generate twiss for injected beam
with open('tempfile', 'w') as f:
    madx = Madx(stdout=f, stderr=f)
madx.option(echo=False, warn=True, info=False, debug=False, verbose=False)


madx.input('select,flag=twiss,clear;')
madx.select(flag='twiss',
            column=['N1', 'apertype', 'aper_1', 'aper_2', 'aper_3', 'aper_4', 'apoff_1', 'apoff_2',
                    'aptol_1', 'aptol_2', 'aptol_3'])
madx.options['update_from_parent'] = True

# Load sequence and initial conditions for short_injection_and_some_sps, to get the injected beam and about 412 m into the SPS
madx.call('data_and_sequences/tt2tt10sps_{}_mod.seq'.format(optics))
#madx.call('data_and_sequences/short_injection_and_some_sps_{}.seq'.format(optics))
# Load Twiss and survey parameters
twiss_reversed = pd.read_csv("data_and_sequences/sps_injection_twiss_reversed_{}.csv".format(optics))
twiss_beta1 = twiss_reversed.iloc[
    -1]  # locate Twiss parameters at the start of the loaded reversed sequence, initial conditions for later

# Call the old aperture files for SPS, for the injection sequence
madx.use(sequence="tt2tt10sps")
madx.call("data_and_sequences/aperturedb_1.dbx")
madx.call("data_and_sequences/aperturedb_2.dbx")
madx.call("data_and_sequences/aperturedb_3.dbx")

#madx.call('data_and_sequences/tt2tt10sps_{}.seq'.format(optics))
madx.input('''EXTRACT,
                SEQUENCE="tt2tt10sps",
                FROM=end_point,
                TO=end_point_3,
                NEWNAME=even_shorter;''')
madx.input('''SAVE,
                SEQUENCE=even_shorter,
                FILE=even_shorter.seq,
                BEAM=TRUE;''')

'''
madx.use(sequence='even_shorter')
twiss = madx.twiss(betx=twiss_beta1['betx'], alfx=-twiss_beta1['alfx'],
                                bety=twiss_beta1['bety'], alfy=-twiss_beta1['alfy'],
                                dx=twiss_beta1['dx'], dpx=-twiss_beta1['dpx'],
                                dy=twiss_beta1['dy'], dpy=-twiss_beta1['dpy'],
                                x=twiss_beta1['x'], y=twiss_beta1['y'],
                                px=-twiss_beta1['px'], py=-twiss_beta1['py']).dframe()
'''

