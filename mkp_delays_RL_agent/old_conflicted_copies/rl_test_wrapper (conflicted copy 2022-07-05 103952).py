"""
RL test wrapper to create fake MKP delays environment, testing out an RL agent with Stable Baselines 3
"""

import numpy as np
import matplotlib.pyplot as plt

from stable_baselines3 import PPO, A2C # DQN coming soon
from stable_baselines3.common.env_util import make_vec_env

from stable_baselines3.common.env_checker import check_env
from mkpdelays_env_RL import MKPOptEnv

# initiate environment
dtc = 250
env =  MKPOptEnv(dtc=dtc)   # also include batch spacing time separation

# If the environment don't follow the interface, an error will be thrown
#check_env(env, warn=True)  # --> for now works without error messages 

# Perform a reset command
obs = env.reset()

# wrap the environment, if desired in vectorized format 
#env = make_vec_env(lambda: env, n_envs=1)

#"""
# ------------------- DEFINE TRAINING ALGORITHM --------------------------------------
# Train the agent - TD3, here we use action noise
#n_actions = env.action_space.shape[-1]
#action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.1 * np.ones(n_actions))
#model = TD3("MlpPolicy", env, action_noise=action_noise, verbose=1)
#odel.learn(total_timesteps=10000, log_interval=10)

# Train the agent - A2C
model = A2C('MlpPolicy', env, verbose=1).learn(500)

# Train agent PPO
#model = PPO('MlpPolicy', env, verbose=1)
#model.learn(total_timesteps=5000)
# ------------------------------------------------------------------------------------


# Test the trained agent
obs = env.reset()
n_steps = 20
for step in range(n_steps):
  action, _ = model.predict(obs, deterministic=True)
  print("Step {}".format(step + 1))
  print("Action: ", action)
  obs, reward, done, info = env.step(action)
  print('obs=', obs, 'reward=', reward, 'done=', done)
  env.render()
  if done:
    print("Goal reached!", "reward=", reward)
    break

#"""
print("\nFinal real action: {}".format(env.log_data["real_actions"][-1]))



#Also plot reward over time 
fig = plt.figure(figsize=(10,7))
ax = fig.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14) 
plt.yticks(fontsize=14)
ax.plot(env.episode_data["reward_sum"])
ax.set_xlabel("Training episode")
ax.set_ylabel("Reward")
#ax.set_yscale('log')   # does not work for negative values 

# plot resulting beam oscillations
fig2 = plt.figure(figsize=(10,7))
env.mkp_delays.plot_oscillations(fig2)