"""
File to extract the twiss parameters and calculate the transfer matrices
"""


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from cpymad.madx import Madx
from numpy import sqrt, sin, cos, pi


# function to calculate the transfer matrix from the twiss parameters
def transfer_matrix(alpha_0, alpha, beta_0, beta, mu_0, mu):
    c = sqrt(beta / beta_0) * (cos(mu - mu_0) + alpha_0 * sin(mu - mu_0))
    c_prime = ((alpha_0 - alpha) * cos(mu - mu_0) - (1 + alpha * alpha_0) * sin(mu - mu_0)) / sqrt(beta * beta_0)
    s = sqrt(beta * beta_0) * sin(mu - mu_0)
    s_prime = sqrt(beta_0 / beta) * (cos(mu - mu_0) - alpha * sin(mu - mu_0))

    transfer = np.array([[c, s],
                        [c_prime, s_prime]])
    return transfer

optics = 'q20'
elements = ['mkpa.11931', 'mkpa.11936', 'mkpc.11952', 'mkp.11955']

# generate twiss for injected beam
with open('tempfile', 'w') as f:
    madx = Madx(stdout=f, stderr=f)
madx.option(echo=False, warn=True, info=False, debug=False, verbose=False)

# Injection sequence
madx.input('select,flag=twiss,clear;')
madx.select(flag='twiss',
            column=['N1', 'apertype', 'aper_1', 'aper_2', 'aper_3', 'aper_4', 'apoff_1', 'apoff_2',
                    'aptol_1', 'aptol_2', 'aptol_3'])
madx.options['update_from_parent'] = True

# Load sequence and initial conditions for short_injection_and_some_sps, to get the injected beam and about 412 m into the SPS
madx.call('data_and_sequences/short_injection_and_some_sps_{}_mod.seq'.format(optics))
# Load Twiss and survey parameters
twiss_reversed = pd.read_csv("data_and_sequences/sps_injection_twiss_reversed_{}.csv".format(optics))
twiss_beta1 = twiss_reversed.iloc[
    -1]  # locate Twiss parameters at the start of the loaded reversed sequence, initial conditions for later

# Call the old aperture files for SPS, for the injection sequence
madx.use(sequence="short_injection_and_some_sps")
madx.call("data_and_sequences/aperturedb_1.dbx")
madx.call("data_and_sequences/aperturedb_2.dbx")
madx.call("data_and_sequences/aperturedb_3.dbx")

# Extract beam info - same for injected and circulating
madx.use(sequence='short_injection_and_some_sps')
sige = madx.sequence['short_injection_and_some_sps'].beam['sige']  # relative energy spread
ex = madx.sequence['short_injection_and_some_sps'].beam['ex']
ey = madx.sequence['short_injection_and_some_sps'].beam['ey']

madx.use(sequence='short_injection_and_some_sps')
twiss = madx.twiss(betx=twiss_beta1['betx'], alfx=-twiss_beta1['alfx'],
                                bety=twiss_beta1['bety'], alfy=-twiss_beta1['alfy'],
                                dx=twiss_beta1['dx'], dpx=-twiss_beta1['dpx'],
                                dy=twiss_beta1['dy'], dpy=-twiss_beta1['dpy'],
                                x=twiss_beta1['x'], y=twiss_beta1['y'],
                                px=-twiss_beta1['px'], py=-twiss_beta1['py']).dframe()


# find the parameters for the transfer matrices
betx = twiss['betx']
alfx = twiss['alfx']
mux = twiss['mux'] * 2 * pi  # the phase advance is in units of 2pi
x = twiss['x']
px = twiss['px']

# calculate transfer matrices
start = 'drift_10[0]'
end = 'mkpa.11931'
tm_01 = transfer_matrix(alfx[start], alfx[end], betx[start], betx[end], mux[start], mux[end])

start = 'mkpa.11931'
end = 'drift_11[0]'
tm_02 = transfer_matrix(alfx[start], alfx[end], betx[start], betx[end], mux[start], mux[end])

start = 'drift_11[0]'
end = 'mkpa.11936'
tm_03 = transfer_matrix(alfx[start], alfx[end], betx[start], betx[end], mux[start], mux[end])

start = 'mkpa.11936'
end = 'drift_12[0]'
tm_04 = transfer_matrix(alfx[start], alfx[end], betx[start], betx[end], mux[start], mux[end])

start = 'drift_12[0]'
end = 'mkpc.11952'
tm_05 = transfer_matrix(alfx[start], alfx[end], betx[start], betx[end], mux[start], mux[end])

start = 'mkpc.11952'
end = 'drift_13[0]'
tm_06 = transfer_matrix(alfx[start], alfx[end], betx[start], betx[end], mux[start], mux[end])

start = 'drift_13[0]'
end = 'mkp.11955'
tm_07 = transfer_matrix(alfx[start], alfx[end], betx[start], betx[end], mux[start], mux[end])

# transport to injection bump
start = 'mkp.11955'
end = 'drift_19[0]'
tm_08 = transfer_matrix(alfx[start], alfx[end], betx[start], betx[end], mux[start], mux[end])

start = 'drift_19[0]'
end = 'mdh.12007'
tm_09 = transfer_matrix(alfx[start], alfx[end], betx[start], betx[end], mux[start], mux[end])

ele = 'end_point'
start = 'mdh.12007'
end = ele
tm_10 = transfer_matrix(alfx[start], alfx[end], betx[start], betx[end], mux[start], mux[end])

# default kicks
mkp_volt = 52

mkpa_kv2rad = 2.036307619e-05
mkpc_kv2rad = 8.140618226e-06
mkpl_kv2rad = 2.147001577e-05

kmkpa11931 = mkp_volt*mkpa_kv2rad
kmkpa11936 = mkp_volt*mkpa_kv2rad
kmkpc11952 = mkp_volt*mkpc_kv2rad
kmkp11955 = mkp_volt*mkpl_kv2rad
kmdh12007 = 1.4668e-05 * 13.34737096

# Transport through kickers
print([x['drift_10[0]'], px['drift_10[0]']])  # starting point
# kick 1
result = np.matmul(tm_01, [0, 0 + kmkpa11931])  # thin kick
result = result - [kmkpa11931 * 3.423 / 2, 0]  # linear correction for x
print('after first kick')
print(result)
#print([x['drift_10[0]'] + px['drift_10[0]'] * 3.423 + kmkpa11931 * 3.423 / 2, px['drift_10[0]'] + kmkpa11931])
#print([x['mkpa.11931'], px['mkpa.11931']])
# between kick 1 and 2
result = np.matmul(tm_02, result)
print('transport')
#print(twiss['l']['drift_11[0]'])
print(result)
#print([x['mkpa.11931'] + px['mkpa.11931'] * (0.19899999999999807), px['mkpa.11931']])
#print([x['drift_11[0]'], px['drift_11[0]']])
# kick 2
result = np.matmul(tm_03, result + [0, kmkpa11936])  # thin kick
result = result - [kmkpa11936 * 3.423 / 2, 0]  # linear correction for x
print('Kick 2')
print(result)
# between kick 2 and 3
result = np.matmul(tm_04, result)
# kick 3
result = np.matmul(tm_05, result + [0, kmkpc11952])  # thin kick
result = result - [kmkpc11952 * 1.78 / 2, 0]  # linear correction for x
# between kick 3 and 4
result = np.matmul(tm_06, result)
# kick 4
result = np.matmul(tm_07, result + [0, kmkp11955])  # thin kick
result = result - [kmkp11955 * 3.423 / 2, 0]  # linear correction for x
print(result)
# between kick 4 and injection bump
result = np.matmul(tm_08, result)
# injection bump
result = np.matmul(tm_09, result + [0, kmdh12007])  # thin kick
result = result - [kmdh12007 * 0.25 / 2, 0]  # linear correction for x
# transport
result = np.matmul(tm_10, result)

# check, whether MADX and transfer matrices agree
print(result)
print([x[ele], px[ele]])

# save transfer matrices for further use
transfer_matrices = np.array([tm_01, tm_02, tm_03, tm_04, tm_05, tm_06, tm_07, tm_08, tm_09, tm_10])
for i in np.arange(1, 11):
    np.savetxt('data_and_sequences/transfer_matrix_{}.txt'.format(i), X=transfer_matrices[i-1])

# now we work on the transport from just after the kickers to the bpm
# start with a twiss of the shorter sequence
madx.input('select,flag=twiss,clear;')
madx.select(flag='twiss',
            column=['N1', 'apertype', 'aper_1', 'aper_2', 'aper_3', 'aper_4', 'apoff_1', 'apoff_2',
                    'aptol_1', 'aptol_2', 'aptol_3'])

madx.call('even_shorter.seq'.format(optics))
madx.use(sequence='even_shorter')

# calculate twiss and check, whether the output is consistent with the full sequence
twiss_after_kickers = (madx.twiss(betx=twiss['betx']['end_point'], alfx=twiss['alfx']['end_point'],
                                bety=twiss['bety']['end_point'], alfy=twiss['alfy']['end_point'],
                                dx=twiss['dx']['end_point'], dpx=twiss['dpx']['end_point'],
                                dy=twiss['dy']['end_point'], dpy=twiss['dpy']['end_point'],
                                x=twiss['x']['end_point'], y=twiss['y']['end_point'],
                                px=twiss['px']['end_point'], py=twiss['py']['end_point'],
                                rmatrix=True,
                                sectormap=True,  # calc sectormap
                                sectoracc=True  # aggregate sectormap
                                  ).dframe())

initial_cond = np.array([twiss_after_kickers['x']['end_point'],
                         twiss_after_kickers['px']['end_point'],
                         twiss_after_kickers['y']['end_point'],
                         twiss_after_kickers['py']['end_point'],
                         twiss_after_kickers['t']['end_point'],
                         twiss_after_kickers['pt']['end_point']])

final_element = 'end_point_2'
final_cond = np.array([twiss_after_kickers['x'][final_element],
                       twiss_after_kickers['px'][final_element],
                       twiss_after_kickers['y'][final_element],
                       twiss_after_kickers['py'][final_element],
                       twiss_after_kickers['t'][final_element],
                       twiss_after_kickers['pt'][final_element]])


print(twiss_after_kickers['x'])

# retrieve R and T matrices
rmatrix = madx.sectortable()
tmatrix = madx.sectortable2()

print('Transport')
first_order = np.matmul(rmatrix[-1][:6, :6], initial_cond)[0]
second_order = np.sum(tmatrix[-1][0] * np.outer(initial_cond, initial_cond))
#print(first_order)
#print(second_order)
print(first_order + second_order)
print(final_cond)

# finally, we need to save the initial conditions for the circulating and the injected beam
# we already ran a twiss for the injected beam
# generate twiss for circulating beam

with open('tempfile2', 'w') as f:
    madx2 = Madx(stdout=f, stderr=f)
madx2.option(echo=False, warn=True, info=False, debug=False, verbose=False)

madx2.input('select,flag=twiss,clear;')
madx2.select(flag='twiss',
                          column=['N1', 'apertype', 'aper_1', 'aper_2', 'aper_3', 'aper_4', 'apoff_1', 'apoff_2',
                                  'aptol_1', 'aptol_2', 'aptol_3'])
madx2.options['update_from_parent'] = True

# Load sequence and initial conditions for the circulating beam
madx2.call('data_and_sequences/reduced_circulating_sps_{}.seq'.format(optics))
initbeta0 = np.genfromtxt('data_and_sequences/reduced_circulating_sps_twiss_init_{}.csv'.format(optics))

# Call the old aperture files for SPS
madx2.use(sequence="reduced_circulating_sps")
madx2.call("data_and_sequences/aperturedb_1.dbx")
madx2.call("data_and_sequences/aperturedb_2.dbx")
madx2.call("data_and_sequences/aperturedb_3.dbx")

madx2.use(sequence="reduced_circulating_sps")
twiss_c = madx2.twiss(betx=initbeta0[0], alfx=initbeta0[1],
                                bety=initbeta0[2], alfy=initbeta0[3],
                                dx=initbeta0[4], dpx=initbeta0[5],
                                dy=initbeta0[6], dpy=initbeta0[7],
                                x=initbeta0[8], y=initbeta0[9],
                                px=initbeta0[10], py=initbeta0[11]).dframe()

# get x and px just before the kickers
init_pos = [twiss['x']['drift_10[0]'],
            twiss['px']['drift_10[0]'],
            twiss_c['x']['drift_24[0]'],
            twiss_c['px']['drift_24[0]']]
# save for further use
np.savetxt('data_and_sequences/initial_positions.txt', X=init_pos)