import optuna
import plotly.express as px
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from optuna.visualization import plot_optimization_history, plot_param_importances, plot_contour, plot_edf, plot_intermediate_values

db_url = 'postgresql://optuna_user:1234@localhost:5432/optuna_database'

study = optuna.load_study(study_name='study_PPO_15_ns', storage=db_url)

print("Number of finished trials: ", len(study.trials))

print("Best trial:")
trial = study.best_trial

print(f"  Value: {trial.value}")

print("  Params: ")
for key, value in trial.params.items():
    print(f"    {key}: {value}")

print("  User attrs:")
for key, value in trial.user_attrs.items():
    print(f"    {key}: {value}")

print(study.trials_dataframe()["state"].value_counts())

fig1 = plot_optimization_history(study)
fig2 = plot_param_importances(study)
fig3 = plot_contour(study, params=["gamma", "learning_rate"])
fig4 = plot_edf(study)
fig5 = plot_intermediate_values(study)
df = study.trials_dataframe()
df = df[(df["state"] == "COMPLETE") & (df["value"] > 650)]
fig6 = px.parallel_coordinates(df, dimensions=["params_depth", "params_width", "params_learning_rate", "params_batch_size", "params_gamma", "value"], color="value")

first_intermediate_values = [trial.intermediate_values[1] if len(trial.intermediate_values) > 0 else -99999 for trial in study.trials]

df = study.trials_dataframe()
df["first_intermediate"] = first_intermediate_values
df.to_csv("plots_and_data_delta_action/df_15_ns.csv")

fig7 = px.parallel_coordinates(df[(df["first_intermediate"] > 650)], dimensions=["params_depth", "params_width", "params_learning_rate", "params_batch_size", "params_gamma", "first_intermediate"], color="first_intermediate")

# Reshape the DataFrame for heatmap plotting
study_2 = optuna.load_study(study_name='study_PPO_15_ns_2', storage=db_url)

first_intermediate_values_2 = [trial.intermediate_values[1] if len(trial.intermediate_values) > 0 else -99999 for trial in study_2.trials]

df_2 = study_2.trials_dataframe()
df_2["first_intermediate"] = first_intermediate_values_2
df_2.to_csv("plots_and_data_delta_action/df_15_ns_2.csv")

df = pd.concat([df[['params_gamma', 'params_learning_rate', 'first_intermediate']], df_2[['params_gamma', 'params_learning_rate', 'first_intermediate']]], ignore_index=True)

idxmax_values = df[['params_gamma', 'params_learning_rate', 'first_intermediate']].groupby(['params_gamma', 'params_learning_rate'])['first_intermediate'].idxmax()

max_value_rows = df.loc[idxmax_values]

heatmap_data = max_value_rows.pivot(index='params_gamma', columns='params_learning_rate', values='first_intermediate')

# Create a 2D heatmap
plt.figure(figsize=(10, 6))
sns.heatmap(heatmap_data, annot=True, cbar_kws={'label': 'Reward sum'}, fmt=".2f")

# Adding labels and title
plt.xlabel('Learning rate')
plt.ylabel('Gamma')
plt.title('2D Heatmap of Hyperparameter Tuning Results')
plt.show()


fig1.show()
fig2.show()
fig3.show()
fig4.show()
fig5.show()
fig6.show()
fig7.show()
