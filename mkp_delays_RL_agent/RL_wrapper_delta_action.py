"""
RL test wrapper to create fake MKP delays environment, testing out an RL agent with Stable Baselines 3
--> this versions uses environment that implements the relative action
"""

import numpy as np
import matplotlib.pyplot as plt
import pickle
import time

# import gym
import os

from stable_baselines3 import PPO, A2C
from stable_baselines3 import TD3, SAC
#from sb3_contrib import TQC
from stable_baselines3.common.noise import NormalActionNoise
from stable_baselines3.common.env_checker import check_env
# from stable_baselines3.common.vec_env import VecNormalize
# from stable_baselines3.common.env_util import make_vec_env
from stable_baselines3.common.callbacks import EvalCallback
from mkpdelays_env_RL_delta_action_v15 import MKPOptEnv  # import environment using relative action
from custom_feature_extractor import CustomCombinedExtractor
from stable_baselines3 import HerReplayBuffer
from stable_baselines3.her.goal_selection_strategy import GoalSelectionStrategy

# Initiate some values and flags 
save_plots = True
n_steps = 500000
dtc = 250
max_rise_time_change = 0.1
check_env_flag = False
algorithm = 'TD3'
run_nr = 62
seed = 144
save_str = "{}_run_{}".format(algorithm, run_nr)

os.mkdir('plots_and_data_delta_action/experiment_{}'.format(run_nr))

# 'dict(net_arch=[400, 300])'

# Initiate environment
env = MKPOptEnv(dtc=dtc, objective=-5e-4, run_nr=run_nr, max_rise_time_change=max_rise_time_change)   # also include batch spacing time separation
if check_env_flag:
    check_env(env, warn=True)  # If the environment don't follow the interface, an error will be thrown

# Separate evaluation env
eval_env = MKPOptEnv(dtc=dtc, objective=-5e-4, run_nr=run_nr, max_rise_time_change=max_rise_time_change)
# Use deterministic actions for evaluation
eval_callback = EvalCallback(eval_env,
                             best_model_save_path='plots_and_data_delta_action/experiment_{}/'.format(run_nr),
                             log_path='plots_and_data_delta_action/experiment_{}/'.format(run_nr),
                             eval_freq=5000,  # each eval_freq steps the agent is tested on the evaluation env
                             deterministic=True,
                             render=False)

# Perform a reset command
obs = env.reset()
#env.render()

#"""
# ------------------- DEFINE TRAINING ALGORITHM --------------------------------------
# Train the agent - TD3, here we use action noise
if algorithm == 'TD3':
    start = time.time()
    n_actions = env.action_space.shape[-1]
    action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.05 * np.ones(n_actions))

    replay_buffer_kwargs = dict(handle_timeout_termination=False)



    # Use custom feature extractor
    policy_kwargs = dict(
        features_extractor_class=CustomCombinedExtractor
    )
    '''
    model = TD3("MultiInputPolicy",
                env,
                verbose=1,
                seed=seed,
                action_noise=action_noise,
                buffer_size=500000,
                learning_starts=1000,
                policy_kwargs=policy_kwargs)
    '''
    '''
    # Model call with HER (Hindsight Experience Replay)
    model = TD3("MultiInputPolicy",
                env,
                verbose=1,
                seed=seed,
                action_noise=action_noise,
                buffer_size=500000,
                learning_starts=1000,
                replay_buffer_class=HerReplayBuffer,
                replay_buffer_kwargs=dict(
                    n_sampled_goal=4,
                    goal_selection_strategy='future')
                )
    '''
    model = TD3("MlpPolicy",
                env,
                verbose=1,
                seed=seed,
                action_noise=action_noise,
                buffer_size=500000,
                learning_starts=1000,
                replay_buffer_kwargs=replay_buffer_kwargs,
                optimize_memory_usage=True  # slower, but requires less RAM
                )

    model.learn(total_timesteps=n_steps, callback=eval_callback, log_interval=50)
    end = time.time()

# Train the agent - A2C, and clock the training 
if algorithm == 'A2C':
    start = time.time()
    model = A2C('MlpPolicy', env, seed=seed, verbose=1).learn(n_steps)
    end = time.time()

# Train agent PPO
if algorithm == 'PPO':
    start = time.time()
    model = PPO('MlpPolicy', env, seed=seed, verbose=1).learn(total_timesteps=n_steps)
    end = time.time()
# ------------------------------------------------------------------------------------

# Train agent SAC
if algorithm == 'SAC':
    policy_kwargs = dict(net_arch=[256, 256])

    start = time.time()
    model = SAC('MlpPolicy',
                env,
                gamma=0.99,
                seed=seed,
                verbose=1,
                buffer_size=1000000,
                learning_starts=1000,
                target_entropy=-8,
                use_sde=True,
                policy_kwargs=policy_kwargs)

    model.learn(total_timesteps=n_steps, callback=eval_callback, log_interval=50)
    end = time.time()
# ------------------------------------------------------------------------------------

# Train agent TQC
if algorithm == 'TQC':
    policy_kwargs = dict(net_arch=dict(pi=[256, 256], qf=[512, 512, 512]),
                         n_critics=5)

    start = time.time()
    model = TQC('MlpPolicy',
                env,
                gamma=0.99,
                seed=seed,
                verbose=1,
                buffer_size=1000000,
                learning_starts=1000,
                target_entropy=-8,
                use_sde=True,
                policy_kwargs=policy_kwargs)

    model.learn(total_timesteps=n_steps, callback=eval_callback, log_interval=50)
    end = time.time()
# ------------------------------------------------------------------------------------

'''
# Test the trained agent
obs = env.reset()
n_steps_test = 20
for step in range(n_steps_test):
  action, _ = model.predict(obs, deterministic=True)
  print("Step {}".format(step + 1))
  print("Action: ", action)
  obs, reward, done, info = env.step(action)
  print("Reward: {}".format(reward))
  if done:
    print("Goal reached!", "reward=", reward)
    break

# Final beam position, also to generate Twiss 
final_beam_pos = env.mkp_delays.step(env.inv_norm_data(action))  

print("\nFinal real action: {}".format(env.log_data["real_actions"][-1]))
'''

print("\nTraining with {} steps lasted for {:.2f} seconds!\n".format(n_steps, end - start))

'''
# Generate figure of the waveforms 
fig = env.render("matplotlib_figures")
if save_plots:
    fig[0].savefig('plots_and_data_delta_action/experiment_{}/waveforms_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)
'''

#Also plot reward over time 
fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14) 
plt.yticks(fontsize=14)
ax.plot(env.episode_data["reward_sum"], 'o', markersize=1)
ax.set_xlabel("Training episode")
ax.set_ylabel("Reward")
if save_plots:
    fig1.savefig('plots_and_data_delta_action/experiment_{}/agent_reward_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)


# calculate running average, starting at index 1 (not a zero)
def running_mean(x, N):
    cumsum = np.cumsum(np.insert(x, 0, 0))
    return (cumsum[N:] - cumsum[:-N]) / float(N)


success_percentage = running_mean(env.episode_data['success'], 100)

#Also plot reward over time
fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.plot(success_percentage)
ax.set_xlabel("Training episode")
ax.set_ylabel("Fraction of successful episodes")

fig1.savefig('plots_and_data_delta_action/experiment_{}/agent_success_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)


'''
# plot resulting beam oscillations
fig2 = plt.figure(figsize=(10,7))
env.mkp_delays.plot_oscillations(fig2)
if save_plots:
    fig2.savefig('plots_and_data_delta_action/experiment_{}/injection_oscillations_{}_steps_{}_ns_dtc_{}.png'.format(
    run_nr, n_steps, dtc, save_str), dpi=250)
'''


# Save model
if save_plots:
    model.save("plots_and_data_delta_action/experiment_{}/model_{}_steps_{}_ns_dtc_{}".format(
        run_nr, n_steps, dtc, save_str))

# Serialize and save data
if save_plots:
    with open(
        "plots_and_data_delta_action/experiment_{}/episode_data_{}_steps_{}_ns_dtc_{}.pickle".format(
            run_nr, n_steps, dtc, save_str
        ),
        "wb",
    ) as handle:
        pickle.dump(
            env.episode_data, handle, protocol=pickle.HIGHEST_PROTOCOL
        )
#"""