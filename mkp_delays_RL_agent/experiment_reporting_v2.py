"""
Script for more in-depth performance plots. Allows to analyze agent performs with respect to the difficulty of episodes
"""

import numpy as np
import pandas as pd
import pickle
import matplotlib.pyplot as plt


def running_quantiles(time_series, window_size, quantiles):
    """
    Calculate running quantiles of a time series using a sliding window.

    Parameters:
    - time_series (numpy.ndarray or list): The input time series.
    - window_size (int): The size of the sliding window.
    - quantiles (list): A list of quantiles to calculate (e.g., [0.25, 0.50, 0.75] for quartiles).

    Returns:
    - numpy.ndarray: An array containing running quantiles for each position in the time series.
    """
    time_series = np.array(time_series)
    num_data_points = len(time_series)
    num_quantiles = len(quantiles)
    running_quantile_values = np.empty((num_data_points - window_size + 1, num_quantiles))

    for i in range(num_data_points - window_size + 1):
        window = time_series[i:i + window_size]
        quantile_values = np.percentile(window, [q * 100 for q in quantiles])
        running_quantile_values[i] = quantile_values

    return running_quantile_values


# Fill out these parameters to specify which run to plot
run_nr = 127
n_steps = 1000000
dtc = 200
save_str = 'PPO_run_{}'.format(run_nr)

# read episode data
with open("plots_and_data_delta_action/experiment_{}/episode_data_{}_steps_{}_ns_dtc_{}.pickle"
          .format(run_nr, n_steps, dtc, save_str), "rb") as input_file:
    episode_data = pickle.load(input_file)

# calculate the best achieved reward for each episode
max_reward = []
for ele in episode_data["all_rewards"]:
    max_reward.append(np.max(ele))


# Plot final reward of each episode
fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.plot(max_reward, label="maximum reward")
ax.plot(episode_data["initial_reward"][2:], label="initial reward")
ax.plot(episode_data["final_reward"], label="final reward")
ax.set_xlabel("Training episode")
ax.set_ylabel("Reward")
ax.legend()
plt.grid()

fig1.savefig('plots_and_data_delta_action/experiment_{}/agent_final_reward_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)

# plot rise time change vs reward
avg_rise_time_change = []
for ele in episode_data['rise_time_change'][-500:]:
    avg_rise_time_change.append(np.mean(ele))

max_rise_time_change = []
for ele in episode_data['rise_time_change'][-500:]:
    max_rise_time_change.append(np.max(ele))

data = pd.DataFrame({'final_reward': episode_data['final_reward'][-500:],
                     'max_reward': max_reward[-500:],
                     'avg_rise_time_change': avg_rise_time_change,
                     'max_rise_time_change': max_rise_time_change})

# plot avg rise time change vs final reward
fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.plot(data['avg_rise_time_change'], data['final_reward'], 'o')
ax.set_xlabel("Avg percent increase of rise time")
ax.set_ylabel("Final reward")
plt.grid()

fig1.savefig('plots_and_data_delta_action/experiment_{}/qq_final_reward_rise_time_change_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)

# plot avg rise time change vs maximum reward
fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.plot(data['avg_rise_time_change'], data['max_reward'], 'o')
ax.set_xlabel("Avg percent increase of rise time")
ax.set_ylabel("Max reward")
plt.grid()

fig1.savefig('plots_and_data_delta_action/experiment_{}/qq_max_reward_rise_time_change_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)

# plot difference between final and inital reward
reward_difference = []

for i in np.arange(0, len(episode_data['initial_reward'])):
    reward_difference.append(episode_data['final_reward'][i] - episode_data['initial_reward'][i])

avg_initial_settings = []
for ele in episode_data['initial_settings']:
    avg_initial_settings.append(np.mean(np.array(ele)))

fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.plot(reward_difference[-500:])
ax.axhline(y=0, color='red')
ax.set_xlabel("Episode")
ax.set_ylabel("Reward improvement")
plt.grid()

fig1.savefig('plots_and_data_delta_action/experiment_{}/reward_difference_final_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)

# plot reward difference vs initial settings
fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
sc = ax.scatter(avg_initial_settings[-500:], reward_difference[-500:], c=data['avg_rise_time_change'], cmap=plt.cm.coolwarm)
plt.colorbar(sc, ax=ax, label='Average rise time change')
ax.set_xlabel("Avg initial settings")
ax.set_ylabel("Reward improvement")
plt.grid()

fig1.savefig('plots_and_data_delta_action/experiment_{}/qq_reward_difference_initial_settings_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)

# plot running quantiles of the reward difference
quantiles = running_quantiles(reward_difference, 25, [0.25, 0.5, 0.75])

fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.plot(np.arange(0, len(quantiles[:, 1])), quantiles[:, 1])
ax.fill_between(np.arange(0, len(quantiles[:, 1])), quantiles[:, 0], quantiles[:, 2], alpha=0.3)
ax.axhline(y=0, color='red')
ax.set_xlabel("Episode")
ax.set_ylabel("Reward improvement")
plt.grid()

fig1.savefig('plots_and_data_delta_action/experiment_{}/reward_difference_final_running_mean_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)

# plot difference between final and inital x
x_difference_i = []
x_difference_c = []
for i in np.arange(0, len(episode_data['initial_x'])):
    x_difference_i.append((np.abs(episode_data['initial_x'][i][0])
                          - np.abs(episode_data['final_x'][i][0])) * 1000)

    x_difference_c.append((np.abs(episode_data['initial_x'][i][1])
                          - np.abs(episode_data['final_x'][i][1])) * 1000)

# plot running quantiles of the improvement in mm
quantiles_i = running_quantiles(x_difference_i, 25, [0.25, 0.5, 0.75])
quantiles_c = running_quantiles(x_difference_c, 25, [0.25, 0.5, 0.75])

fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.plot(np.arange(0, len(quantiles_i[:, 1])), quantiles_i[:, 1], label='injected')
ax.fill_between(np.arange(0, len(quantiles_i[:, 1])), quantiles_i[:, 0], quantiles_i[:, 2], alpha=0.3)
ax.plot(np.arange(0, len(quantiles_c[:, 1])), quantiles_c[:, 1], label='circulating')
ax.fill_between(np.arange(0, len(quantiles_c[:, 1])), quantiles_c[:, 0], quantiles_c[:, 2], alpha=0.3)
ax.axhline(y=0, color='red')
ax.set_xlabel("Episode")
ax.set_ylabel("deviation improvement (mm)")
ax.legend()
plt.grid()

fig1.savefig('plots_and_data_delta_action/experiment_{}/deviation_difference_final_running_mean_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)


# plot running quantiles of final x
x_i = []
x_c = []
for i in np.arange(0, len(episode_data['final_x'])):
    x_i.append(episode_data['final_x'][i][0] * 1000)
    x_c.append(episode_data['final_x'][i][1] * 1000)

# plot running quantiles of the final deviations
quantiles_i = np.abs(running_quantiles(x_i, 25, [0.25, 0.5, 0.75]))
quantiles_c = np.abs(running_quantiles(x_c, 25, [0.25, 0.5, 0.75]))

fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.plot(np.arange(0, len(quantiles_i[:, 1])), quantiles_i[:, 1], label='injected')
ax.fill_between(np.arange(0, len(quantiles_i[:, 1])), quantiles_i[:, 0], quantiles_i[:, 2], alpha=0.3)
ax.plot(np.arange(0, len(quantiles_c[:, 1])), quantiles_c[:, 1], label='circulating')
ax.fill_between(np.arange(0, len(quantiles_c[:, 1])), quantiles_c[:, 0], quantiles_c[:, 2], alpha=0.3)
ax.set_xlabel("Episode")
ax.set_ylabel("x (mm)")
ax.legend()
plt.grid()

fig1.savefig('plots_and_data_delta_action/experiment_{}/deviation_final_running_mean_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)



# plot episode length
fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.plot(episode_data['length'])
ax.set_xlabel("Episode")
ax.set_ylabel("# of steps")
plt.grid()

fig1.savefig('plots_and_data_delta_action/experiment_{}/episode_length_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)

# plot running quantiles of the episode length
quantiles = running_quantiles(episode_data['length'], 25, [0.25, 0.5, 0.75])

fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.plot(np.arange(0, len(quantiles[:, 1])), quantiles[:, 1])
ax.fill_between(np.arange(0, len(quantiles[:, 1])), quantiles[:, 0], quantiles[:, 2], alpha=0.3)
ax.set_xlabel("Episode")
ax.set_ylabel("# of steps")
plt.grid()

fig1.savefig('plots_and_data_delta_action/experiment_{}/episode_length_running_mean_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)


'''
# plot reward difference vs average rise time change
fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
sc = ax.scatter(data['avg_rise_time_change'], reward_difference[-500:], c=avg_initital_settings[-500:], cmap=plt.cm.coolwarm)
plt.colorbar(sc, ax=ax, label='Average initial delays')
ax.set_xlabel("Avg percent increase of rise time")
ax.set_ylabel("Reward improvement")
plt.grid()

fig1.savefig('plots_and_data_delta_action/experiment_{}/qq_reward_difference_rise_time_change_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)
'''

'''
# comparison
run_nr_1 = 54
n_steps_1 = 500000
dtc_1 = 250
save_str_1 = 'SAC_run_{}'.format(run_nr_1)

run_nr_2 = 56
n_steps_2 = 500000
dtc_2 = 250
save_str_2 = 'SAC_run_{}'.format(run_nr_2)

with open("plots_and_data_delta_action/experiment_{}/episode_data_{}_steps_{}_ns_dtc_{}.pickle"
          .format(run_nr_1, n_steps_1, dtc_1, save_str_1), "rb") as input_file:
    episode_data_1 = pickle.load(input_file)

with open("plots_and_data_delta_action/experiment_{}/episode_data_{}_steps_{}_ns_dtc_{}.pickle"
          .format(run_nr_2, n_steps_2, dtc_2, save_str_2), "rb") as input_file:
    episode_data_2 = pickle.load(input_file)

success_percentage_1 = running_mean(episode_data_1['success'], 100)
success_percentage_2 = running_mean(episode_data_2['success'], 100)

fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.plot(success_percentage_1, label='SAC full waveforms')
ax.plot(success_percentage_2, label='SAC few points')
ax.set_xlabel("Training episode")
ax.set_ylabel("Fraction of successful episodes")
ax.legend()

fig1.savefig('plots_and_data_delta_action/comparison_run_{}_run_{}.png'.format(
        run_nr_1, run_nr_2), dpi=250)
'''

