# Script to calculate and test PCA models for the waveforms
# Evaluation is performed by recreating the original waveforms both on a training and test set.

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import librosa

from sklearn.decomposition import PCA

# load data
mkp_kick_data = np.genfromtxt('data_and_sequences/sps_lhcpilot_kick_data_centered.txt')  # .astype('float32')
time_data = np.genfromtxt('data_and_sequences/time_data_centered.txt')

max_rise_time_change = 0.2
wave_ind = 1
# indices for randomization
min_ind = 1400  # t = 4800
max_ind = 5000

factors = np.arange(0, 0.22, 0.02)
shifts = np.arange(-50, 60, 10)

'''
df_train = np.zeros((len(factors) * len(shifts), 33))

ind_r = 0
for factor in factors:
    waveform = np.copy(mkp_kick_data)[0]
    new_waveform = librosa.resample(waveform[min_ind:max_ind],
                                        orig_sr=(max_ind - min_ind),
                                        target_sr=(max_ind - min_ind) * (1 + factor),
                                        res_type='linear')

    waveform[min_ind:max_ind] = new_waveform[0:(max_ind - min_ind)]
    for shift in shifts:
        center_ind = 1494 - int(shift / 2)
        df_train[ind_r] = waveform[(center_ind - 64):(center_ind + 65):4]
        ind_r += 1

waveform = np.copy(mkp_kick_data)[0]
new_waveform = librosa.resample(waveform[min_ind:max_ind],
                                        orig_sr=(max_ind - min_ind),
                                        target_sr=(max_ind - min_ind) * (1 + 0),
                                        res_type='linear')

'''
# create training set for PCA
df_train = np.zeros((len(factors) * len(shifts), 75))

ind_r = 0
for factor in factors:
    waveform = np.copy(mkp_kick_data)[wave_ind]
    new_waveform = librosa.resample(waveform[min_ind:max_ind],
                                        orig_sr=(max_ind - min_ind),
                                        target_sr=(max_ind - min_ind) * (1 + factor),
                                        res_type='linear')

    waveform[min_ind:max_ind] = new_waveform[0:(max_ind - min_ind)]
    for shift in shifts:
        low = 1250 - int(shift / 2) + 100
        high = 1550 - int(shift / 2) + 100
        df_train[ind_r] = waveform[low:high:4]
        ind_r += 1

factors = np.arange(0.01, 0.23, 0.02)
shifts = np.arange(-45, 65, 10)

# create test set for PCA
df_test = np.zeros((len(factors) * len(shifts), 75))

ind_r = 0
for factor in factors:
    waveform = np.copy(mkp_kick_data)[wave_ind]
    new_waveform = librosa.resample(waveform[min_ind:max_ind],
                                        orig_sr=(max_ind - min_ind),
                                        target_sr=(max_ind - min_ind) * (1 + factor),
                                        res_type='linear')

    waveform[min_ind:max_ind] = new_waveform[0:(max_ind - min_ind)]
    for shift in shifts:
        low = 1250 - int(shift / 2) + 100
        high = 1550 - int(shift / 2) + 100
        df_test[ind_r] = waveform[low:high:4]
        ind_r += 1

results_train = np.zeros((20, df_train.shape[0]))
results_test = np.zeros((20, df_test.shape[0]))

# get errors for different numbers of PCAs
for i in np.arange(1, 21, 1):
    pca = PCA(n_components=i)
    pca.fit(df_train)

    Y = pca.transform(df_train)
    df_train_reconstructed = pca.inverse_transform(Y)

    Y = pca.transform(df_test)
    df_test_reconstructed = pca.inverse_transform(Y)

    rms = np.zeros(df_train.shape[0])
    for j in np.arange(0, df_train.shape[0], 1):
        rms[j] = (np.sum(((df_train[j] - df_train_reconstructed[j]) ** 2)) / len(df_train[j])) ** 0.5
    results_train[i-1] = rms

    rms = np.zeros(df_test.shape[0])
    for j in np.arange(0, df_test.shape[0], 1):
        rms[j] = (np.sum(((df_test[j] - df_test_reconstructed[j]) ** 2)) / len(df_test[j])) ** 0.5
    results_test[i - 1] = rms


fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(np.arange(1, 21, 2), fontsize=14)
plt.yticks(fontsize=14)
ax.plot(np.arange(1, 21, 1),
        (results_train.mean(axis=1) - np.min(results_train.mean(axis=1))) / (np.max(results_train.mean(axis=1)) - np.min(results_train.mean(axis=1))),
        label='Normalized average RMSE')
ax.plot(np.arange(1, 21, 1),
        (results_train.max(axis=1) - np.min(results_train.max(axis=1))) / (np.max(results_train.max(axis=1)) - np.min(results_train.max(axis=1))),
        label='Normalized maximum RMSE')
ax.set_xlabel("# principal components")
ax.set_ylabel("RMSE")
ax.legend()

fig1.savefig('plots_and_data_delta_action/PCA_training_error_waveform_{}.png'.format(wave_ind + 1))


fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(np.arange(1, 21, 2), fontsize=14)
plt.yticks(fontsize=14)
ax.plot(np.arange(1, 21, 1),
        (results_test.mean(axis=1) - np.min(results_test.mean(axis=1))) / (np.max(results_test.mean(axis=1)) - np.min(results_test.mean(axis=1))),
        label='Normalized average RMSE')
ax.plot(np.arange(1, 21, 1),
        (results_test.max(axis=1) - np.min(results_test.max(axis=1))) / (np.max(results_test.max(axis=1)) - np.min(results_test.max(axis=1))),
        label='Normalized maximum RMSE')
ax.set_xlabel("# principal components")
ax.set_ylabel("RMSE")
ax.legend()

fig1.savefig('plots_and_data_delta_action/PCA_test_error_waveform_{}.png'.format(wave_ind + 1))

