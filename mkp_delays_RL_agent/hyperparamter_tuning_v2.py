import numpy as np
import pickle
import torch
import os

class PPOTrial:
    def __init__(self, hyperparameters, path):
        self.path = path
        self.hyperparameters = hyperparameters
        self.current_best = None

        try:
            with open(path + '/trials.pickle', 'rb') as f:
                trials = pickle.load(f)
        except:
            trials = {}

        self.id = len(trials)

        new_trial = {'Trial{}'.format(self.id): hyperparameters}
        trials.update(new_trial)

        with open(path + '/trials.pickle', 'wb') as f:
            pickle.dump(trials, f)

        os.mkdir(path + 'Trial{}'.format(self.id))

    def update(self, model, performance_metric):
        if self.current_best is None:
            self.current_best = performance_metric
            model.save(self.path + 'Trial{}/best_model'.format(self.id))
            return True

        if performance_metric > self.current_best:
            self.current_best = performance_metric
            model.save(self.path + 'Trial{}/best_model'.format(self.id))
            return True

        return False

    def save(self):
        with open(self.path + 'Trial{}/progress'.format(self.id), 'wb') as f:
            pickle.dump(self, f)

    @classmethod
    def load(cls, filename):
        with open(filename, 'rb') as f:
            return pickle.load(f)


def create_grid(hyperparameters):
    for key in hyperparameters.keys():
        if len(hyperparameters[key]) > 1:
            for ele in hyperparameters[key]:
                kwargs = hyperparameters.copy()
                kwargs[key] = [ele]
                yield from create_grid(kwargs)
            return None

    hyperparameters = reformat_hyperparameters(hyperparameters)
    yield hyperparameters


def create_study_PPO(hyperparameters, width=[64], depth=[2]):
    """
    Sampler for PPO hyperparams.

    :param hyperparameters:
    :param width:
    :param depth:
    :return:
    """
    default_parameters = {
        'batch_size': [64],  # default
        'n_steps': [2048],  # default
        'gamma': [0.99],  # default
        'learning_rate': [3e-4],  # default
        'ent_coef': [0],  # default
        'clip_range': [0.2],  # default
        'n_epochs': [10],  # default
        'gae_lambda': [1.0],  # original paper
        'max_grad_norm': [0.5],  # default
        'vf_coef': [0.5],  # default
        'ortho_init': [False],
        'activation_fn': [torch.nn.ReLU]
    }

    net_arch = []
    for d in depth:
        for w in width:
            layers = [w for i in np.arange(0, d)]
            net_arch.append(dict(pi=layers, vf=layers))

    kwargs = default_parameters.copy()
    kwargs.update(dict(net_arch=net_arch))
    kwargs.update(hyperparameters)

    return kwargs


def reformat_hyperparameters(hyperparameters):
    kwargs = hyperparameters.copy()
    for key in hyperparameters.keys():
        kwargs[key] = kwargs[key][0]

    policy_kwargs = {'policy_kwargs': dict(
                                        net_arch=kwargs['net_arch'],
                                        activation_fn=kwargs['activation_fn'],
                                        ortho_init=kwargs['ortho_init'])
                     }
    kwargs.update(policy_kwargs)
    del kwargs['net_arch']
    del kwargs['activation_fn']
    del kwargs['ortho_init']

    return kwargs


hyp = {'batch_size': [1, 2, 3],
       'learning_rate': [0.5]}

study = create_study_PPO(hyp)

for x in create_grid(study):
    print(x)
