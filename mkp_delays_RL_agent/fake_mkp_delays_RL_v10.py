"""
Helper class meant to replicate the real MKP delays, but using historical waveform data. Version for RL agent, where
we include the possibility to randomize the shape of the waveforms for the agent to learn.
"""

import numpy as np
import librosa


class FakeMKPdelays:
    def __init__(self, optics='q20', randomize_waveforms=True, dtc=500, max_rise_time_change=0.2):
        """
        :param optics: Which optics of the SPS to use.
        :param randomize_waveforms: Whether waveforms should be stretched or not.
        :param dtc: Batch spacing.
        :param max_rise_time_change: Maximum stretching factor for the waveforms.
        """
        self.optics = optics
        self.mkp_kick_data = np.genfromtxt('data_and_sequences/sps_kick_data_train.txt')
        self.mkp_kick_data = self.mkp_kick_data.reshape(16, int(self.mkp_kick_data.shape[1] / 10000), 10000)
        self.time_data = np.genfromtxt('data_and_sequences/time_data_centered.txt')
        self.rmatrix = np.genfromtxt('data_and_sequences/R_matrix.txt')
        self.tmatrix = (np.genfromtxt('data_and_sequences/T_matrix.txt')).reshape(6, 6, 6)
        self.number_of_samples = self.mkp_kick_data.shape[1]
        self.current_sample = 0
        self.ref_timestamp = 4900  # timestamp of the beam at injection
        self.dtc = dtc
        self.dt_general = 4900
        self.randomize_waveforms = randomize_waveforms
        self.randomize_waveform_amplitude = False  # randomize waveform amplitude
        self.max_rise_time_change = max_rise_time_change  # maximum relative increase of rise time
        self.rise_time_change = []   # for logging purposes
        self.new_kicks = None
        self.new_kicks_c = None
        self.mkp_kicks = None
        self.mkp_kicks_c = None
        self.x_bar = None
        self.x_bar_c = None

        # set default kicks for injected beam
        mkp_volt = 52
        mkpa_kv2rad = 2.036307619e-05
        mkpc_kv2rad = 8.140618226e-06
        mkpl_kv2rad = 2.147001577e-05
        self.design_kicks = np.array([mkpa_kv2rad * mkp_volt,
                                      mkpa_kv2rad * mkp_volt,
                                      mkpc_kv2rad * mkp_volt,
                                      mkpl_kv2rad * mkp_volt])

        # -------------------- RANDOMIZATION OF WAVEFORM SHAPE OR AMPLITUDE -------------------------------
        # define waveform indices on which to start and stop
        self.min_ind = 1400  # t = 4800
        self.max_ind = 8000  # Complete flat top has to be included, we need it for normalization later

        # randomize waveforms
        self.mkp_waveforms = None
        self.augment_waveforms()

    def get_positions(self, dtau: np.ndarray) -> tuple[float, float]:
        """
        :param numpy.array dtau: the delays of the 8 switches.

        :return: tuple containing the horizontal deviations of the injected and circulating beam.
        """

        # Check at what time to evaluate the MKP kick for the injected beam, and for the circulating beam
        t = self.ref_timestamp   # timestamp for evaluation for injected beam
        tc = t - self.dtc  #  timestamp for evaluation for injected beam
        self.dt_general = dtau[-1]  # general time shift
        t_inds = []
        tc_inds = []
        for i in range(8):  # 8 switches in total to iterate over
            t_ind = np.argwhere(self.time_data == (2 * t - dtau[i] - (
                        (2 * t - dtau[i]) % 2)))  # find closest even index corresponding to this time
            tc_ind = np.argwhere(self.time_data == (tc + t - dtau[i] - ((tc + t - dtau[i]) % 2)))
            t_inds.append(t_ind)
            tc_inds.append(tc_ind)

        # Iterate over the modules, check the time shift for each switch, and add the kicks for the injected and circulating beam
        self.new_kicks = []
        self.new_kicks_c = []
        switch_count = 0
        for i in range(16):
            if ((i % 2) == 0) and (i != 0):  # after every two steps, change electrical switch
                switch_count += 1
            kick = self.mkp_waveforms[i][t_inds[switch_count]]
            kick_c = self.mkp_waveforms[i][tc_inds[switch_count]]
            self.new_kicks.append(kick)
            self.new_kicks_c.append(kick_c)

        # Define a vector with new MKP kicks, for the injected and circulating beam
        self.mkp_kicks = np.empty(4)
        self.mkp_kicks[0] = np.sum(self.new_kicks[:5])
        self.mkp_kicks[1] = np.sum(self.new_kicks[5:10])
        self.mkp_kicks[2] = np.sum(self.new_kicks[10:12])
        self.mkp_kicks[3] = np.sum(self.new_kicks[12:16])
        self.mkp_kicks_c = np.empty(4)
        self.mkp_kicks_c[0] = np.sum(self.new_kicks_c[:5])
        self.mkp_kicks_c[1] = np.sum(self.new_kicks_c[5:10])
        self.mkp_kicks_c[2] = np.sum(self.new_kicks_c[10:12])
        self.mkp_kicks_c[3] = np.sum(self.new_kicks_c[12:16])

        # deviation for the circulating beam, expecting 0 kick
        self.x_bar_c = self.calculate_deviations(kmkpa11931_actual=self.mkp_kicks_c[0],
                                                 kmkpa11936_actual=self.mkp_kicks_c[1],
                                                 kmkpc11952_actual=self.mkp_kicks_c[2],
                                                 kmkp11955_actual=self.mkp_kicks_c[3],
                                                 kmkpa11931_expected=0,
                                                 kmkpa11936_expected=0,
                                                 kmkpc11952_expected=0,
                                                 kmkp11955_expected=0)[0]

        # deviation for the injected beam, expecting design kick
        self.x_bar = self.calculate_deviations(kmkpa11931_actual=self.mkp_kicks[0],
                                               kmkpa11936_actual=self.mkp_kicks[1],
                                               kmkpc11952_actual=self.mkp_kicks[2],
                                               kmkp11955_actual=self.mkp_kicks[3],
                                               kmkpa11931_expected=self.design_kicks[0],
                                               kmkpa11936_expected=self.design_kicks[1],
                                               kmkpc11952_expected=self.design_kicks[2],
                                               kmkp11955_expected=self.design_kicks[3])[0]

        # return horizontal beam position of circulating and injected beam
        return self.x_bar, self.x_bar_c

    # call this function after every episode to randomize waveforms
    def augment_waveforms(self) -> None:
        """
        Increase the rise times randomly.
        For each switch a factor x is drawn from [0, self.max_rise_time_change].
        The rise times are scaled by (1 + x).
        """
        self.rise_time_change = []

        # initialize the waveforms
        sample = self.current_sample % self.number_of_samples
        self.mkp_waveforms = np.copy(self.mkp_kick_data[:, sample, :])

        self.current_sample += 1  # different sample every time we call augment_waveforms()

        if self.randomize_waveforms:
            ind = 0

            for ele in self.mkp_waveforms:
                if (ind % 2) == 0:  # randomize the change in rise time for each switch
                    factor = np.random.rand() * self.max_rise_time_change
                    self.rise_time_change.append(factor)
                new_waveform = librosa.resample(ele[self.min_ind:self.max_ind],
                                                orig_sr=(self.max_ind - self.min_ind),
                                                target_sr=(self.max_ind - self.min_ind) * (1 + factor),
                                                res_type='linear')

                # make sure, that the maximum is preserved (copy entire new waveform)
                self.mkp_waveforms[ind][self.min_ind:self.min_ind + len(new_waveform)] = new_waveform
                ind += 1

        # extract previous waveform data with randomized amplitude, or as it is
        if self.randomize_waveform_amplitude:
            ind = 0
            for ele in self.mkp_waveforms:
                self.mkp_waveforms[ind] = ele * (
                        1 + np.random.uniform(low=-0.05, high=0.05))  # randomize waveform amplitude of present values +/- X percent
                ind += 1

    # takes the actual and expected MKP kicks and returns the resulting deviations
    def calculate_deviations(self,
                             kmkpa11931_actual: float,
                             kmkpa11936_actual: float,
                             kmkpc11952_actual: float,
                             kmkp11955_actual: float,
                             kmkpa11931_expected: float,
                             kmkpa11936_expected: float,
                             kmkpc11952_expected: float,
                             kmkp11955_expected: float) -> np.ndarray:

        """
        Takes the actual and expected MKP-kicks and returns the 6d phase space variables.

        :param kmkpa11931_actual:
        :param kmkpa11936_actual:
        :param kmkpc11952_actual:
        :param kmkp11955_actual:
        :param kmkpa11931_expected:
        :param kmkpa11936_expected:
        :param kmkpc11952_expected:
        :param kmkp11955_expected:

        :return: The phase space variables at BPM.51503.
        """

        kmkpa11931_delta = kmkpa11931_actual - kmkpa11931_expected
        kmkpa11936_delta = kmkpa11936_actual - kmkpa11936_expected
        kmkpc11952_delta = kmkpc11952_actual - kmkpc11952_expected
        kmkp11955_delta = kmkp11955_actual - kmkp11955_expected

        # starting point is the center of the first kicker, initial conditions are zero
        result = np.array([0, kmkpa11931_delta])  # first kick
        result = result + [result[1] * 3.622, kmkpa11936_delta]  # drift to center of second kicker and second kick
        result = result + [result[1] * 2.8005, kmkpc11952_delta]  # drift to center of third kicker and third kick
        result = result + [result[1] * 2.8005, kmkp11955_delta]  # drift to center of fourth kicker and fourth kick
        result = result + [result[1] * 1.72, 0]  # marker after the fourth kicker, from here MADX takes over the transport

        result = np.pad(result, (0, 4), 'constant', constant_values=(0, 0))  # the other four phase space variables are 0

        first_order = np.matmul(self.rmatrix[:6, :6], result)

        second_order = np.zeros(6)
        for i in np.arange(0, 6):
            second_order[i] = np.sum(self.tmatrix[i] * np.outer(result, result))

        result = first_order + second_order

        return result
