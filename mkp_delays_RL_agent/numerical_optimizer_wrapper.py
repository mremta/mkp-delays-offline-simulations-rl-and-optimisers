import numpy as np
import matplotlib.pyplot as plt
import pickle
import time
import pybobyqa

import gym
import os

from mkpdelays_env_RL_delta_action_v17 import MKPOptEnv  # import environment using relative action
from scipy import optimize
from bayes_opt import BayesianOptimization, UtilityFunction

dtc = 200
run_nr = 128
max_rise_time_change = 0.2

os.mkdir('plots_and_data_delta_action/experiment_{}'.format(run_nr))

# Carry out the optimisation
dtau0 = np.array([4750, 4750, 4750, 4750, 4750, 4750, 4750, 4750])
bounds = ((4700, 4900), (4700, 4900),(4700, 4900),(4700, 4900),(4700, 4900),(4700, 4900),(4700, 4900),(4700, 4900))

# Initiate environment
env = MKPOptEnv(dtc=dtc, max_rise_time_change=max_rise_time_change, max_steps=1000,
                verbose=True)  # also include batch spacing time separation
env.seed(10815)

num_episodes = 100

results = {
    'final_x': [],
    'steps': []
}


# Define function to minimise, which also generates plots and saves data
def bayesian_objective(dtau1, dtau2, dtau3, dtau4, dtau5, dtau6, dtau7, dtau8, dtau9):
    actions = np.array([dtau1, dtau2, dtau3, dtau4, dtau5, dtau6, dtau7, dtau8])
    for i in np.arange(0, len(actions)):
        actions[i] = actions[i] + dtau9

    reward = -1 * env.compute_single_objective(actions)

    return reward


# Define function to minimise, which also generates plots and saves data
def minimizing_objective(dtau):
    actions = np.array(dtau[0:len(dtau) - 1])
    for i in np.arange(0, len(actions)):
        actions[i] = actions[i] + dtau[-1]
    reward = env.compute_single_objective(actions)

    return reward


# Evaluate the agent for the specified number of episodes
for episode in range(num_episodes):
    env.reset()

    x0_action = np.zeros(9)
    for i in np.arange(0, len(env.x0_action)):
        x0_action[i] = env.x0_action[i] - min(env.x0_action)

    x0_action[8] = min(env.x0_action)

    #'''
    result = optimize.minimize(env.compute_single_objective, dtau0, bounds=bounds, method='Powell', options={'xtol': 1e-2})
    print(result)
    results['final_x'].append(env.beampos)
    results['steps'].append(env.current_step)
    #'''

    '''
    # Bayesian optimisation
    p_bounds = {f"dtau{ele}": (x0_action[ele - 1] - 100, x0_action[ele - 1] + 100) for ele in range(1, 10)}
    optimiser = BayesianOptimization(
        f=bayesian_objective,
        pbounds=p_bounds,
        verbose=1,
        random_state=10,
    )
    # init-point = random exploration
    # n_iter = iteration of gaussian process
    # hyperparameters can be tuned from this example: https://github.com/fmfn/BayesianOptimization/blob/master/examples/exploitation_vs_exploration.ipynb
    utility = UtilityFunction(kind="ei", xi=1e-1)
    optimiser.maximize(init_points=30, n_iter=100, acquisition_function=utility)
    final_x = np.array(
        [
            optimiser.max["params"][f"dtau{ele}"]
            for ele in range(1, 9)
        ]
    )

    print("Results:")
    print(optimiser.max)
    print(final_x)
    '''

    '''
    upper_lim = np.ones(9) * 100 + x0_action
    lower_lim = -1 * np.ones(9) * 100 + x0_action
    bounds = (lower_lim, upper_lim)
    dtau0 = x0_action

    soln = pybobyqa.solve(
        minimizing_objective,
        dtau0,
        bounds=bounds,
        rhobeg=0.5,
        rhoend=0.02,
        # objfun_has_noise=True,
        seek_global_minimum=True,
        print_progress=False,
    )
    print(soln)
    '''
env.close()

with open(
        "plots_and_data_delta_action/experiment_{}/optimizer_results.pickle".format(
            run_nr
        ),
        "wb",
    ) as handle:
        pickle.dump(
            results, handle, protocol=pickle.HIGHEST_PROTOCOL
        )

'''
run_nr = 94
n_steps = 1000000
dtc = 200
save_str = 'PPO_run_{}'.format(run_nr)

# read episode data
with open("plots_and_data_delta_action/experiment_{}/episode_data_{}_steps_{}_ns_dtc_{}.pickle"
          .format(run_nr, n_steps, dtc, save_str), "rb") as input_file:
    episode_data = pickle.load(input_file)

print(episode_data['final_x'])
'''

