"""
Script for more in-depth performance plots. Allows to analyze agent performs with respect to the difficulty of episodes
"""



import numpy as np
import pandas as pd

import pandas as pd
import pickle
import matplotlib.pyplot as plt

# Fill out these parameters to specify which run to plot
run_nr = 61
n_steps = 500000
dtc = 250
save_str = 'TD3_run_{}'.format(run_nr)

# read episode data
with open("plots_and_data_delta_action/experiment_{}/episode_data_{}_steps_{}_ns_dtc_{}.pickle"
          .format(run_nr, n_steps, dtc, save_str), "rb") as input_file:
    episode_data = pickle.load(input_file)

# plot the left and right quantiles of the success rates
rise_time_change = []
for ele in episode_data['rise_time_change'][-500:]:
    rise_time_change.append(np.mean(ele))

data = pd.DataFrame({'success': episode_data['success'][-500:],
                    'rise_time_change': rise_time_change})

data = data.sort_values(['rise_time_change'], ascending=True)

# left quantile
x = data['rise_time_change']
y = np.cumsum(data['success']) / np.cumsum(np.ones(len(data['success'])))

fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.plot(x, y)
ax.set_xlabel("Increase of rise time")
ax.set_ylabel("Fraction of cases solved")

fig1.savefig('plots_and_data_delta_action/experiment_{}/success_rate_leftq_rise_time_change_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)

# right quantile
data = data.sort_values(['rise_time_change'], ascending=False)

x = data['rise_time_change']
y = np.cumsum(data['success']) / np.cumsum(np.ones(len(data['success'])))

fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.plot(x, y)
ax.set_xlabel("Increase of rise time")
ax.set_ylabel("Fraction of cases solved")

fig1.savefig('plots_and_data_delta_action/experiment_{}/success_rate_rightq_rise_time_change_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)



# boxplot of each individual magnet, only for the failed cases
rise_time_change = []
for i in np.arange(0, 8, 1):
    rise_time_change_indiv = []
    for ele in episode_data['rise_time_change'][-500:]:
        rise_time_change_indiv.append(ele[i])
    rise_time_change.append(rise_time_change_indiv)

data = pd.DataFrame({'success': episode_data['success'][-500:],
                     'rise_time_change_1': rise_time_change[0],
                     'rise_time_change_2': rise_time_change[1],
                     'rise_time_change_3': rise_time_change[2],
                     'rise_time_change_4': rise_time_change[3],
                     'rise_time_change_5': rise_time_change[4],
                     'rise_time_change_6': rise_time_change[5],
                     'rise_time_change_7': rise_time_change[6],
                     'rise_time_change_8': rise_time_change[7]})

# split dataframe into data from the individual switches
data_gb = data.groupby('success')

data_split_1 = [data_gb.get_group(x)['rise_time_change_1'] for x in data_gb.groups]
data_split_2 = [data_gb.get_group(x)['rise_time_change_2'] for x in data_gb.groups]
data_split_3 = [data_gb.get_group(x)['rise_time_change_3'] for x in data_gb.groups]
data_split_4 = [data_gb.get_group(x)['rise_time_change_4'] for x in data_gb.groups]
data_split_5 = [data_gb.get_group(x)['rise_time_change_5'] for x in data_gb.groups]
data_split_6 = [data_gb.get_group(x)['rise_time_change_6'] for x in data_gb.groups]
data_split_7 = [data_gb.get_group(x)['rise_time_change_7'] for x in data_gb.groups]
data_split_8 = [data_gb.get_group(x)['rise_time_change_8'] for x in data_gb.groups]

rise_time_change = []
rise_time_change.append(data_split_1[0])
rise_time_change.append(data_split_2[0])
rise_time_change.append(data_split_3[0])
rise_time_change.append(data_split_4[0])
rise_time_change.append(data_split_5[0])
rise_time_change.append(data_split_6[0])
rise_time_change.append(data_split_7[0])
rise_time_change.append(data_split_8[0])

# create boxplot for each switch (fails)
fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
#ax.set_xticklabels(['fail', 'success'])
ax.boxplot(rise_time_change)
ax.set_xlabel("Switch")
ax.set_ylabel("Percent increase of rise time")

fig1.savefig('plots_and_data_delta_action/experiment_{}/fail_indiv_rise_time_change_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)

rise_time_change = []
rise_time_change.append(data_split_1[1])
rise_time_change.append(data_split_2[1])
rise_time_change.append(data_split_3[1])
rise_time_change.append(data_split_4[1])
rise_time_change.append(data_split_5[1])
rise_time_change.append(data_split_6[1])
rise_time_change.append(data_split_7[1])
rise_time_change.append(data_split_8[1])

# create boxplot for each switch (successes)
fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
#ax.set_xticklabels(['fail', 'success'])
ax.boxplot(rise_time_change)
ax.set_xlabel("Switch")
ax.set_ylabel("Percent increase of rise time")

fig1.savefig('plots_and_data_delta_action/experiment_{}/success_indiv_rise_time_change_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)

# boxplot of the average rise time change, split by episode outcome
avg_rise_time_change = []
for ele in episode_data['rise_time_change'][-500:]:
    avg_rise_time_change.append(np.mean(ele))

max_rise_time_change = []
for ele in episode_data['rise_time_change'][-500:]:
    max_rise_time_change.append(np.max(ele))

data = pd.DataFrame({'success': episode_data['success'][-500:],
                     'avg_rise_time_change': avg_rise_time_change,
                     'max_rise_time_change': max_rise_time_change})

# split dataframe
data_gb = data.groupby('success')
data_split_avg = [data_gb.get_group(x)['avg_rise_time_change'] for x in data_gb.groups]
data_split_max = [data_gb.get_group(x)['max_rise_time_change'] for x in data_gb.groups]

# create boxplot for mean
fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.set_xticklabels(['fail', 'success'])
ax.boxplot(data_split_avg)
ax.set_xlabel("Episode outcome")
ax.set_ylabel("Avg percent increase of rise time")

fig1.savefig('plots_and_data_delta_action/experiment_{}/avg_rise_time_change_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)

# create boxplot for max
fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.set_xticklabels(['fail', 'success'])
ax.boxplot(data_split_max)
ax.set_xlabel("Episode outcome")
ax.set_ylabel("Max percent increase of rise time")

fig1.savefig('plots_and_data_delta_action/experiment_{}/max_rise_time_change_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)

# plot avg rise time change vs max rise time change
fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.plot(data_split_avg[0], data_split_max[0], 'o', color="red")
ax.plot(data_split_avg[1], data_split_max[1], 'o', color="green")
ax.set_xlabel("Avg percent increase of rise time")
ax.set_ylabel("Max percent increase of rise time")

fig1.savefig('plots_and_data_delta_action/experiment_{}/qq_rise_time_change_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)

# plot avg rise time change vs max rise time change
fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.plot(data_split_avg[1], data_split_max[1], 'o')
ax.set_xlabel("Avg percent increase of rise time")
ax.set_ylabel("Max percent increase of rise time")

fig1.savefig('plots_and_data_delta_action/experiment_{}/success_qq_rise_time_change_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)



'''
#Also plot reward over time
fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.plot(final_reward, 'o', markersize=1)
ax.axhline(y=(-5e-4), color='red')
ax.set_xlabel("Training episode")
ax.set_ylabel("Final Loss")

fig1.savefig('plots_and_data_delta_action/experiment_{}/agent_final_loss_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)


#Also plot reward over time
fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.plot(episode_data["reward_sum"], 'o', markersize=1)
ax.set_xlabel("Training episode")
ax.set_ylabel("Reward")

fig1.savefig('plots_and_data_delta_action/experiment_{}/agent_reward_{}_steps_{}_ns_dtc_{}_points.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)
'''
# calculate running average, starting at index 1 (not a zero)
def running_mean(x, N):
    cumsum = np.cumsum(np.insert(x, 0, 0))
    return (cumsum[N:] - cumsum[:-N]) / float(N)

'''
success_percentage = running_mean(episode_data['success'], 100)

#Also plot avg successrate over time
fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.plot(success_percentage)
ax.set_xlabel("Training episode")
ax.set_ylabel("Fraction of successful episodes")

fig1.savefig('plots_and_data_delta_action/experiment_{}/agent_success_{}_steps_{}_ns_dtc_{}.png'.format(
        run_nr, n_steps, dtc, save_str), dpi=250)

print(np.sum(episode_data['success'][1000:2000]) / len(episode_data['success'][1000:2000]))
print(np.sum(episode_data['success'][-1000:]) / len(episode_data['success'][-1000:]))

print(np.sum(episode_data['success']) / len(episode_data['success']))

test_data = np.load("plots_and_data_delta_action/experiment_{}/evaluations.npz".format(run_nr), "rb")
print(test_data['results'])
'''

'''
# comparison
run_nr_1 = 54
n_steps_1 = 500000
dtc_1 = 250
save_str_1 = 'SAC_run_{}'.format(run_nr_1)

run_nr_2 = 56
n_steps_2 = 500000
dtc_2 = 250
save_str_2 = 'SAC_run_{}'.format(run_nr_2)

with open("plots_and_data_delta_action/experiment_{}/episode_data_{}_steps_{}_ns_dtc_{}.pickle"
          .format(run_nr_1, n_steps_1, dtc_1, save_str_1), "rb") as input_file:
    episode_data_1 = pickle.load(input_file)

with open("plots_and_data_delta_action/experiment_{}/episode_data_{}_steps_{}_ns_dtc_{}.pickle"
          .format(run_nr_2, n_steps_2, dtc_2, save_str_2), "rb") as input_file:
    episode_data_2 = pickle.load(input_file)

success_percentage_1 = running_mean(episode_data_1['success'], 100)
success_percentage_2 = running_mean(episode_data_2['success'], 100)

fig1 = plt.figure(figsize=(10,7))
ax = fig1.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.yaxis.label.set_size(20)
ax.xaxis.label.set_size(20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
ax.plot(success_percentage_1, label='SAC full waveforms')
ax.plot(success_percentage_2, label='SAC few points')
ax.set_xlabel("Training episode")
ax.set_ylabel("Fraction of successful episodes")
ax.legend()

fig1.savefig('plots_and_data_delta_action/comparison_run_{}_run_{}.png'.format(
        run_nr_1, run_nr_2), dpi=250)
'''