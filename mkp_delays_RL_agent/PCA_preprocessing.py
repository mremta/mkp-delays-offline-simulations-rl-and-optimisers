# Script to create and save PCA models for the waveforms

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import librosa
import pickle

from sklearn.decomposition import PCA

# load data
mkp_kick_data = np.genfromtxt('data_and_sequences/sps_lhcpilot_kick_data_centered.txt')  # .astype('float32')
time_data = np.genfromtxt('data_and_sequences/time_data_centered.txt')

max_rise_time_change = 0.2
n_pc = 7
# indices for randomization
min_ind = 1400  # t = 4800
max_ind = 5000

factors = np.arange(0, 0.22, 0.02)
shifts = np.arange(-50, 60, 10)

for i in np.arange(0, 16, 2):
    # create training set for PCA
    df_train = np.zeros((len(factors) * len(shifts), 75))

    ind_r = 0
    for factor in factors:
        waveform = np.copy(mkp_kick_data)[i]
        new_waveform = librosa.resample(waveform[min_ind:max_ind],
                                            orig_sr=(max_ind - min_ind),
                                            target_sr=(max_ind - min_ind) * (1 + factor),
                                            res_type='linear')

        waveform[min_ind:max_ind] = new_waveform[0:(max_ind - min_ind)]
        for shift in shifts:
            low = 1250 - int(shift / 2) + 100
            high = 1550 - int(shift / 2) + 100
            df_train[ind_r] = waveform[low:high:4]
            ind_r += 1

    pca = PCA(n_components=n_pc)
    pca.fit(df_train)

    # save pca in a pickle file
    with open('data_and_sequences/pca_waveform_{}.pickle'.format(i+1), 'wb') as pickle_file:
        pickle.dump(pca, pickle_file)

# approximate normalization parameters
normalization_parameter = np.zeros((16, 2))

for i in np.arange(0, 12, 1):
    normalization_parameter[i][0] = -0.00035
    normalization_parameter[i][1] = 0.00035
for i in np.arange(12, 16, 1):
    normalization_parameter[i][0] = -0.00045
    normalization_parameter[i][1] = 0.00045

np.savetxt('data_and_sequences/normalization_parameter_pca.txt', X=normalization_parameter)