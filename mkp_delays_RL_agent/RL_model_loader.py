"""
Script to load trained RL model for the MKP delay RL agent
"""

import numpy as np
import matplotlib.pyplot as plt
import pickle
import time 
import gym
from scipy import optimize

from stable_baselines3 import PPO, A2C # DQN coming soon
from stable_baselines3 import TD3
from stable_baselines3.common.noise import NormalActionNoise
from stable_baselines3.common.env_checker import check_env
from mkpdelays_env_RL import MKPOptEnv

# Initiate and load model 
algorithm = 'A2C'
n_steps = 50000
dtc = 250
model_str = "plots_and_data/model _{}_steps_{}_ns_dtc_{}_run_6_sigmoid_of_3".format(n_steps, dtc, algorithm)
save_plot = False 

# Initiate environment
env =  MKPOptEnv(dtc=dtc, sigmoid_start=3)   # also include batch spacing time separation

# ------------------- DEFINE TRAINING ALGORITHM and LOAD MODEL --------------------------------------
# Train the agent - TD3, here we use action noise
if algorithm == 'TD3':
    model = TD3.load(model_str)

# Train the agent - A2C, and clock the training 
if algorithm == 'A2C':
    model = A2C.load(model_str)

# Train agent PPO
if algorithm == 'PPO':
    model = PPO.load(model_str)
# ------------------------------------------------------------------------------------


# ---------------------- Test the trained agent ------------------------
obs = env.reset()  # first reset and observe where initial real action is 
fig = env.render("matplotlib_figures")
if save_plot:
    fig[0].savefig('plots_and_data/agent_test_before_{}_steps_{}_ns_dtc_{}.png'.format(n_steps, dtc, algorithm), dpi=250)

n_steps_test = 20
for step in range(n_steps_test):
  action, _ = model.predict(obs, deterministic=True)
  print("Step {}".format(step + 1))
  print("Action: ", action)
  obs, reward, done, info = env.step(action)
  print("Reward: {}".format(reward))
  if done:
    print("Goal reached!", "reward=", reward)
    break

 # observe where final real action is 
fig2 = env.render("matplotlib_figures")
if save_plot:
    fig2[0].savefig('plots_and_data/agent_test_after_{}_steps_{}_ns_dtc_{}.png'.format(n_steps, dtc, algorithm), dpi=250)

# plot resulting beam oscillations
fig3 = plt.figure(figsize=(10,7))
env.mkp_delays.plot_oscillations(fig3)

# Final beam position, also to generate Twiss 
final_beam_pos = env.mkp_delays.step(env.inv_norm_data(action))  
print("Final beam position: {}".format(final_beam_pos))



