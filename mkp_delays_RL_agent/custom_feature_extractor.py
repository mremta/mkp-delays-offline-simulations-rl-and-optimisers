import torch as th
import torch.nn as nn
from gym import spaces

from stable_baselines3.common.torch_layers import BaseFeaturesExtractor


class CustomCombinedExtractor(BaseFeaturesExtractor):
    """
    :param observation_space: (gym.Space)
    :param features_dim: (int) Number of features extracted.
        This corresponds to the number of unit for the last layer.
    """

    def __init__(self, observation_space: spaces.Dict):
        n_flat_features = observation_space['other'].shape[0]
        n_input_channels = observation_space['waveforms'].shape[0]

        super().__init__(observation_space, (128 + n_flat_features))

        extractors = {}
        # 1d CNN for waveforms
        extractors['waveforms'] = self.cnn = nn.Sequential(
                                        nn.Conv1d(n_input_channels, 16, kernel_size=8, stride=4, padding=2),
                                        nn.ReLU(),
                                        nn.MaxPool1d(kernel_size=4, stride=2, padding=1),
                                        nn.ReLU(),
                                        nn.Flatten()
        )

        # Flatten all other (numerical) inputs
        extractors['other'] = self.flatten = nn.Sequential(
                                        nn.Flatten()
        )

        self.extractors = nn.ModuleDict(extractors)

    def forward(self, observations) -> th.Tensor:
        encoded_tensor_list = []

        # self.extractors contain nn.Modules that do all the processing.
        for key, extractor in self.extractors.items():
            encoded_tensor_list.append(extractor(observations[key]))
        # Return a (B, self._features_dim) PyTorch tensor, where B is batch dimension.
        return th.cat(encoded_tensor_list, dim=1)

