"""
Optimisation environment for numerical optimisers to find ideal ideal delays of the MKP electrical switches 
in the SPS injection in order to minimise the horizontal beam oscillations

Self-contained environment, with all sequence and IC files loaded from the same repository

Also added possibility to randomize rise time of waveforms with sigmoids, as in the case of RL agent 
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
from cpymad.madx import Madx

#Import accelerator physics library with functions
sys.path.append('/home/ewaagaa/cernbox2/Tech_student/Code')  #for office computer
import acc_phy_lib_elias 

class MKPOptEnv():
    
    #Load optics, voltage-to-kick converted data, and corresponding time data
    def __init__(self, optics, randomize_waveforms=False):
        self.optics = optics
        self.mkp_kick_data = np.genfromtxt('data_and_sequences/sps_lhcpilot_kick_data.txt') 
        self.time_data = np.genfromtxt('data_and_sequences/sps_lhcpilot_time_data.txt') 
        self.ref_timestamp = 0   #timestamp of flat top kick in ns,  use a third into the flat top for MKP module 16 (meaning exactly at the midpoint - 1/6*width)
        self.dtc = 900  #time shift in ns between injected batch and ciruclating batch in the SPS
        self.randomize_waveforms = randomize_waveforms  # randomize waveform rise time shape with sigmoid 
        self.sigmoid_start = -5 # for randomization of rise time, where to start the sigmoid function interval

        # -------------------- RANDOMIZATION OF WAVEFORM SHAPE OR AMPLITUDE -------------------------------
        # define waveform indices on which to start and stop
        self.min_ind = 2400
        self.max_ind = 2625
    
        # randomize shape of waveform rise time  
        if self.randomize_waveforms:
            ind = 0
            self.mkp_waveforms = self.mkp_kick_data  # first initialize the waveforms as they are today 
            print("\nRandomizing waveform rise times...\n")
            for ele in self.mkp_kick_data:
                sigmoid_interval = np.linspace(np.random.rand()*self.sigmoid_start, 5, num=(self.max_ind - self.min_ind))  # space over which the randomizing sigmoid function is defined: random start value between 0 and sigmoid_start, up to 10
                ele[self.min_ind:self.max_ind] = ele[self.min_ind:self.max_ind]*self.sigmoid(sigmoid_interval)
                self.mkp_waveforms[ind] = ele
        else:
            self.mkp_waveforms = self.mkp_kick_data  # otherwise just copy the waveform data 
        self.mkp_kick_data = self.mkp_waveforms
    
        self.start_reset()

    # define sigmoid function by which we vary the rise time of the MKP
    def sigmoid(self, x):
        z = np.exp(-x)
        sig = 1 / (1 + z)
    
        return sig            

    #Method to calculate kick deviation dx' for injected beam, as well as the residual kick x_c' for the circulating beam  
    def step_dkick(self, dtau):

        #Check at what time to evaluate the MKP kick for the injected beam, and for the circulating beam
        self.t = self.ref_timestamp + dtau[-1]  #new general time stamp 
        self.tc = self.ref_timestamp + dtau[-1] - self.dtc  #new general time stamp for circulating beam
        self.dt_general = dtau[-1]  #general time shift 
        self.t_inds = []
        self.tc_inds = []
        for i in range(8):  #8 switches in total to iterate over 
            t_ind = np.argwhere(self.time_data==(self.t+dtau[i] - ((self.t+dtau[i]) % 2)))  #find closest even index corresponding to this time 
            tc_ind = np.argwhere(self.time_data==(self.tc+dtau[i] - ((self.tc+dtau[i]) % 2)))  
            self.t_inds.append(t_ind)
            self.tc_inds.append(tc_ind)

        #Iterate over the modules, check the time shift for each switch, and add the kicks for the injected and circulating beam
        self.new_kicks = []
        self.new_kicks_c = []
        switch_count = 0
        for i in range(16):
            if (((i) % 2) == 0) and (i!=0):  #after every two steps, change electrical switch
                switch_count += 1  
            kick = self.mkp_kick_data[i][self.t_inds[switch_count]]
            kick_c = self.mkp_kick_data[i][self.tc_inds[switch_count]]
            self.new_kicks.append(kick)
            self.new_kicks_c.append(kick_c)
        
        #Define a vector with new MKP kicks, for the injected and circulating beam
        self.mkp_kicks = np.empty(4)
        self.mkp_kicks[0] = np.sum(self.new_kicks[:5])
        self.mkp_kicks[1] = np.sum(self.new_kicks[5:10])
        self.mkp_kicks[2] = np.sum(self.new_kicks[10:12])
        self.mkp_kicks[3] = np.sum(self.new_kicks[12:16])
        self.mkp_kicks_c = np.empty(4)
        self.mkp_kicks_c[0] = np.sum(self.new_kicks_c[:5])
        self.mkp_kicks_c[1] = np.sum(self.new_kicks_c[5:10])
        self.mkp_kicks_c[2] = np.sum(self.new_kicks_c[10:12])
        self.mkp_kicks_c[3] = np.sum(self.new_kicks_c[12:16])
         
        #Calculate difference in kick for injected beam, and residual kick for circulating beam 
        self.dkicks = np.array([self.mkp_kicks[0] - self.mkp_kicks_org1, self.mkp_kicks[1] - self.mkp_kicks_org2, \
            self.mkp_kicks[2] - self.mkp_kicks_org3, self.mkp_kicks[3] - self.mkp_kicks_org4])
        self.dkicks_c = np.array([self.mkp_kicks_c[0] - self.mkp_kicks_org1_c, self.mkp_kicks_c[1] - self.mkp_kicks_org2_c, \
            self.mkp_kicks_c[2] - self.mkp_kicks_org3_c, self.mkp_kicks_c[3] - self.mkp_kicks_org4_c])
        
        #Find square of the difference for all kicks, for the objective function 
        dkick_sq = (self.mkp_kicks[0] - self.mkp_kicks_org1)**2 + (self.mkp_kicks[1] - self.mkp_kicks_org2)**2 + \
            (self.mkp_kicks[2] - self.mkp_kicks_org3)**2 + (self.mkp_kicks[3] - self.mkp_kicks_org4)**2
        dkick_c_sq = (self.mkp_kicks_c[0] - self.mkp_kicks_org1_c)**2 + (self.mkp_kicks_c[1] - self.mkp_kicks_org2_c)**2 + \
            (self.mkp_kicks_c[2] - self.mkp_kicks_org3_c)**2 + (self.mkp_kicks_c[3] - self.mkp_kicks_org4_c)**2
    
        #print("MKP kick offset injected beam: {}".format(dkick_sq))
        #print("MKP kick offset circulating beam: {}".format(dkick_c_sq))
    
        obj_func = 5*dkick_sq + dkick_c_sq
        
        return obj_func
    

    #Method to calculate emittance of the injected and circulating beam (c) for a given main timeshift for the MKP switches 
    def step_ex(self, t_mkp):
        
        #Initialise emittance and Twiss parameters if needed 
        self.betx_mkp = 44.521566367347496 #self.twiss.loc['mkpa.11936'].betx  #find the value of beta in the middle of the MKPs
        self.ex0 = 1.26305858e-07 #self.madx.sequence['short_injection_and_some_sps'].beam.ex  #find original emittance 
        
        #Find corresponding kick deviation to a main time shift 
        dtau = np.full(8, 0)
        dtau = np.append(dtau, t_mkp)
        self.step_dkick(dtau)  #execute steo method, which generates dx' and dx'_c arrays 
        #add the injection kick deviations from today's values:
        self.dxprime = np.sum(self.dkicks) 
        #add the circulating kick deviations from today's values (i.e deviations from 0 kick)
        self.dxprime_c = np.sum(self.dkicks_c)  
          
        #Calculate emittance blow-up
        self.ex_new_opt = self.ex0 + 1/(2*self.betx_mkp)*(self.betx_mkp*self.dxprime)**2
        self.ex_new_opt_c = self.ex0 + 1/(2*self.betx_mkp)*(self.betx_mkp*self.dxprime_c)**2
        
        obj_func = self.ex_new_opt**2 + self.ex_new_opt_c**2
        
        return obj_func


    #Method to calculate oscillations of the injected and circulating beam (c) for a given timeshift vector for the MKP switches 
    def step(self, dtau):
        print(dtau)
        #Here pipe in time shift vector, output average oscillations 
        
        
        #Check at what time to evaluate the MKP kick for the injected beam, and for the circulating beam
        self.t = self.ref_timestamp + dtau[-1]  #new general time stamp 
        self.tc = self.ref_timestamp + dtau[-1] - self.dtc  #new general time stamp for circulating beam
        self.dt_general = dtau[-1]  #general time shift 
        self.t_inds = []
        self.tc_inds = []
        for i in range(8):  #8 switches in total to iterate over 
            t_ind = np.argwhere(self.time_data==(self.t+dtau[i] - ((self.t+dtau[i]) % 2)))  #find closest even index corresponding to this time 
            tc_ind = np.argwhere(self.time_data==(self.tc+dtau[i] - ((self.tc+dtau[i]) % 2)))  
            self.t_inds.append(t_ind)
            self.tc_inds.append(tc_ind)

        
        #Iterate over the modules, check the time shift for each switch, and add the kicks for the injected and circulating beam
        self.new_kicks = []
        self.new_kicks_c = []
        switch_count = 0
        for i in range(16):
            if (((i) % 2) == 0) and (i!=0):  #after every two steps, change electrical switch
                switch_count += 1  
            kick = self.mkp_kick_data[i][self.t_inds[switch_count]]
            kick_c = self.mkp_kick_data[i][self.tc_inds[switch_count]]
            self.new_kicks.append(kick)
            self.new_kicks_c.append(kick_c)
        
        #Define a vector with new MKP kicks, for the injected and circulating beam
        self.mkp_kicks = np.empty(4)
        self.mkp_kicks[0] = np.sum(self.new_kicks[:5])
        self.mkp_kicks[1] = np.sum(self.new_kicks[5:10])
        self.mkp_kicks[2] = np.sum(self.new_kicks[10:12])
        self.mkp_kicks[3] = np.sum(self.new_kicks[12:16])
        self.mkp_kicks_c = np.empty(4)
        self.mkp_kicks_c[0] = np.sum(self.new_kicks_c[:5])
        self.mkp_kicks_c[1] = np.sum(self.new_kicks_c[5:10])
        self.mkp_kicks_c[2] = np.sum(self.new_kicks_c[10:12])
        self.mkp_kicks_c[3] = np.sum(self.new_kicks_c[12:16])
      
        #Update the MKP kicks through the MADX globals
        self.madx.globals['kmkpa11931'] = self.mkp_kicks[0] 
        self.madx.globals['kmkpa11936'] = self.mkp_kicks[1] 
        self.madx.globals['kmkpc11952'] = self.mkp_kicks[2]
        self.madx.globals['kmkp11955'] = self.mkp_kicks[3] 
        self.madx2.globals['kmkpa11931'] = self.mkp_kicks_c[0] 
        self.madx2.globals['kmkpa11936'] = self.mkp_kicks_c[1] 
        self.madx2.globals['kmkpc11952'] = self.mkp_kicks_c[2]
        self.madx2.globals['kmkp11955'] = self.mkp_kicks_c[3] 
        
        """
        self.madx.sequence['short_injection_and_some_sps'].elements['mkpa.11931'].kick = self.mkp_kicks[0] 
        self.madx.sequence['short_injection_and_some_sps'].elements['mkpa.11936'].kick = self.mkp_kicks[1] 
        self.madx.sequence['short_injection_and_some_sps'].elements['mkpc.11952'].kick = self.mkp_kicks[2] 
        self.madx.sequence['short_injection_and_some_sps'].elements['mkp.11955'].kick = self.mkp_kicks[3] 
        self.madx2.sequence['reduced_circulating_sps'].elements['mkpa.11931'].kick = self.mkp_kicks_c[0] 
        self.madx2.sequence['reduced_circulating_sps'].elements['mkpa.11936'].kick = self.mkp_kicks_c[1] 
        self.madx2.sequence['reduced_circulating_sps'].elements['mkpc.11952'].kick = self.mkp_kicks_c[2] 
        self.madx2.sequence['reduced_circulating_sps'].elements['mkp.11955'].kick = self.mkp_kicks_c[3] 
        """

        print("MKP kicks injected beam: {}".format(self.mkp_kicks))
        print("MKP kicks circulating beam: {}".format(self.mkp_kicks_c))
        #print("MKP kicks circulating beam: {}".format(self.mkp_kicks_c))
            
        #Get oscillations from injected beam
        try:   
            #Attempt Twiss commands for the injected and circulating beam , restart MADX if it crashes    
            #Injected beam
            self.madx.use(sequence='short_injection_and_some_sps')
            twiss = self.madx.twiss(betx=self.twiss_beta1['betx'], alfx=-self.twiss_beta1['alfx'],
                                    bety=self.twiss_beta1['bety'], alfy=-self.twiss_beta1['alfy'], 
                                   dx=self.twiss_beta1['dx'], dpx=-self.twiss_beta1['dpx'],
                                    dy = self.twiss_beta1['dy'], dpy=-self.twiss_beta1['dpy'],
                                    x=self.twiss_beta1['x'], y=self.twiss_beta1['y'],
                                    px=-self.twiss_beta1['px'], py=-self.twiss_beta1['py']).dframe()
    
            
            #Find the average amplitude of the oscillations for the injected beam
            self.x_bar = np.max(abs(twiss.loc['qf.12010':'qf.13010'].x))
            self.px_bar = np.max(abs(twiss.loc['qf.12010':'qf.13010'].px))
            
            del twiss
            
            #oscillations = self.x_bar**2 + self.px_bar**2    
            x1bar = self.x_bar
            #print("Oscillations = {:.2e}".format(oscillations))
           

        #Restart MADX if it crashes
        except RuntimeError:
            print("Resetting MADX for injected beam...") 
            with open('tempfile', 'r') as f:
                lines = f.readlines()
                for ele in lines:
                    if '+=+=+= fatal' in ele:
                        print('{}'.format(ele))
            self.reset()
            #Set the objective function to a high value
            x1bar = 100  
         
        
        #"""
        #Get oscillations from circulating beam
        try:
            #Attempt Twiss commands for the injected and circulating beam , restart MADX if it crashes 
            self.madx2.use(sequence="reduced_circulating_sps")
            twiss_c = self.madx2.twiss(betx=self.initbeta0[0], alfx=self.initbeta0[1],
                                bety=self.initbeta0[2], alfy=self.initbeta0[3], 
                               dx=self.initbeta0[4], dpx=self.initbeta0[5],
                                dy = self.initbeta0[6], dpy=self.initbeta0[7],
                                x=self.initbeta0[8], y=self.initbeta0[9],
                                px=self.initbeta0[10], py=self.initbeta0[11]).dframe()
                
            #Find the average amplitude of the oscillations for the circulating beam
            self.x_bar_c = np.max(abs(twiss_c.loc['qf.12010':'qf.13010'].x))
            self.px_bar_c = np.max(abs(twiss_c.loc['qf.12010':'qf.13010'].px))
        
            del twiss_c
        
            #oscillations_c = self.x_bar_c**2 + self.px_bar_c**2
            x2bar = self.x_bar_c 
            #print("Oscillations_c = {:.2e}".format(oscillations_c))

        #Restart MADX if it crashes
        except RuntimeError:
            print("Resetting MADX for circulating beam...") 
            with open('tempfile2', 'r') as f:
                lines = f.readlines()
                for ele in lines:
                    if '+=+=+= fatal' in ele:
                        print('{}'.format(ele))
            self.reset()
            #Set the objective function to a high value
            x2bar = 100  
        
        # Calculate injection oscillations
        self.sq_sum_bpm = x1bar ** 2 + x2bar ** 2 + (x1bar - x2bar) ** 2
        
        #"""
        
        #"""
        #Reset the MKP kicks to their original values, by acting on the global variables 
        self.madx.globals['kmkpa11931'] = self.mkp_kicks_org1
        self.madx.globals['kmkpa11936'] = self.mkp_kicks_org2
        self.madx.globals['kmkpc11952'] = self.mkp_kicks_org3
        self.madx.globals['kmkp11955'] = self.mkp_kicks_org4
        self.madx2.globals['kmkpa11931'] = self.mkp_kicks_org1_c 
        self.madx2.globals['kmkpa11936'] = self.mkp_kicks_org2_c 
        self.madx2.globals['kmkpc11952'] = self.mkp_kicks_org3_c 
        self.madx2.globals['kmkp11955'] = self.mkp_kicks_org4_c 
        #"""
        
        return self.sq_sum_bpm 
    
   
    #Method to start a new madx process 
    def start_reset(self): 
        
        with open('tempfile', 'w') as f:
            self.madx = Madx(stdout=f,stderr=f)   
        self.madx.option(echo=False, warn=True, info=False, debug=False, verbose=False)

        # --------------- CIRCULATING BEAM -----------------
        with open('tempfile2', 'w') as f:
            self.madx2 = Madx(stdout=f,stderr=f)   
        self.madx2.option(echo=False, warn=True, info=False, debug=False, verbose=False)
        
        #Load sequence from class method 
        self.load_seq()    
    
    
    #Method to reset madx if it crashes, and kills it 
    def reset(self): 
        
        print("Reset!")
        # ------------- INJECTED BEAM ---------------------
        #If MAD-X fails, re-spawn process   
        #if 'self.madx' in globals():
        self.madx.quit()
        del self.madx
        print("Deleted!")
        with open('tempfile', 'w') as f:
            self.madx = Madx(stdout=f,stderr=f)   
        self.madx.option(echo=False, warn=True, info=False, debug=False, verbose=False)

        # --------------- CIRCULATING BEAM -----------------
        #If MAD-X fails, re-spawn process   
        #if 'self.madx2' in globals():
        self.madx2.quit()    
        del self.madx2
        with open('tempfile2', 'w') as f:
            self.madx2 = Madx(stdout=f,stderr=f)   
        self.madx2.option(echo=False, warn=True, info=False, debug=False, verbose=False)
        
        #Load sequence from class method 
        self.load_seq()


    #Method to load injection sequence and parts of SPS 
    def load_seq(self):
       
   
         # -------------------------- THEN LOAD INJECTED BEAM, WITH SOME SPS ------------------------------ 
        #Activate the aperture for the Twiss flag to include it in Twiss command! 
        self.madx.input('select,flag=twiss,clear;')
        self.madx.select(flag='twiss', column=['N1', 'apertype', 'aper_1', 'aper_2', 'aper_3', 'aper_4', 'apoff_1', 'apoff_2', 'aptol_1', 'aptol_2', 'aptol_3'])                
        self.madx.options['update_from_parent']=True

       #Load sequence and initial conditions for short_injection_and_some_sps, to get the injected beam and about 412 m into the SPS 
        self.madx.call('data_and_sequences/short_injection_and_some_sps_{}.seq'.format(self.optics))
        #Load Twiss and survey parameters
        self.twiss_reversed = pd.read_csv("data_and_sequences/sps_injection_twiss_reversed_{}.csv".format(self.optics))
        self.twiss_beta1 = self.twiss_reversed.iloc[-1] #locate Twiss parameters at the start of the loaded reversed sequence, initial conditions for later    
    
        #Call the old aperture files for SPS, for the injection sequence
        self.madx.use(sequence="short_injection_and_some_sps")
        self.madx.call("data_and_sequences/aperturedb_1.dbx")
        self.madx.call("data_and_sequences/aperturedb_2.dbx")
        self.madx.call("data_and_sequences/aperturedb_3.dbx")   
    
        #Extract beam info - same for injected and circulating 
        self.madx.use(sequence='short_injection_and_some_sps')
        self.sige = self.madx.sequence['short_injection_and_some_sps'].beam['sige']  #relative energy spread
        self.ex = self.madx.sequence['short_injection_and_some_sps'].beam['ex']
        self.ey = self.madx.sequence['short_injection_and_some_sps'].beam['ey']
      
        # -------------------------- LOAD CIRCULATING BEAM IN SEPARATE MADX PROCESS ---------------------------------------
        #Activate the aperture for the Twiss flag to include it in Twiss command! 
        self.madx2.input('select,flag=twiss,clear;')
        self.madx2.select(flag='twiss', column=['N1', 'apertype', 'aper_1', 'aper_2', 'aper_3', 'aper_4', 'apoff_1', 'apoff_2', 'aptol_1', 'aptol_2', 'aptol_3']) 
        self.madx2.options['update_from_parent']=True
    
        #Load sequence and initial conditions for tt2tt10SPS
        self.madx2.call('data_and_sequences/reduced_circulating_sps_{}.seq'.format(self.optics))
        self.initbeta0 = np.genfromtxt('data_and_sequences/reduced_circulating_sps_twiss_init_{}.csv'.format(self.optics))
        #self.initbeta0 = pd.read_csv('data_and_sequences/reduced_circulating_sps_twiss_init_{}.csv'.format(self.optics), index_col=0, header=False)
        
        # ----------------- FIX THE APERTURES for the plot ------------------------------- 
        #Call the old aperture files for SPS 
        self.madx2.use(sequence="reduced_circulating_sps")
        self.madx2.call("data_and_sequences/aperturedb_1.dbx")
        self.madx2.call("data_and_sequences/aperturedb_2.dbx")
        self.madx2.call("data_and_sequences/aperturedb_3.dbx")
       
        #Save original MKP kicks for later
        self.mkp_kicks_org1 = self.madx.globals['kmkpa11931']
        self.mkp_kicks_org2 = self.madx.globals['kmkpa11936']
        self.mkp_kicks_org3 = self.madx.globals['kmkpc11952']
        self.mkp_kicks_org4 = self.madx.globals['kmkp11955'] 
       
        self.mkp_kicks_org1_c = self.madx2.globals['kmkpa11931']
        self.mkp_kicks_org2_c = self.madx2.globals['kmkpa11936']
        self.mkp_kicks_org3_c = self.madx2.globals['kmkpc11952']
        self.mkp_kicks_org4_c = self.madx2.globals['kmkp11955'] 
        
        
        
    #Method to generate Twiss parameters specifically for the environment    
    def generate_twiss(self):
        
        #Update the MKP kicks through the MADX globals
        self.madx.globals['kmkpa11931'] = self.mkp_kicks[0] 
        self.madx.globals['kmkpa11936'] = self.mkp_kicks[1] 
        self.madx.globals['kmkpc11952'] = self.mkp_kicks[2]
        self.madx.globals['kmkp11955'] = self.mkp_kicks[3] 
        self.madx2.globals['kmkpa11931'] = self.mkp_kicks_c[0] 
        self.madx2.globals['kmkpa11936'] = self.mkp_kicks_c[1] 
        self.madx2.globals['kmkpc11952'] = self.mkp_kicks_c[2]
        self.madx2.globals['kmkp11955'] = self.mkp_kicks_c[3] 
        
        #Injected beam
        self.madx.use(sequence='short_injection_and_some_sps')
        self.twiss = self.madx.twiss(betx=self.twiss_beta1['betx'], alfx=-self.twiss_beta1['alfx'],
                                bety=self.twiss_beta1['bety'], alfy=-self.twiss_beta1['alfy'], 
                               dx=self.twiss_beta1['dx'], dpx=-self.twiss_beta1['dpx'],
                                dy = self.twiss_beta1['dy'], dpy=-self.twiss_beta1['dpy'],
                                x=self.twiss_beta1['x'], y=self.twiss_beta1['y'],
                                px=-self.twiss_beta1['px'], py=-self.twiss_beta1['py']).dframe()

        #Circulating beam
        self.madx2.use(sequence="reduced_circulating_sps")
        self.twiss_c = self.madx2.twiss(betx=self.initbeta0[0], alfx=self.initbeta0[1],
                                bety=self.initbeta0[2], alfy=self.initbeta0[3], 
                               dx=self.initbeta0[4], dpx=self.initbeta0[5],
                                dy = self.initbeta0[6], dpy=self.initbeta0[7],
                                x=self.initbeta0[8], y=self.initbeta0[9],
                                px=self.initbeta0[10], py=self.initbeta0[11]).dframe()
        
        #Find maximum oscillation amplitude 
        self.x_bar = np.max(abs(self.twiss.loc['qf.12010':'qf.13010'].x))
        self.px_bar = np.max(abs(self.twiss.loc['qf.12010':'qf.13010'].px))
        self.x_bar_c = np.max(abs(self.twiss_c.loc['qf.12010':'qf.13010'].x))
        self.px_bar_c = np.max(abs(self.twiss_c.loc['qf.12010':'qf.13010'].px))
        
        #Find Courant-Snyder invariant, for injected and circulating beam
        alpha = self.twiss.loc['qf.12010'].alfx
        beta = self.twiss.loc['qf.12010'].betx
        gamma = (1+alpha**2)/beta
        x = self.twiss.loc['qf.12010'].x
        px = self.twiss.loc['qf.12010'].px
        self.J = 0.5*(gamma*x**2 + 2*alpha*x*px + beta*px**2)

        alpha_c = self.twiss_c.loc['qf.12010'].alfx
        beta_c = self.twiss_c.loc['qf.12010'].betx
        gamma_c = (1+alpha_c**2)/beta_c
        x_c = self.twiss_c.loc['qf.12010'].x
        px_c = self.twiss_c.loc['qf.12010'].px
        self.J_c = 0.5*(gamma_c*x_c**2 + 2*alpha_c*x_c*px_c + beta_c*px_c**2)



    #Method to plot the oscillations 
    def plot_oscillations(self, fig): 
        if fig is None:
            fig = plt.figure(figsize=(10,7))
        fig.suptitle('SPS oscillations - injected and circulating beam',fontsize=20)
        ax = fig.add_subplot(1, 1, 1)  # create an axes object in the figure
        ax.yaxis.label.set_size(24)
        ax.xaxis.label.set_size(24)
        plt.xticks(fontsize=18)  
        plt.yticks(fontsize=18)
        #Define some parameters for the circulating beam, and plot it on the same axis as the injected 
        if self.optics == 'sftpro':
            nx = 6
        else:
            nx = 5
        #Plot the injected beam:
        acc_phy_lib_elias.plot_envelope(self.twiss, self.sige, self.ex, self.ey, ax, nx=nx)
        #Align the injected and circulating beam longitudinally, then plot it on the same axis
        ds = self.twiss_c.loc['qf.12010'].s - self.twiss.loc['qf.12010'].s
        self.twiss_c['s'] = self.twiss_c['s'].add(-ds)  
        acc_phy_lib_elias.plot_envelope(self.twiss_c, self.sige, self.ex, self.ey, ax, nx=nx, hcolor='r') 
        # GET THE APERTURE AND PLOT IT 
        new_pos_x, aper_neat_x = acc_phy_lib_elias.get_apertures_real(self.twiss_c.iloc[1:-2 , :])
        acc_phy_lib_elias.plot_aper_real(new_pos_x, aper_neat_x, ax)
        #----------------------------------------------------------------
        
        ax.legend(['Injected beam', 'Circulating beam'], loc='upper right', facecolor='white', framealpha=0.8, prop={'size': 18})
        plt.ylim(-0.1, 0.1)
        plt.xlim(40, 450)
        t1 = plt.text(300, -0.095, '$d\\tau_{{general}} = {:.1f}$ ns\n$dt_{{c}} = {}$ ns'.format(self.dt_general, self.dtc), fontsize=18, horizontalalignment='left')
        #t2 = plt.text(55, -0.08, '$\\bar{{x}} =$ {:.1e} m,  $\\bar{{p}}_{{x}} =$ {:.1e} GeV/c'.format(self.x_bar, self.px_bar), fontsize=12, horizontalalignment='left')
        #t3= plt.text(55, -0.095, '$\\bar{{x}}_{{c}} =$ {:.1e} m,  $\\bar{{p}}_{{x, c}} =$ {:.1e} GeV/c'.format(self.x_bar_c, self.px_bar_c), fontsize=12, horizontalalignment='left')
        t1.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
        #t2.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
        #t3.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='white'))
        #print("MKP kicks injected beam: {}".format(self.mkp_kicks))
        #print("MKP kicks circulating beam: {}".format(self.mkp_kicks_c))
    
    
    #Method to plot the MKP waveform kicks, and where the injected and circulating beam will hit 
    def plot_mkp_kicks(self, fig):
        if fig is None:
            fig = plt.figure(figsize=(10,7))
        fig.suptitle('MKP kick waveforms - injected and circulating beam',fontsize=20)
        ax = fig.add_subplot(1, 1, 1)  # create an axes object in the figure
        ax.yaxis.label.set_size(24)
        ax.xaxis.label.set_size(24)
        plt.xticks(fontsize=18)  
        plt.yticks(fontsize=18)
        for i in range(16):
            plt.plot(self.time_data, 1000*self.mkp_kick_data[i], label='_nolegend_')
        plt.ylabel('Kick [mrad]')
        plt.xlabel('Time [ns]')
        plt.xlim(4.5e3, 6e3)
        plt.axvline(x=self.t, color='b', ls='--', linewidth=4, label='Injected beam')
        plt.axvline(x=self.tc, color='r', ls='--', linewidth=4, label='Circulating beam')
        plt.legend(facecolor='white', framealpha=1, loc=4, prop={'size': 18})
    
        
    #Plot Courant-Snyder invariant as a function of t_MKP
    def plot_J(self, fig):
        
        #Create time vector for MKP ref time stamp, and find corresponding Courant-Snyder invariant
        self.Js = []
        self.Js_c = []
        self.t_mkp = np.linspace(4500, 6000, 15)  #Define linspace for steering error in nanoseconds 
        self.reset()
        for times in self.t_mkp:    
            #self.reset()  #uncomment if computer cannot handle consecutive Twiss commands
            dtau = np.full(8, 0)
            dtau = np.append(dtau, times)
            self.step_dkick(dtau)  #execute steo method, which generates dx' and dx'_c arrays 
            self.generate_twiss()
            self.Js.append(self.J)
            self.Js_c.append(self.J_c)

        #Plot the values
        if fig is None:
            fig = plt.figure(figsize=(10,7))
        fig.suptitle('Courant-Snyder invariant J - injected and circulating beam',fontsize=16)        
        ax = fig.add_subplot(1, 1, 1)  # create an axes object in the figure
        ax.yaxis.label.set_size(24)
        ax.xaxis.label.set_size(24)
        plt.xticks(fontsize=18)  
        plt.yticks(fontsize=18)
        plt.plot(self.t_mkp, self.Js, linewidth=3, label='Injected', color='b')
        plt.plot(self.t_mkp, self.Js_c, linewidth=3, label='Circulating', color='r')
        plt.plot(self.t_mkp, abs(np.array(self.Js)) + abs(np.array(self.Js_c)), label='|Injected| + |Circulating|', marker='*', markersize=16, linewidth=4, linestyle='dotted', color='g')
        ax.set_ylabel("$J_{new}$ [m rad]")
        ax.set_xlabel("$t_{MKP}$ [ns]")
        ax.set_yscale('log')
        plt.legend(loc=4, prop={'size': 18})
    
    
    #Method to plot the kick deviation dx', where dx' = dx'(t_MKP), and the emittance as a function of t_MKP
    def plot_emittance(self, fig):
        #Initialise emittance and Twiss parameters 
        if self.twiss is None:
            self.generate_twiss()  #generate Twiss if not already done  
        self.betx_mkp = self.twiss.loc['mkpa.11936'].betx  #find the value of beta in the middle of the MKPs
        self.ex0 = self.madx.sequence['short_injection_and_some_sps'].beam.ex  #find original emittance 
        
        #Create time vector for MKP ref time stamp, and find corresponding kick deviation
        self.dxprimes = []
        self.dxprimes_c = []
        self.t_mkp = np.linspace(4500, 8500, 100)  #Define linspace for steering error in nanoseconds 
        for times in self.t_mkp:
            dtau = np.full(8, 0)
            dtau = np.append(dtau, times)
            self.step_dkick(dtau)  #execute steo method, which generates dx' and dx'_c arrays 
            #add the injection kick deviations from today's values:
            dxprime = np.sum(self.dkicks) 
            self.dxprimes.append(dxprime)
            #add the circulating kick deviations from today's values (i.e deviations from 0 kick)
            dxprime_c = np.sum(self.dkicks_c)  
            self.dxprimes_c.append(dxprime_c)
        #Convert them to numpy arrays 
        self.dxprimes = np.array(self.dxprimes)
        self.dxprimes_c = np.array(self.dxprimes_c)
        
        #Calculate emittance blow-up
        self.ex_new = self.ex0 + 1/(2*self.betx_mkp)*(self.betx_mkp*self.dxprimes)**2
        self.ex_new_c = self.ex0 + 1/(2*self.betx_mkp)*(self.betx_mkp*self.dxprimes_c)**2
        
        #Calculate emittance blow-up after filamentation after many turns 
        self.ex_new_fil = self.ex0 + 1/(2*self.betx_mkp)*(self.betx_mkp*self.dxprimes)**2*np.exp(-2*100/144.27)
        self.ex_new_c_fil = self.ex0 + 1/(2*self.betx_mkp)*(self.betx_mkp*self.dxprimes_c)**2*np.exp(-2*100/144.27)
        
        #Plot kick deviations 
        if fig is None:
            fig = plt.figure(figsize=(10,7))
        fig.suptitle('Emittance blow-up from MKP steering error',fontsize=20)
        ax = fig.add_subplot(2, 1, 1)  # create an axes object in the figure
        ax.yaxis.label.set_size(20)
        ax.xaxis.label.set_size(20)
        plt.xticks(fontsize=14)  
        plt.yticks(fontsize=14)
        plt.plot(self.t_mkp, self.dxprimes, label='Injected', color='g')
        plt.plot(self.t_mkp, self.dxprimes_c, label='Circulating', color='r')
        plt.plot(self.t_mkp, abs(self.dxprimes) + abs(self.dxprimes_c), label='|Injected| + |Circulating|', linestyle='dashed')
        ax.set_ylabel("$\Delta x'$ [rad]")
        ax.set_xlabel("$t_{MKP}$ [ns]")
        plt.legend()
        
        #Plot emittance blow-up as a function of t_MKP
        ax2 = fig.add_subplot(2, 1, 2)  # create an axes object in the figure
        ax2.yaxis.label.set_size(20)
        ax2.xaxis.label.set_size(20)
        plt.xticks(fontsize=14)  
        plt.yticks(fontsize=14)
        plt.plot(self.t_mkp, self.ex_new, label='Injected', color='g')
        plt.plot(self.t_mkp, self.ex_new_c, label='Circulating', color='r')
        plt.plot(self.t_mkp, self.ex_new_fil, label='Injected, filamented 100 turns', marker='*', color='g')
        plt.plot(self.t_mkp, self.ex_new_c_fil, label='Circulating, filamented 100 turns', marker='*', color='r')
        ax2.set_ylabel("$\epsilon_{new}$")
        ax2.set_xlabel("$t_{MKP}$ [ns]")
        ax2.set_yscale('log')
        plt.axhline(y=self.ex0, color='m', linestyle='dotted', label='$\epsilon_{0}$')
        plt.legend()
        
    #If context manager has been used, print the lines of the temporary error file 
    def print_madx_error(self):
        with open('tempfile', 'r') as f:
            lines = f.readlines()
            for ele in lines:
                if '+=+=+= fatal' in ele:
                    print('{}'.format(ele))
